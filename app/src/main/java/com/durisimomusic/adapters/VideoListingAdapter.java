package com.durisimomusic.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.durisimomusic.R;
import com.durisimomusic.activities.HomeScreen;
import com.durisimomusic.application.Durisimo;
import com.durisimomusic.data.models.video.FavVideosResponse;
import com.durisimomusic.data.models.video.MenuItemsData;
import com.durisimomusic.data.remote.APIUtils;
import com.durisimomusic.fragments.AssetDetailFragment;
import com.durisimomusic.fragments.GridMusicFragment;
import com.durisimomusic.fragments.VideoFragment;
import com.durisimomusic.util.AppManageInterface;
import com.durisimomusic.util.AppUtil;
import com.durisimomusic.util.Const;
import com.durisimomusic.util.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class VideoListingAdapter extends RecyclerView.Adapter<VideoListingAdapter.ViewHolder> {

    public static final int TYPE_VERTICAL = 0, TYPE_HORIZONTAL = 1, TYPE_FAV = 2, TYPE_MUSIC = 3, TYPE_GRID_MUSIC = 4;
    private final Context context;
    public ArrayList<MenuItemsData> data = new ArrayList<>();
    //    public static ArrayList<MenuItemsData> data1 = new ArrayList<>();
    private String type;
    private FragmentManager fragmentManager;
    private AppManageInterface appManageInterface;

    public VideoListingAdapter(Context context, FragmentManager fragmentManager, ArrayList<MenuItemsData> data, String type) {
        this.context = context;
        this.data = data;
        this.type = type;
        this.fragmentManager = fragmentManager;
        appManageInterface = (AppManageInterface) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_HORIZONTAL)
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_asset_grid_video, parent, false), viewType);
        else if (viewType == TYPE_MUSIC)
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_mixes, parent, false), viewType);
        else if (viewType == TYPE_GRID_MUSIC)
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_category, parent, false), viewType);
        else
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_asset_grid_verticle_video, parent, false), viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

//        data = data;
        if (getItemViewType(position) == TYPE_HORIZONTAL) {
            holder.assetTitle.setText(data.get(position).getCategoryName());

            Glide
                    .with(Durisimo.applicationContext)
                    .load(data.get(position).getCategoryImage())
                    .placeholder(R.drawable.app_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .error(R.drawable.app_logo)
                    .into(holder.assetImage);

            holder.container.setOnClickListener(view -> {
                VideoFragment fragment = VideoFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("path", "2");
                bundle.putString("type", data.get(position).getCategoryFor());
                bundle.putString("title", data.get(position).getCategoryName());
                bundle.putString("id", data.get(position).getCategoryId());
                bundle.putBoolean("isInner", true);
                fragment.setArguments(bundle);
                AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
            });
        }

        else if (getItemViewType(position) == TYPE_MUSIC) {
            Glide
                    .with(Durisimo.applicationContext)
                    .load(data.get(position).getCategoryImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.app_logo)
                    .centerCrop()/*
                    .thumbnail(0.25f)
                    .transition(withCrossFade())*/
                    .error(R.drawable.app_logo)
                    .into(holder.assetImage);
            holder.assetTitle.setText(data.get(position).getCategoryName());

            holder.container.setOnClickListener(view -> {
                AssetDetailFragment fragment = AssetDetailFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("path", data.get(position).getMenuId());
                bundle.putString("type", data.get(position).getCategoryFor());
                bundle.putString("title", data.get(position).getCategoryName());
                bundle.putString("id", data.get(position).getCategoryId());
                fragment.setArguments(bundle);
                AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
            });
        }

        else if (getItemViewType(position) == TYPE_GRID_MUSIC) {
            Glide
                    .with(Durisimo.applicationContext)
                    .load(data.get(position).getCategoryImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(R.drawable.app_logo)
                    .error(R.drawable.app_logo)
                    .into(holder.iv_category);
            holder.tv_category_name.setText(data.get(position).getCategoryName());
            holder.tv_parent_category_name.setText(data.get(position).getParentCategoryName());

            holder.rl_main.setOnClickListener(view -> {
                HomeCategoryAdapter.isfromDialogMusic = false;
                GridMusicFragment fragment = GridMusicFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("path", data.get(position).getMenuId());
                bundle.putString("type", "GridTracks");
                bundle.putString("title", data.get(position).getCategoryName());
                bundle.putString("id", data.get(position).getCategoryId());
                fragment.setArguments(bundle);
                AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
            });
        }

        else {

            if (getItemViewType(position) == TYPE_FAV) {
                Prefs.getPrefInstance().setValue(context, Const.VIDEO_TYPE, type);
                holder.assetDate.setVisibility(View.GONE);
                holder.delete.setVisibility(View.VISIBLE);
            } else {
                Prefs.getPrefInstance().setValue(context, Const.VIDEO_TYPE, type);
                holder.assetDate.setVisibility(View.VISIBLE);
                holder.delete.setVisibility(View.GONE);
            }
            holder.assetTitle.setText(data.get(position).getVideosName());
            String date = "Released\n" + data.get(position).getDate();
            holder.assetDate.setText(date);
            holder.container.setOnClickListener(view -> {
                if (data.get(position).getUrlType() == 1) {
                    appManageInterface.hideVideoPlayer();
                    Prefs.getPrefInstance().setValue(context, Const.VIDEO_URL_TYPE, "youtube");
                    Prefs.getPrefInstance().setValue(context, Const.VIDEO_YOUTUBE_URL, data.get(position).getVideosLink());
                    appManageInterface.webviewManageMent();
                    HomeScreen.isMusicopen = false;
                    HomeScreen.isVideoOpen = true;
                    HomeScreen.isPipModeEnabled = false;
                    appManageInterface.showYoutubeWebview(data.get(position), data, position, type);
                    appManageInterface.closeVideoBottom();
                } else {
                    Prefs.getPrefInstance().saveSharedPreferencesLogList(context, data);
                    Prefs.getPrefInstance().setValue(context, Const.VIDEO_POSITION, String.valueOf(position));
//                    Log.d("mytag", "here is size is-----------------------------------" + data.size());
//                    Log.d("mytag", "here is position is-----------------------------------" +position);
//                    Log.d("mytag", "here is type is-----------------------------------" +type);
                    Prefs.getPrefInstance().setValue(context, Const.VIDEO_URL_TYPE, "normal");
                    appManageInterface.PlayerManageMent();
                    HomeScreen.isMusicopen = false;
                    HomeScreen.isVideoOpen = true;
                    HomeScreen.isPipModeEnabled = false;
                    HomeScreen.islivetvBackClicked = true;
                    appManageInterface.showMotionVideo(data.get(position), data, position, type);
                }
            });

            Glide
                    .with(Durisimo.applicationContext)
                    .load(data.get(position).getVideosImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(R.drawable.video_placeholder)
                    .error(R.drawable.app_logo)
                    .into(holder.assetImage);

            holder.delete.setOnClickListener(view -> {
                View dialog_view1 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                final AlertDialog dialog1 = new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setView(dialog_view1)
                        .show();

                if (dialog1.getWindow() != null)
                    dialog1.getWindow().getDecorView().getBackground().setAlpha(0);

                ((TextView) dialog_view1.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.remove_video_from_fav));
                ((Button) dialog_view1.findViewById(R.id.dialog_ok)).setText(context.getResources().getString(R.string.yes));
                ((Button) dialog_view1.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.no));
                dialog_view1.findViewById(R.id.dialog_ok).setOnClickListener(v1 -> {
                    dialog1.dismiss();
                    add_remove_fav_videos(position, view);
                });
                dialog_view1.findViewById(R.id.dialog_cancel).setOnClickListener(v2 -> dialog1.dismiss());
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (type.equals(context.getResources().getString(R.string.type_vertical)) || type.equals(context.getResources().getString(R.string.type_suggested))) {
            return TYPE_VERTICAL;
        } else if (type.equals(context.getResources().getString(R.string.type_fav))) {
            return TYPE_FAV;
        } else if (type.equals(context.getResources().getString(R.string.type_tracks_category))) {
            return TYPE_MUSIC;
        } else if (type.equals("GridTracksCategory")) {
            return TYPE_GRID_MUSIC;
        } else {
            return TYPE_HORIZONTAL;
        }
    }

    public void add(ArrayList<MenuItemsData> listingData) {
        this.data.addAll(listingData);
        notifyDataSetChanged();
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    private void add_remove_fav_videos(int position, View mainView) {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("menu_id", data.get(position).getMenuId());
                jsonObject.put("videos_id", data.get(position).getVideosId());
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody add_remove_fav_videos = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().add_remove_fav_videos(add_remove_fav_videos, "AddRemoveFavouriteVideos").enqueue(new Callback<FavVideosResponse>() {
                @Override
                public void onResponse(@NonNull Call<FavVideosResponse> call, @NonNull Response<FavVideosResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            VideoFragment.getInstance().setFavVideoList();
                            AppUtil.show_Snackbar(context, mainView, response.body().getMessage(), false);
                        } else {
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<FavVideosResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView assetImage, iv_category;
        TextView assetTitle, tv_category_name, tv_parent_category_name;
        TextView assetDate;
        ImageView delete;
        LinearLayout container;
        RelativeLayout rl_main;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            assetImage = itemView.findViewById(R.id.asset_image);
            tv_category_name = itemView.findViewById(R.id.tv_category_name);
            tv_parent_category_name = itemView.findViewById(R.id.tv_parent_category_name);
            rl_main = itemView.findViewById(R.id.rl_main);
            iv_category = itemView.findViewById(R.id.iv_category);
            assetTitle = itemView.findViewById(R.id.asset_title);
            container = itemView.findViewById(R.id.container);
            if (viewType == TYPE_VERTICAL || viewType == TYPE_FAV)
                assetDate = itemView.findViewById(R.id.asset_date);
            delete = itemView.findViewById(R.id.delete);
        }
    }
}
