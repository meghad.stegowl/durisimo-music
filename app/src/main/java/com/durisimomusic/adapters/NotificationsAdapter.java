package com.durisimomusic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.durisimomusic.R;
import com.durisimomusic.data.models.notifications.NotificationsData;

import java.util.ArrayList;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<NotificationsData> data;
    private FragmentManager fragmentManager;


    public NotificationsAdapter(Context context, ArrayList<NotificationsData> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_notifications, parent, false), viewType);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.notificationMessage.setText(data.get(position).getMessage());
        holder.notificationTitle.setText(data.get(position).getTitle());
        holder.notificationTime.setText(data.get(position).getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView notificationTitle;
        LinearLayout assetContainer;
        LinearLayout mainContainer;
        TextView notificationTime;
        TextView notificationMessage;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            notificationMessage = itemView.findViewById(R.id.notification_message);
            assetContainer = itemView.findViewById(R.id.asset_container);
            mainContainer = itemView.findViewById(R.id.main_container);
            notificationTime = itemView.findViewById(R.id.notification_time);
            notificationTitle = itemView.findViewById(R.id.notification_title);
        }
    }
}
