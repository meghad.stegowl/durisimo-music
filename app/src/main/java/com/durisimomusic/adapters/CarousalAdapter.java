package com.durisimomusic.adapters;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.durisimomusic.R;
import com.durisimomusic.Services.FloatingWidgetService;
import com.durisimomusic.application.Durisimo;
import com.durisimomusic.data.models.live_tv.LiveTvData;
import com.durisimomusic.util.AppUtil;

import java.util.ArrayList;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class CarousalAdapter extends RecyclerView.Adapter<CarousalAdapter.ViewHolder> {

    Context context;
    ArrayList<LiveTvData> content;


    public CarousalAdapter(Context context, ArrayList<LiveTvData> content) {
        this.context = context;
        this.content = content;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.row_carousal), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Glide
                .with(Durisimo.applicationContext)
                .load(content.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.app_logo)
                .centerCrop()/*
                .thumbnail(0.25f)
                .transition(withCrossFade())*/
                .error(R.drawable.app_logo)
                .into(holder.assetImage);

        holder.assetImage.setOnClickListener(view -> {
            try {
                FloatingWidgetService.hidePipPlayer();
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(content.get(position).getLink()));
                context.startActivity(intent);

            } catch (ActivityNotFoundException e) {
                //TODO smth
            }
        });

    }

    @Override
    public int getItemCount() {
        return content.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView assetImage;
        RelativeLayout container;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            assetImage = itemView.findViewById(R.id.asset_image);
            container = itemView.findViewById(R.id.container);
        }
    }
}
