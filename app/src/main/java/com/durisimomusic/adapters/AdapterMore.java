package com.durisimomusic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.durisimomusic.R;
import com.durisimomusic.data.models.More;

import java.util.ArrayList;

public class AdapterMore extends RecyclerView.Adapter<AdapterMore.ViewHolder> {

    private ArrayList<More> moreArrayList;
    private Context context;
    private int selectedItem=-1;


    public AdapterMore(ArrayList<More> moreArrayList, Context context) {
        this.moreArrayList = moreArrayList;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title;
        ImageView iv_menu_icon;
        RelativeLayout rl_title;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            iv_menu_icon = (ImageView) itemView.findViewById(R.id.iv_menu_icon);
            rl_title =  itemView.findViewById(R.id.rl_title);
        }
    }


    @Override
    public AdapterMore.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_more, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterMore.ViewHolder holder, int position) {
        More item = moreArrayList.get(position); holder.tv_title.setText(item.getTitle());
        holder.iv_menu_icon.setImageResource(item.getImage());

    }

    public void setSelectedItem(int selectedItem)
    {
        this.selectedItem = selectedItem;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return moreArrayList.size();
    }


}