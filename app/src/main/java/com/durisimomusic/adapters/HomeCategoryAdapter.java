package com.durisimomusic.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.durisimomusic.R;
import com.durisimomusic.data.models.HomeCategoryData;
import com.durisimomusic.data.models.asset.SongDetails;
import com.durisimomusic.data.models.music.MusicResponse;
import com.durisimomusic.data.remote.APIUtils;
import com.durisimomusic.fragments.CategoryViewAllFragment;
import com.durisimomusic.util.AppManageInterface;
import com.durisimomusic.util.AppUtil;
import com.durisimomusic.util.Const;
import com.durisimomusic.util.Prefs;
import com.durisimomusic.util.RecyclerViewPositionHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.ViewHolder> {

    private static Context context;
    private ArrayList<HomeCategoryData> homeCategoryData;
    private FragmentManager fragmentManager;
    private AppManageInterface appManageInterface;
    public static  AppCompatDialog dialog2;
    private int LIMIT = 20, CURRENT_PAGE = 1, LAST_PAGE = 100;
    private static AssetListingAdapter assetListingAdapter;
    private boolean loadingInProgress;
    private static ArrayList<SongDetails> data = new ArrayList<>();
   public static boolean isfromDialogMusic = true;

    public HomeCategoryAdapter(Context context,FragmentManager fragmentManager, ArrayList<HomeCategoryData> homeCategoryData) {
        this.context = context;
        this.appManageInterface = (AppManageInterface) context;
        this.homeCategoryData = homeCategoryData;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category, parent, false);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.width = (int) (parent.getWidth() / 4);
        view.setLayoutParams(layoutParams);
        return new HomeCategoryAdapter.ViewHolder(view,viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_category_name.setText(homeCategoryData.get(position).getCategoryName());
        holder.tv_parent_category_name.setText(homeCategoryData.get(position).getParentCategoryName());


        Glide.with(context)
                .load( homeCategoryData.get(position).getCategoryImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.app_logo)
                .error(R.drawable.app_logo)
                .centerCrop()
                .into(holder.iv_category);

        holder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(homeCategoryData,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeCategoryData.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_category_name,tv_parent_category_name;
        ImageView iv_category;
        RelativeLayout rl_main;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            tv_category_name = itemView.findViewById(R.id.tv_category_name);
            tv_parent_category_name = itemView.findViewById(R.id.tv_parent_category_name);
            iv_category = itemView.findViewById(R.id.iv_category);
            rl_main = itemView.findViewById(R.id.rl_main);
        }
    }

    private void openDialog(ArrayList<HomeCategoryData> homeCategoryData, int pos){
        dialog2 = new AppCompatDialog(context, R.style.dialogFullScreen);
        dialog2.setContentView(R.layout.dialog_view_all_music);
        RecyclerView listing = dialog2.findViewById(R.id.listing);
        NestedScrollView scrollView = dialog2.findViewById(R.id.scrollView);
        ImageView ivDrawerClosed = dialog2.findViewById(R.id.iv_drawer_closed);
        ContentLoadingProgressBar loader = dialog2.findViewById(R.id.loader);
        TextView no_data_found = dialog2.findViewById(R.id.no_data_found);
        RelativeLayout rl_tv_all = dialog2.findViewById(R.id.rl_tv_all);
        dialog2.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog2.setTitle("");
        dialog2.show();
        ivDrawerClosed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        CURRENT_PAGE = 1;
        LAST_PAGE = 1;
        LIMIT = 20;
        listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        listing.setHasFixedSize(true);
        isfromDialogMusic = true;
        assetListingAdapter = new AssetListingAdapter(context, fragmentManager, new ArrayList<>(), "Tracks");
        listing.setAdapter(assetListingAdapter);
        listing.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
        no_data_found.setVisibility(View.GONE);

        String path = null,type = null,title = null,id = null;
        path = String.valueOf(homeCategoryData.get(pos).getMenuId());
        type = "GridTracks";
        title = homeCategoryData.get(pos).getCategoryName();
        id = String.valueOf(homeCategoryData.get(pos).getCategoryId());

        RecyclerViewPositionHelper positionHelper = RecyclerViewPositionHelper.createHelper(listing);

        listing.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) listing.getLayoutManager();
                    if (layoutManager != null) {
                        int total = layoutManager.getItemCount();
                        int currentLastItem = positionHelper.findLastCompletelyVisibleItemPosition() + 1;

                        if (currentLastItem == (total - 10)) {
                            if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                                loadingInProgress = true;
                                CURRENT_PAGE++;
                                String path = null,type = null,title = null,id = null;
                                path = String.valueOf(homeCategoryData.get(pos).getMenuId());
                                type = "GridTracks";
                                title = homeCategoryData.get(pos).getCategoryName();
                                id = String.valueOf(homeCategoryData.get(pos).getCategoryId());
                                getGridMusic(listing,path,type,title,id,loader,no_data_found);
                            }
                        }
                    }
                }
            }
        });

        scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener)
                (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                            if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                                loadingInProgress = true;
                                CURRENT_PAGE++;
                                String path1 = null,type1 = null,title1 = null,id1 = null;
                                path1 = String.valueOf(homeCategoryData.get(pos).getMenuId());
                                type1 = "GridTracks";
                                title1 = homeCategoryData.get(pos).getCategoryName();
                                id1 = String.valueOf(homeCategoryData.get(pos).getCategoryId());
                                getGridMusic(listing,path1,type1,title1,id1,loader,no_data_found);
                            }
                        }
                    }
                });

        getGridMusic(listing,path,type,title,id,loader,no_data_found);

        rl_tv_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                appManageInterface.showFragment();
                CategoryViewAllFragment fragment = CategoryViewAllFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("path", String.valueOf(homeCategoryData.get(pos).getMenuId()));
                bundle.putString("type", "GridTracks");
                bundle.putString("title", homeCategoryData.get(pos).getCategoryName());
                bundle.putString("id", String.valueOf(homeCategoryData.get(pos).getCategoryId()));
                fragment.setArguments(bundle);
                AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
            }
        });
    }


    private void getGridMusic(RecyclerView listing, String path, String type, String title, String id,
                              ContentLoadingProgressBar loader, TextView noDataFound) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_music_detail = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_category_music(get_music_detail, path, id, "Tracks", LIMIT, CURRENT_PAGE).enqueue(new Callback<MusicResponse>() {
                @Override
                public void onResponse(@NonNull Call<MusicResponse> call, @NonNull Response<MusicResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                loadingInProgress = false;
                                assetListingAdapter.add(response.body().getData());
                                LAST_PAGE = response.body().getLastPage();
                                loader.setVisibility(View.GONE);
                                if (CURRENT_PAGE == 1) {
                                    listing.setVisibility(View.VISIBLE);
                                    noDataFound.setVisibility(View.GONE);
                                    loader.setVisibility(View.GONE);
                                } else {
                                    loadingInProgress = false;
                                    if (CURRENT_PAGE == 1) {
                                        loader.setVisibility(View.GONE);
                                        noDataFound.setVisibility(View.VISIBLE);
                                        listing.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                loadingInProgress = false;
                                if (CURRENT_PAGE == 1) {
                                    loader.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.VISIBLE);
                                    listing.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            loadingInProgress = false;
                            if (CURRENT_PAGE == 1) {
                                loader.setVisibility(View.GONE);
                                noDataFound.setVisibility(View.VISIBLE);
                                listing.setVisibility(View.GONE);

                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MusicResponse> call, @NonNull Throwable t) {
                    loadingInProgress = false;
                    if (CURRENT_PAGE == 1) {
                        loader.setVisibility(View.GONE);
                        noDataFound.setVisibility(View.VISIBLE);
                        listing.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }
            });
        } else {
            loadingInProgress = false;
            if (CURRENT_PAGE == 1) {
                loader.setVisibility(View.GONE);
                noDataFound.setVisibility(View.VISIBLE);
                listing.setVisibility(View.GONE);
            }
        }
    }

    public static  void onLoadSong(String songid) {
        Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, songid);
        if (data != null) {
            for (SongDetails item : data) {
                if (String.valueOf(item.getSongId()).equals(songid)) {
                    Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, songid);
                }
                Log.d("mytag", "onload song now playin : " + Prefs.getPrefInstance().getValue(context, Const.NOW_PLAYING, ""));
            }
            if (assetListingAdapter != null) {
                assetListingAdapter.notifyDataSetChanged();
            }
        }
    }
}
