package com.durisimomusic.adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.durisimomusic.R;
import com.durisimomusic.data.models.HomeCategoryData;
import com.durisimomusic.data.models.HomeData;
import com.durisimomusic.fragments.CategoryViewAllFragment;
import com.durisimomusic.util.AppManageInterface;
import com.durisimomusic.util.AppUtil;

import java.util.ArrayList;

public class HomeListingAdapter extends RecyclerView.Adapter<HomeListingAdapter.ViewHolder> {

    public static final int TYPE_CATEGORY = 0, TYPE_MUSIC = 1;
    private static Context context;
    private ArrayList<HomeData> homeData = new ArrayList<>();

    private FragmentManager fragmentManager;
    private AppManageInterface appManageInterface;

    public HomeListingAdapter(Context context, FragmentManager fragmentManager, ArrayList<HomeData> homeData) {
        this.context = context;
        this.homeData = homeData;
        this.fragmentManager = fragmentManager;
        appManageInterface = (AppManageInterface) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_CATEGORY)
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_home_category, parent, false), viewType);
        else
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_main_music, parent, false), viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getItemViewType(position) == TYPE_CATEGORY) {
                    ArrayList<HomeCategoryData> homeCategoryData;
                    homeCategoryData = homeData.get(position).getAssests();
                    int id = homeData.get(position).getCategoryId();
                    holder.title1.setText(homeData.get(position).getName());
                    if (homeCategoryData.size() == 0) {
                        holder.container1.setVisibility(View.GONE);
                        holder.rl_header1.setVisibility(View.GONE);
                        holder.recycler1.setVisibility(View.GONE);
                    }else{
                        holder.recycler1.setHasFixedSize(true);
                        holder.recycler1.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
                        HomeCategoryAdapter adapter = new HomeCategoryAdapter(context, fragmentManager, homeCategoryData);
                        holder.recycler1.setAdapter(adapter);

                        holder.view_all_category.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String menu_id = null;
                                try {
                                    for (int i = 0; i < homeData.size(); i++) {
                                        menu_id = String.valueOf(homeCategoryData.get(i).getMenuId());
                                    }

                                } catch (Exception e) {
                                }
                                appManageInterface.showFragment();
                                CategoryViewAllFragment fragment = CategoryViewAllFragment.newInstance();
                                Bundle bundle = new Bundle();
                                bundle.putString("path", menu_id);
                                bundle.putString("type", "GridTracksCategory");
                                bundle.putString("title", homeData.get(position).getName());
                                bundle.putString("id", String.valueOf(id));
                                fragment.setArguments(bundle);
                                AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
                            }
                        });
                    }
                }

                else if (getItemViewType(position) == TYPE_MUSIC) {
                    ArrayList<HomeCategoryData> homeMusicData;
                    holder.title.setText(homeData.get(position).getName());
                    homeMusicData = homeData.get(position).getAssests();
                   String music_name = homeData.get(position).getName();
                    int id = homeData.get(position).getCategoryId();

                    if (homeMusicData.size() == 0) {
                        holder.container.setVisibility(View.GONE);
                        holder.rl_header.setVisibility(View.GONE);
                        holder.recycler.setVisibility(View.GONE);
                    } else {
                        holder.recycler.setHasFixedSize(true);
                        holder.recycler.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
                        HomeMusicAdapter adapter = new HomeMusicAdapter(context, fragmentManager, homeMusicData);
                        holder.recycler.setAdapter(adapter);
                        holder.tv_view_all_music.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String menu_id = null;
                                try {
                                    for (int i = 0; i < homeData.size(); i++) {
                                        menu_id = String.valueOf(homeMusicData.get(i).getMenuId());
                                    }
                                } catch (Exception e) {
                                }
                                appManageInterface.showFragment();
                                CategoryViewAllFragment fragment = CategoryViewAllFragment.newInstance();
                                Bundle bundle = new Bundle();
                                bundle.putString("path", menu_id);
                                bundle.putString("type", "Tracks");
                                bundle.putString("title", music_name);
                                bundle.putString("id", String.valueOf(id));
                                fragment.setArguments(bundle);
                                AppUtil.setup_Fragment_with_bundle(fragmentManager, fragment, "Inner", "Inner", true);
                            }
                        });
                    }

                }
            }
        }, 200);
    }


    @Override
    public int getItemCount() {
        return homeData.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (homeData.get(position).getType().toLowerCase().equals("TracksCategory".toLowerCase())) {
            return TYPE_CATEGORY;
        } else return TYPE_MUSIC;
    }

//    public void add(ArrayList<HomeData> listingData) {
//        this.homeData.addAll(listingData);
//        notifyDataSetChanged();
//    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, title1, view_all_category, tv_view_all_music;
        RecyclerView recycler, recycler1;
        RelativeLayout rl_header,rl_header1;
        LinearLayout container,container1;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            title1 = itemView.findViewById(R.id.title1);
            view_all_category = itemView.findViewById(R.id.view_all_category);
            recycler1 = itemView.findViewById(R.id.recycler1);
            recycler = itemView.findViewById(R.id.recycler);
            rl_header = itemView.findViewById(R.id.rl_header);
            rl_header1 = itemView.findViewById(R.id.rl_header1);
            title = itemView.findViewById(R.id.title);
            tv_view_all_music = itemView.findViewById(R.id.tv_view_all_music);
            container = itemView.findViewById(R.id.container);
            container1 = itemView.findViewById(R.id.container1);
        }
    }
}
