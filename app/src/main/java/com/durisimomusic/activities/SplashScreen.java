package com.durisimomusic.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.durisimomusic.R;
import com.durisimomusic.application.Durisimo;
import com.durisimomusic.data.models.video.MenuStatusResponse;
import com.durisimomusic.data.remote.APIUtils;
import com.durisimomusic.util.AppUtil;
import com.durisimomusic.util.Const;
import com.durisimomusic.util.Prefs;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Objects;

import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {

    Handler handler;
    Runnable runnable;
    private Tracker mTracker;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        Const.isPopupClose = false;
        // Make Full Screen - Hide StatusBar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Make UI Full Screen
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        // Set StatusBar Color Transparent
        AppUtil.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        getWindow().setStatusBarColor(Color.TRANSPARENT);

        mTracker = Durisimo.getDefaultTracker();
        get_menu_status();
        FirebaseApp.initializeApp(SplashScreen.this);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Splash");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, bundle);

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                return;
            }

            // Get new Instance ID token
            String token = Objects.requireNonNull(task.getResult());
            Log.d("mytag", "fcm token------------------------------------ " + task.getResult());
            Prefs.getPrefInstance().setValue(this, Const.FCM_ID, token);
        });

        handler = new Handler();
        runnable = () -> {
            if (Prefs.getPrefInstance().getValue(SplashScreen.this, Const.APP_STATUS, "").equals("")) {
                startActivity(new Intent(SplashScreen.this, IntroSlider.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK));
            } else {
                if (Prefs.getPrefInstance().getValue(this, Const.LOGIN_ACCESS, "").equals(getString(R.string.logged_in))
                        || Prefs.getPrefInstance().getValue(this, Const.LOGIN_ACCESS, "").equals(getString(R.string.skipped)))
                    startActivity(new Intent(SplashScreen.this, HomeScreen.class));
                else
                    startActivity(new Intent(SplashScreen.this, Login.class));
//                startActivity(new Intent(SplashScreen.this,HomeScreen.class));
            }
            finish();
        };
        if (AppUtil.isInternetAvailable(SplashScreen.this)) {
            handler.postDelayed(runnable, 500);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Splash");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    private void get_menu_status() {
        if (AppUtil.isInternetAvailable(SplashScreen.this)) {
            APIUtils.getAPIService().get_menu_status().enqueue(new Callback<MenuStatusResponse>() {
                @Override
                public void onResponse(@NonNull Call<MenuStatusResponse> call, @NonNull Response<MenuStatusResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getLivetvMenuData() != null && !response.body().getLivetvMenuData().isEmpty()) {
                                if (response.body().getLivetvMenuData().get(0).getVisibleStatus())
                                    Prefs.getPrefInstance().setValue(SplashScreen.this, Const.MENU_LIVE_TV_STATUS, "visible");
                                else
                                    Prefs.getPrefInstance().setValue(SplashScreen.this, Const.MENU_LIVE_TV_STATUS, "gone");
                                Prefs.getPrefInstance().setValue(SplashScreen.this, Const.MENU_LIVE_TV_ID, response.body().getLivetvMenuData().get(0).getMenuId());
                            }
                            if (response.body().getRadioMenuData() != null && !response.body().getRadioMenuData().isEmpty()) {
                                if (response.body().getRadioMenuData().get(0).getVisibleStatus())
                                    Prefs.getPrefInstance().setValue(SplashScreen.this, Const.MENU_RADIO_STATUS, "visible");
                                else
                                    Prefs.getPrefInstance().setValue(SplashScreen.this, Const.MENU_RADIO_STATUS, "gone");
                                Prefs.getPrefInstance().setValue(SplashScreen.this, Const.MENU_RADIO_ID, response.body().getRadioMenuData().get(0).getMenuId());
                                Prefs.getPrefInstance().setValue(SplashScreen.this, Const.MENU_VIDEO, response.body().getRadioMenuData().get(0).getMenuId());
                            }
                        } else {
                        }
                    } else {
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MenuStatusResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                }
            });
        } else {
        }
    }
}
