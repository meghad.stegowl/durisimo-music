package com.durisimomusic.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.durisimomusic.R;
import com.durisimomusic.adapters.CarousalAdapter;
import com.durisimomusic.application.Durisimo;
import com.durisimomusic.data.models.common.ChatModal;
import com.durisimomusic.data.models.live_tv.LikeUnlikeResponse;
import com.durisimomusic.data.models.live_tv.LiveTvResponse;
import com.durisimomusic.data.models.user.SkipUserResponse;
import com.durisimomusic.data.models.video.MenuItemsResponse;
import com.durisimomusic.data.remote.APIUtils;
import com.durisimomusic.databinding.ActivityLiveTvBinding;
import com.durisimomusic.util.AppUtil;
import com.durisimomusic.util.Const;
import com.durisimomusic.util.Prefs;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_OUT_OF_MEMORY;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_REMOTE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_RENDERER;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_SOURCE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_UNEXPECTED;

public class LiveTv extends AppCompatActivity implements PlayerControlView.VisibilityListener, PlaybackPreparer {

    ActivityLiveTvBinding binding;
    private Handler handler;
    private Tracker mTracker;
    private SimpleExoPlayer player;
    private String playBackUrl;
    private String video_id;
    private String path, type, pageTitle;
    private FirebaseRecyclerAdapter adapter;
    private CarousalAdapter carousalAdapter;
    private Runnable runnable;
    private String shareDetails;

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        binding = ActivityLiveTvBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        mTracker = Durisimo.getDefaultTracker();

        type = getIntent().getStringExtra("type");
        path = getIntent().getStringExtra("path");
        init();

    }

    private void init() {
        binding.noDataFound.setVisibility(View.GONE);
        binding.loader.setVisibility(View.GONE);
        binding.videoOverlay.setVisibility(View.VISIBLE);
        getMessages();

        if (getIntent() != null && getIntent().hasExtra("path")) {
            if (Prefs.getPrefInstance().getValue(LiveTv.this, Const.TOKEN, "") != null && !Prefs.getPrefInstance().getValue(LiveTv.this, Const.TOKEN, "").isEmpty())
            { getCarousal();
            call_video();}
            else callSkipUser();
        } else {
            View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(d -> {
                dialog.dismiss();
                finish();
            });
        }

        binding.fav.setOnClickListener(view -> {
            calLikeUnlike();
        });
        binding.share.setOnClickListener(view13 -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareDetails);
            sendIntent.setType("text/plain");
            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);
        });
        binding.back.setOnClickListener(view ->{
            Intent intent = new Intent();
            intent.putExtra("playBackUrl", playBackUrl);
            setResult(RESULT_OK, intent);
            finish();
            onBackPressed();
        });
        binding.sendMessage.setOnClickListener(view -> {
            String msg = binding.messageInput.getText().toString().trim();
            if (!TextUtils.isEmpty(msg)) {
                if (AppUtil.isInternetAvailable(LiveTv.this)) {
                    String user_id = "";
                    String name;
                    String path = "User";
                    String profile_pic = "";
                    String type = "user";
                    if (Prefs.getPrefInstance().getValue(LiveTv.this, Const.USER_NAME, "") != null && !Prefs.getPrefInstance().getValue(LiveTv.this, Const.USER_NAME, "").isEmpty())
                        name = Prefs.getPrefInstance().getValue(LiveTv.this, Const.USER_NAME, "");
                    else name = "User";

                    if (Prefs.getPrefInstance().getValue(LiveTv.this, Const.PROFILE_IMAGE, "") != null && !Prefs.getPrefInstance().getValue(LiveTv.this, Const.PROFILE_IMAGE, "").isEmpty())
                        profile_pic = Prefs.getPrefInstance().getValue(LiveTv.this, Const.PROFILE_IMAGE, "");
//                    else profile_pic = "http://admin.highlightmusicgroup.com/assets/admin/user.png";
                    else profile_pic = "http://54.226.159.242/durisimomusicadmin/assets/images/avtar-profile.png";

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference();
                    DatabaseReference databaseReference = myRef.child(path).push();
                    Map<String, Object> map = new HashMap<>();
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a");
                    sdf.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                    sdfTime.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                    long time = System.currentTimeMillis();
                    String date = sdf.format(time);
                    String currentTime = sdfTime.format(time);
                    map.put("comment", binding.messageInput.getText().toString());
                    map.put("commentByName", name);
                    map.put("commentDate", date);
                    map.put("commentImageURL", profile_pic);
                    map.put("time", currentTime);
                    map.put("type", type);
                    databaseReference.setValue(map);
                    binding.messageInput.getText().clear();
                } else {
                    Toast.makeText(LiveTv.this, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(LiveTv.this, "Please Enter Some Text", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMessages() {
        binding.messageInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                ((InputMethodManager) Objects.requireNonNull(getSystemService(Activity.INPUT_METHOD_SERVICE))).hideSoftInputFromWindow(v.getWindowToken(), 0);
                binding.sendMessage.performClick();
                return true;
            }
            return false;
        });
        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference();
        String path = "User";

        Query query = myRef.child(path);

        FirebaseRecyclerOptions<ChatModal> options = new FirebaseRecyclerOptions.Builder<ChatModal>()
                .setQuery(query, snapshot -> {
                    String time = "", date = "", image = "", name = "", comment = "", type = "";
                    if (snapshot.hasChild("time") && snapshot.child("time").getValue() != null)
                        time = snapshot.child("time").getValue().toString();
                    if (snapshot.hasChild("commentDate") && snapshot.child("commentDate").getValue() != null)
                        date = snapshot.child("commentDate").getValue().toString();
                    if (snapshot.hasChild("commentImageURL") && snapshot.child("commentImageURL").getValue() != null)
                        image = snapshot.child("commentImageURL").getValue().toString();
                    if (snapshot.hasChild("commentByName") && snapshot.child("commentByName").getValue() != null)
                        name = snapshot.child("commentByName").getValue().toString();
                    if (snapshot.hasChild("comment") && snapshot.child("comment").getValue() != null)
                        comment = snapshot.child("comment").getValue().toString();
                    if (snapshot.hasChild("type") && snapshot.child("type").getValue() != null)
                        type = snapshot.child("type").getValue().toString();
                    return new ChatModal(time, date, image, name, comment, type);
                })
                .build();

        adapter = new FirebaseRecyclerAdapter<ChatModal, ViewHolder>(options) {
            @NotNull
            @Override
            public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_comment, parent, false));
            }

            @Override
            protected void onBindViewHolder(@NotNull ViewHolder holder, final int position, @NotNull ChatModal model) {
                if (model.getType().equals("user")) {
                    holder.adminItemContainer.setVisibility(View.GONE);
                    holder.adminView.setVisibility(View.GONE);
                    holder.userItemContainer.setVisibility(View.VISIBLE);
                    holder.userView.setVisibility(View.VISIBLE);
                    holder.userComment.setText(model.getComment());
                    holder.userName.setText(model.getCommentByName());
                    holder.userCommentTime.setText(model.getTime());
                    Glide
                            .with(Durisimo.applicationContext)
                            .load(model.getImage())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .thumbnail(0.25f)
                            .centerCrop()
                            .error(R.drawable.ic_profile_icon)
                            .placeholder(R.drawable.ic_profile_icon)
                            .into(holder.userImage);
                } else {
                    holder.adminItemContainer.setVisibility(View.VISIBLE);
                    holder.adminView.setVisibility(View.VISIBLE);
                    holder.userItemContainer.setVisibility(View.GONE);
                    holder.userView.setVisibility(View.GONE);
                    holder.adminComment.setText(model.getComment());
                    holder.adminCommentTime.setText(model.getTime());
                }


            }
        };
        binding.listing.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.listing.setLayoutManager(linearLayoutManager);
        binding.listing.setAdapter(adapter);
        adapter.startListening();
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                binding.listing.postDelayed(() -> binding.listing.scrollToPosition(adapter.getItemCount() - 1), 1000);
            }

            @Override
            public void onCancelled(@NotNull DatabaseError error) {
                // Failed to read value
            }
        });
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout userItemContainer;
        LinearLayout adminItemContainer;
        ImageView userImage;
        TextView userName;
        TextView userComment;
        TextView userCommentTime;
        TextView adminCommentTime;
        TextView adminComment;
        View adminView;
        View userView;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            userItemContainer = itemView.findViewById(R.id.user_item_container);
            adminItemContainer = itemView.findViewById(R.id.admin_item_container);
            userImage = itemView.findViewById(R.id.user_image);
            userName = itemView.findViewById(R.id.user_name);
            userComment = itemView.findViewById(R.id.user_comment);
            userCommentTime = itemView.findViewById(R.id.user_comment_time);
            adminCommentTime = itemView.findViewById(R.id.admin_comment_time);
            adminComment = itemView.findViewById(R.id.admin_comment);
            adminView = itemView.findViewById(R.id.admin_view);
            userView = itemView.findViewById(R.id.user_view);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Video Player");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        if (binding.playerView != null) {
            binding.playerView.onResume();
            if (player != null)
                player.setPlayWhenReady(true);
        }
    }

    private void initializePlayer() {
        binding.videoOverlay.setVisibility(View.GONE);
        player = new SimpleExoPlayer.Builder(LiveTv.this).build();
        binding.playerView.setPlayer(player);
        binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        binding.playerView.setUseController(true);
        player.setPlayWhenReady(true);
        player.seekTo(0);

        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.CONTENT_TYPE_MOVIE)
                .build();
        if (player.getAudioComponent() != null)
            player.getAudioComponent().setAudioAttributes(audioAttributes, true);
        try {
            if (!playBackUrl.equals("")) {
                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(LiveTv.this, Util.getUserAgent(LiveTv.this, getResources().getString(R.string.app_name)));
                MediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(playBackUrl));
                player.prepare(videoSource, true, true);
            }
        } catch (Exception e) {

        }
        player.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                switch (error.type) {
                    case TYPE_SOURCE:
                        binding.videoOverlay.setVisibility(View.VISIBLE);
                        break;
                    case TYPE_RENDERER:
                    case TYPE_UNEXPECTED:
                        player.retry();
                        break;
                    case TYPE_OUT_OF_MEMORY:
                    case TYPE_REMOTE:
                        AppUtil.show_Snackbar(LiveTv.this, binding.playerView, error.getMessage(), true);
                        break;
                }
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onVisibilityChange(int visibility) {
    }

    @Override
    protected void onStart() {
        super.onStart();
        binding.playerView.onResume();
        if (player != null)
            player.setPlayWhenReady(true);
    }

    @Override
    protected void onPause() {
        binding.playerView.onPause();
        if (player != null) {
            player.setPlayWhenReady(false);
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        binding.playerView.onPause();
        if (player != null)
            player.setPlayWhenReady(false);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        binding.playerView.onPause();
        if (player != null) {
            player.release();
            player = null;
        }
        super.onDestroy();
    }

    @Override
    public void preparePlayback() {
    }

    private void call_video() {
        if (AppUtil.isInternetAvailable(LiveTv.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(LiveTv.this, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_menu_items = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_menu_items(call_menu_items, path, type).enqueue(new Callback<MenuItemsResponse>() {
                @Override
                public void onResponse(@NonNull Call<MenuItemsResponse> call, @NonNull Response<MenuItemsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                playBackUrl = response.body().getData().get(0).getVideosLink();
                                shareDetails = "Video Url: "+ playBackUrl +"\n"+ "App Name: "+ getResources().getString(R.string.app_name);
                                video_id = response.body().getData().get(0).getVideosId();
                                binding.likeCount.setText(response.body().getData().get(0).getVideoLikeCount());
                                binding.fav.setSelected(response.body().getData().get(0).getVideoLikeStatus());
                                initializePlayer();
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFound.setVisibility(View.GONE);
                            } else {
                                binding.noDataFound.setVisibility(View.GONE);
                                binding.loader.setVisibility(View.GONE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(LiveTv.this, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            binding.noDataFound.setVisibility(View.GONE);
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MenuItemsResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.noDataFound.setVisibility(View.GONE);
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            binding.noDataFound.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void getCarousal() {
        if (AppUtil.isInternetAvailable(LiveTv.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(LiveTv.this, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_live_tv_slider = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_live_tv_slider(get_live_tv_slider).enqueue(new Callback<LiveTvResponse>() {
                @Override
                public void onResponse(@NonNull Call<LiveTvResponse> call, @NonNull Response<LiveTvResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                carousalAdapter = new CarousalAdapter(LiveTv.this, response.body().getData());
                                binding.carousal.setAdapter(carousalAdapter);
                                binding.carousal.setOffscreenPageLimit(response.body().getData().size());
                                binding.carousal.setCurrentItem(0, true);
                                binding.carousal.getChildAt(0).setOverScrollMode(View.OVER_SCROLL_NEVER);

                                final int speedScroll = 5000;
                                handler = new Handler();
                                runnable = new Runnable() {
                                    int count = 0;
                                    boolean flag = true;

                                    @Override
                                    public void run() {
                                        if (count < carousalAdapter.getItemCount()) {
                                            if (count == carousalAdapter.getItemCount() - 1) {
                                                flag = false;
                                            } else if (count == 0) {
                                                flag = true;
                                            }
                                            if (flag) count++;
                                            else count--;
                                            binding.carousal.setCurrentItem(count, true);
                                            handler.postDelayed(this, speedScroll);
                                        }
                                    }
                                };
                                handler.postDelayed(runnable, speedScroll);
                                binding.carousal.setVisibility(View.VISIBLE);
                            } else {
                                binding.carousal.setVisibility(View.GONE);
                            }
                        } else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(LiveTv.this, Login.class);
                            startActivity(i);
                            finish();
                        }
                    } else {
                        binding.carousal.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LiveTvResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.carousal.setVisibility(View.GONE);
                }
            });
        } else {
            binding.carousal.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(LiveTv.this.getResources().getString(R.string.no_internet_connection));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);

            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                dialog.dismiss();
            });

        }
    }

    private void calLikeUnlike() {
        if (AppUtil.isInternetAvailable(LiveTv.this)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("videos_id", video_id);
                jsonObject.put("token", Prefs.getPrefInstance().getValue(LiveTv.this, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody share_my_app = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_like_unlike(share_my_app).enqueue(new Callback<LikeUnlikeResponse>() {
                @Override
                public void onResponse(@NonNull Call<LikeUnlikeResponse> call, @NonNull Response<LikeUnlikeResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                                binding.likeCount.setText(response.body().getVideosLikeCount());
                                binding.fav.setSelected(response.body().getVideosLikeStatus());
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(LiveTv.this, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LikeUnlikeResponse> call, @NonNull Throwable t) {
                    View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void callSkipUser() {
        if (AppUtil.isInternetAvailable(LiveTv.this)) {
            JSONObject jsonObject = new JSONObject();
            try {
                String device_type = "ANDROID";
                jsonObject.put("fcm_id", Prefs.getPrefInstance().getValue(LiveTv.this, Const.FCM_ID, ""));
                jsonObject.put("device", device_type);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody user_logging_in = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().skip_user(user_logging_in).enqueue(new Callback<SkipUserResponse>() {
                @Override
                public void onResponse(@NonNull Call<SkipUserResponse> call, @NonNull Response<SkipUserResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            Prefs.getPrefInstance().setValue(LiveTv.this, Const.LOGIN_ACCESS, getString(R.string.live_tv));
                            Prefs.getPrefInstance().setValue(LiveTv.this, Const.USER_ID, response.body().getUserId());
                            Prefs.getPrefInstance().setValue(LiveTv.this, Const.TOKEN, response.body().getToken());
                            getCarousal();
                            call_video();
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(LiveTv.this, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SkipUserResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(LiveTv.this).inflate(AppUtil.setLanguage(LiveTv.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(LiveTv.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }
}
