package com.durisimomusic.activities;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;

import com.durisimomusic.R;
import com.durisimomusic.Services.FloatingWidgetService;
import com.durisimomusic.adapters.AdapterMainviewPager;
import com.durisimomusic.adapters.ExistingPlaylistAdapter;
import com.durisimomusic.adapters.HomeCategoryAdapter;
import com.durisimomusic.adapters.HomeListingAdapter;
import com.durisimomusic.adapters.VideoListingAdapter;
import com.durisimomusic.application.Durisimo;
import com.durisimomusic.data.models.BannerSlider;
import com.durisimomusic.data.models.BannerSliderData;
import com.durisimomusic.data.models.Home;
import com.durisimomusic.data.models.adView.ADView;
import com.durisimomusic.data.models.asset.SongDetails;
import com.durisimomusic.data.models.home.HomeItemsResponse;
import com.durisimomusic.data.models.instagram.InstagramResponse;
import com.durisimomusic.data.models.music.AddPlaylistResponse;
import com.durisimomusic.data.models.music.AddPlaylistSongsResponse;
import com.durisimomusic.data.models.music.ExistingPlaylistResponse;
import com.durisimomusic.data.models.music.FavSongResponse;
import com.durisimomusic.data.models.music.SongPlayResponse;
import com.durisimomusic.data.models.music.TotalSharedResponse;
import com.durisimomusic.data.models.video.FavVideosResponse;
import com.durisimomusic.data.models.video.MenuItemsData;
import com.durisimomusic.data.models.video.VideoDetailsResponse;
import com.durisimomusic.data.remote.APIUtils;
import com.durisimomusic.databinding.ActivityHomeScreenBinding;
import com.durisimomusic.fragments.AssetDetailFragment;
import com.durisimomusic.fragments.CategoryViewAllFragment;
import com.durisimomusic.fragments.GridMusicFragment;
import com.durisimomusic.fragments.LiveTvFragment;
import com.durisimomusic.fragments.MenuFragment;
import com.durisimomusic.fragments.NowPlayingFragment;
import com.durisimomusic.fragments.PlaylistFavListingFragment;
import com.durisimomusic.fragments.RadioFragment;
import com.durisimomusic.fragments.SearchFragment;
import com.durisimomusic.fragments.VideoFragment;
import com.durisimomusic.playerManager.MediaController;
import com.durisimomusic.playerManager.MusicPreference;
import com.durisimomusic.playerManager.NotificationManager;
import com.durisimomusic.util.AppManageInterface;
import com.durisimomusic.util.AppUtil;
import com.durisimomusic.util.Const;
import com.durisimomusic.util.OnSwipeTouchListener;
import com.durisimomusic.util.Prefs;
import com.github.florent37.viewanimator.ViewAnimator;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.material.textfield.TextInputEditText;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.durisimomusic.fragments.LiveTvFragment.closeLoveTvPlayer;
import static com.durisimomusic.fragments.LiveTvFragment.isLiveTVShareClicked;
import static com.durisimomusic.fragments.VideoFragment.isFromVideoFragment;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_OUT_OF_MEMORY;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_REMOTE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_RENDERER;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_SOURCE;
import static com.google.android.exoplayer2.ExoPlaybackException.TYPE_UNEXPECTED;

public class HomeScreen extends AppCompatActivity implements AppManageInterface, NotificationManager.NotificationCenterDelegate,
        PlayerControlView.VisibilityListener, PlaybackPreparer {

    ActivityHomeScreenBinding binding;
    ActionBarDrawerToggle drawerToggle;
    String type;
    private Tracker mTracker;
    private Context context;
    private boolean doubleBackToExitPressedOnce;
    private String instagramPath;
    private String videoType;
    private static SimpleExoPlayer player;
    private SimpleExoPlayer popupPlayer;
    private String playBackUrl;
    private ArrayList<SongDetails> carousalData = new ArrayList<>();
    private ArrayList<MenuItemsData> menuItemsData = new ArrayList<>();
    private boolean isGoingDown = false;
    private int LIMIT = 20, CURRENT_PAGE = 1, LAST_PAGE = 100;
    private boolean loadingInProgress;
    private VideoListingAdapter videoListingAdapter;
    private int selectedPosition = 0;
    private ArrayList<BannerSliderData> bannerSliderData = new ArrayList<>();
    AdapterMainviewPager adapterMainviewPager;
    public static boolean isMusicopen, isVideoOpen;
    private SongDetails songDetail = null;
    String isfrom, isfromvideo;
    private ImageView imgViewChangeScreenOrientation;
    public static boolean isPipModeEnabled = true;
    public static boolean islivetvBackClicked = false;
    private static int playCount = 4;

    public static AdRequest adRequest;
    boolean homevisible;
    public static InterstitialAd mInterstitialAd;
    private final Object progressTimerSync = new Object();
    private final Object sync = new Object();
    private Timer progressTimer = null;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        binding = ActivityHomeScreenBinding.inflate(getLayoutInflater());
        FloatingWidgetService.hidePipPlayer();

        View view = binding.getRoot();
        setContentView(view);
        mTracker = Durisimo.getDefaultTracker();
        context = HomeScreen.this;
//        MobileAds.initialize(context, "ca-app-pub-3964617377343461~2181146561");

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                homevisible = true;
                displayInterstitial();
            }
        });
        homevisible = true;
        displayInterstitial();

//        MobileAds.initialize(HomeScreen.this, context.getResources().getString(R.string.ads_app_id));
        binding.home.setSelected(true);
        askpermisiion();
        isfrom = getIntent().getStringExtra("isfrom");
        isfromvideo = getIntent().getStringExtra("isfromvideo");
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (!getSupportFragmentManager().getFragments().isEmpty()) {
                count = getSupportFragmentManager().getFragments().size() - 1;
                Fragment fragment = getSupportFragmentManager().getFragments().get(count);
                fragment.onResume();
            }
        });
        if (!Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_LIVE_TV_STATUS, "").isEmpty()
                && Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_LIVE_TV_STATUS, "") != null
                && Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_LIVE_TV_STATUS, "".toLowerCase()).equals("visible"))
            binding.liveTvPopup.setVisibility(View.VISIBLE);
        else binding.liveTvPopup.setVisibility(View.GONE);
        if (!Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_STATUS, "").isEmpty()
                && Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_STATUS, "") != null
                && Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_STATUS, "".toLowerCase()).equals("visible"))
            binding.radio.setVisibility(View.VISIBLE);
        else binding.radio.setVisibility(View.GONE);
        if (isMusicopen || isVideoOpen) {
            binding.ivMiniUper.setVisibility(View.VISIBLE);
            binding.ivPopupUper.setVisibility(View.VISIBLE);
            binding.ivPopupBottom.setVisibility(View.GONE);
            binding.ivMiniBottom.setVisibility(View.GONE);
        } else {
            binding.ivPopupBottom.setVisibility(View.GONE);
            binding.ivMiniBottom.setVisibility(View.GONE);
            binding.ivPopupUper.setVisibility(View.GONE);
            binding.ivMiniUper.setVisibility(View.GONE);
            binding.popupPlayerController.setVisibility(View.GONE);
            binding.miniPlayerContainer.setVisibility(View.GONE);
        }

        binding.mainScreen.setVisibility(View.GONE);
        binding.popupPlayerContainer.setVisibility(View.VISIBLE);
        binding.songPlayPauseMini.setSelected(false);
        binding.songPlayPausePopup.setSelected(false);
        binding.songPlayPauseMini.setVisibility(View.VISIBLE);
        binding.songPlayPausePopup.setVisibility(View.VISIBLE);
        binding.songLoaderMini.setVisibility(View.GONE);
        binding.songLoaderPopup.setVisibility(View.GONE);
        binding.popupPlayerControlsContainer.setVisibility(View.VISIBLE);
        binding.popupVideoContainer.setVisibility(View.GONE);
        binding.drawerContainer.setAlpha(1);
        binding.noDataFoundSuggesions.setVisibility(View.GONE);
        binding.videoOverlay.setVisibility(View.VISIBLE);

        String video_image = Prefs.getPrefInstance().getValue(context, Const.VIDEO_IMAGE, "");

        Const.isSwipe = false;
        binding.songRepeat.setSelected(MusicPreference.getRepeat(HomeScreen.this) == 1);
        MediaController.repeatMode = binding.songRepeat.isSelected() ? 1 : 0;
        MediaController.repeatMode = binding.songRepeatMini.isSelected() ? 1 : 0;

        binding.songRepeatMini.setSelected(MusicPreference.getRepeat(HomeScreen.this) == 1);
        MediaController.repeatMode = binding.songRepeat.isSelected() ? 1 : 0;
        MediaController.repeatMode = binding.songRepeatMini.isSelected() ? 1 : 0;

        binding.songShuffle.setSelected(MusicPreference.getShuffle(HomeScreen.this));
        MediaController.shuffleMusic = binding.songShuffle.isSelected();
        MediaController.shuffleMusic = binding.songShuffleMini.isSelected();
        MediaController.shuffleList(MusicPreference.playlist);

        binding.songShuffleMini.setSelected(MusicPreference.getShuffle(HomeScreen.this));
        MediaController.shuffleMusic = binding.songShuffle.isSelected();
        MediaController.shuffleMusic = binding.songShuffleMini.isSelected();
        MediaController.shuffleList(MusicPreference.playlist);

        binding.songSeekBarPopup.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    songSeekBarMini.setProgress(progress, true);
                } else {
//                    songSeekBarMini.setProgress(progress);
                }
                MediaController.getInstance().seekToProgress(MediaController.getInstance().getPlayingSongDetail(), (float) progress / 100);
            }
        });

        binding.songSeekBarPopupMini.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    songSeekBarMini.setProgress(progress, true);
                } else {
//                    songSeekBarMini.setProgress(progress);
                }
                MediaController.getInstance().seekToProgress(MediaController.getInstance().getPlayingSongDetail(), (float) progress / 100);
            }
        });

        show_dismiss_ProgressLoader(View.GONE);
        clickMethods();
        getGoogleAdView();
        getSliderData();
        getMainHomeData();
        get_instagram();

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(Prefs.getPrefInstance().getValue(context, Const.Google_add_key, ""));
        android.util.Log.d("mytag", "prefr banner add id is ---" + Prefs.getPrefInstance().getValue(context, Const.Google_add_key, ""));
//        adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");

        AdRequest adRequest = new AdRequest.Builder().build();
        // Add the AdView to the view hierarchy.
        adView.loadAd(adRequest);

        binding.adViewContainer.addView(adView);

        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                binding.adViewContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
                binding.adViewContainer.setVisibility(View.GONE);
                android.util.Log.d("mytag", "Ad is closed");
            }

            public void onAdFailedToLoad(int errorCode) {
                android.util.Log.d("mytag", "Ad is Failed to load");
            }

            public void onAdLeftApplication() {
                binding.adViewContainer.setVisibility(View.GONE);
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                android.util.Log.d("mytag", "Ad is opened");
            }
        });

        imgViewChangeScreenOrientation = binding.playerViewVideo.findViewById(R.id.exo_fullscreen_icon);
        imgViewChangeScreenOrientation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.setPlayWhenReady(false);
                player.release();
                String url = Prefs.getPrefInstance().getValue(context, Const.VIDEO_URL, "");
                Intent serviceIntent = new Intent(context, FloatingWidgetService.class);
                serviceIntent.putExtra("videoUri", url);
                serviceIntent.putExtra("isfrom", "video");
                startService(serviceIntent);
            }
        });

        if (isfrom != null) {
            if (isfrom.equals("livetv")) {
                Log.d("mytag", "hreeeeeeeeeeeeeeeee isss1111111111111111111111");
                AppUtil.setup_Fragment(getSupportFragmentManager(), LiveTvFragment.newInstance(),
                        "", "", "", "", "LiveTv", "LiveTv", true);
                showFragment();
            }
        }

        if (isfromvideo != null) {
            if (isfromvideo.equals("isfromvideo")) {
                FloatingWidgetService.hidePipPlayer();
                isPipModeEnabled = true;
                String pos = Prefs.getPrefInstance().getValue(context, Const.VIDEO_POSITION, "");
                String type = Prefs.getPrefInstance().getValue(context, Const.VIDEO_TYPE, "");
                int position = Integer.parseInt(pos);

                ArrayList<MenuItemsData> listGet = new ArrayList<>();
                listGet = Prefs.getPrefInstance().loadSharedPreferencesLogList(context);

                Log.e("mytag", "Get ArrayList size" + listGet.size());
                if (listGet != null && listGet.size() != 0) {
                    showMotionVideo(listGet.get(position), listGet, position, type);
                }
            }
        }

        if (!Const.isPopupClose)
            getPopupVideo();

        drawerToggle = new ActionBarDrawerToggle(HomeScreen.this, binding.drawerContainer, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                binding.drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (slideOffset <= 1.0 && slideOffset > 0.999) {
                    if (Prefs.getPrefInstance().getValue(HomeScreen.this, Const.LOGIN_ACCESS, "").equals(getResources().getString(R.string.logged_in))) {
//                        getProfile();
                    }
                }
            }
        };
        binding.drawerContainer.addDrawerListener(drawerToggle);
//        drawerContainer.setScrimColor(getResources().getColor(R.color.cod_gray_trans_light));
        binding.drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerToggle.syncState();

        binding.miniPlayerContainer.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeTop() {
                animateOpenPopUpPlayer(true, songDetail);
            }
        });

        binding.playerViewVideo.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeTop() {
                if (Const.isSwipe) {
                    if (binding.motionContainer.getCurrentState() == R.id.end) {
                        Log.d("mytag", "here is player view video open------------------");
                        isFromVideoFragment = true;
                        isPipModeEnabled = false;
                        isGoingDown = false;
                        binding.drawerContainer.setAlpha(1.0f);
                        binding.drawerContainer.setVisibility(View.VISIBLE);
                        binding.drawerContainer.animate().alpha(0.0f).setDuration(1000).withEndAction(() -> binding.drawerContainer.setVisibility(View.GONE)).start();
                        binding.motionContainer.transitionToStart();
                        binding.playerViewVideo.setUseController(true);
                    }
                }
            }
        });

        binding.motionContainer.setTransitionListener(new MotionLayout.TransitionListener() {
            @Override
            public void onTransitionStarted(MotionLayout motionLayout, int i, int i1) {
            }

            @Override
            public void onTransitionChange(MotionLayout motionLayout, int i, int i1, float v) {
            }

            @Override
            public void onTransitionCompleted(MotionLayout motionLayout, int i) {


            }

            @Override
            public void onTransitionTrigger(MotionLayout motionLayout, int i, boolean b, float v) {

            }
        });
    }

    public static void closeVideoTVPlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.release();
            player = null;
        }
    }

    public void InterstitialAdmob() {
        String android_interstitial = Prefs.getPrefInstance().getValue(context, Const.INTERESTITIAL_KEY, "");
        InterstitialAd.load(HomeScreen.this, android_interstitial, adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {

                    @Override
                    public void onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent();
                        Log.d("mytag", "The ad was dismissed.");
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(com.google.android.gms.ads.AdError adError) {
                        super.onAdFailedToShowFullScreenContent(adError);
                        Log.d("mytag", "The ad failed to show.");
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        super.onAdShowedFullScreenContent();
                        mInterstitialAd = null;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                Log.d("mytag", "onAdFailedToLoad called.");
                mInterstitialAd = null;
            }
        });
    }

    private void displayInterstitial() {
        adRequest = new AdRequest.Builder().build();
        InterstitialAdmob();

        if (mInterstitialAd != null) {
            mInterstitialAd.show(HomeScreen.this);
        }
    }

    private void clickMethods() {
        binding.songPlayPauseMini.setOnClickListener(view -> {
            if (AppUtil.isInternetAvailable(this)) {
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    if (MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                    } else {
                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                } else {

                }
            } else {
                AppUtil.show_Snackbar(HomeScreen.this, binding.frameContainer, getResources().getString(R.string.no_internet_connection), false);
            }
        });
        binding.songPlayPausePopup.setOnClickListener(view -> {
            if (AppUtil.isInternetAvailable(this)) {
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    if (MediaController.getInstance().getPlayingSongDetail().getType() == null || (MediaController.getInstance().getPlayingSongDetail().getType() != null && !MediaController.getInstance().getPlayingSongDetail().getType().toLowerCase().equals(getResources().getString(R.string.radio).toLowerCase()))) {
                        if (MediaController.getInstance().setSongList != null && !MediaController.getInstance().setSongList.isEmpty()) {
                            if (MediaController.getInstance().isAudioPaused()) {
                                MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                            } else {
                                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                            }
                        } else {
                            MediaController.getInstance().setPlaylist(carousalData, carousalData.get(0));
                        }
                    } else {
                        MediaController.getInstance().setPlaylist(carousalData, carousalData.get(0));
                    }
                } else {
                    MediaController.getInstance().setPlaylist(carousalData, carousalData.get(0));
                }
            } else {
                AppUtil.show_Snackbar(HomeScreen.this, binding.frameContainer, getResources().getString(R.string.no_internet_connection), false);
            }
        });
        binding.songShuffle.setOnClickListener(view -> {
            binding.songShuffle.setSelected(!binding.songShuffle.isSelected());
            binding.songShuffle.setSelected(!binding.songShuffleMini.isSelected());
            MediaController.shuffleMusic = binding.songShuffle.isSelected();
            MediaController.shuffleMusic = binding.songShuffleMini.isSelected();
            MusicPreference.setShuffle(HomeScreen.this, (binding.songShuffle.isSelected()));
            MusicPreference.setShuffle(HomeScreen.this, (binding.songShuffleMini.isSelected()));
            if (binding.songShuffle.isSelected())
                MediaController.shuffleList(MusicPreference.playlist);
        });
        binding.songShuffleMini.setOnClickListener(view -> {
            binding.songShuffleMini.setSelected(!binding.songShuffleMini.isSelected());
            binding.songShuffle.setSelected(!binding.songShuffle.isSelected());
            MediaController.shuffleMusic = binding.songShuffleMini.isSelected();
            MediaController.shuffleMusic = binding.songShuffle.isSelected();
            MusicPreference.setShuffle(HomeScreen.this, (binding.songShuffleMini.isSelected()));
            MusicPreference.setShuffle(HomeScreen.this, (binding.songShuffle.isSelected()));
            if (binding.songShuffleMini.isSelected())
                MediaController.shuffleList(MusicPreference.playlist);
        });
        binding.songRepeat.setOnClickListener(view -> {
            binding.songRepeat.setSelected(!binding.songRepeat.isSelected());
            binding.songRepeatMini.setSelected(!binding.songRepeatMini.isSelected());
            MediaController.repeatMode = binding.songRepeat.isSelected() ? 1 : 0;
            MediaController.repeatMode = binding.songRepeatMini.isSelected() ? 1 : 0;
            MusicPreference.setRepeat(HomeScreen.this, (binding.songRepeat.isSelected() ? 1 : 0));
            MusicPreference.setRepeat(HomeScreen.this, (binding.songRepeatMini.isSelected() ? 1 : 0));
        });
        binding.songRepeatMini.setOnClickListener(view -> {
            binding.songRepeat.setSelected(!binding.songRepeat.isSelected());
            binding.songRepeatMini.setSelected(!binding.songRepeatMini.isSelected());
            MediaController.repeatMode = binding.songRepeat.isSelected() ? 1 : 0;
            MediaController.repeatMode = binding.songRepeatMini.isSelected() ? 1 : 0;
            MusicPreference.setRepeat(HomeScreen.this, (binding.songRepeat.isSelected() ? 1 : 0));
            MusicPreference.setRepeat(HomeScreen.this, (binding.songRepeatMini.isSelected() ? 1 : 0));
        });
        binding.songNextMini.setOnClickListener(view -> {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                MediaController.getInstance().playNextSong();
            }
        });
        binding.songNextPopup.setOnClickListener(view -> {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                MediaController.getInstance().playNextSong();
            }
        });
        binding.songPreviousMini.setOnClickListener(view -> {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                MediaController.getInstance().playPreviousSong();
            }
        });
        binding.songPreviousPopup.setOnClickListener(view -> {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                MediaController.getInstance().playPreviousSong();
            }
        });

        binding.ivPopupUper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.ivPopupBottom.setVisibility(View.VISIBLE);
                binding.ivPopupUper.setVisibility(View.GONE);
                if (binding.popupPlayerController.getVisibility() == View.VISIBLE) {
                    isMusicopen = true;
                    isVideoOpen = false;
                    expand(binding.popupPlayerController, 2000, 30);
                    binding.popupPlayerController.setVisibility(View.GONE);
                }
                if (binding.playerViewVideo.getVisibility() == View.VISIBLE && isVideoOpen) {
                    isVideoOpen = true;
                    isMusicopen = false;
                    binding.playerViewVideo.setVisibility(View.GONE);
                    binding.playerViewVideo.setUseController(false);
                    binding.playerViewVideo.hideController();
                    binding.color.setVisibility(View.GONE);
                    binding.playerControllerContainer.setVisibility(View.GONE);
                    binding.playerViewVideo.setControllerAutoShow(false);
                    binding.previous.setVisibility(View.GONE);
                    binding.playPause.setVisibility(View.GONE);
                    binding.next.setVisibility(View.GONE);
                    binding.volume.setVisibility(View.GONE);
                    setPlayerClose();
                }
            }
        });
        binding.ivPopupBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.ivPopupUper.setVisibility(View.VISIBLE);
                binding.ivPopupBottom.setVisibility(View.GONE);
                if (isMusicopen) {
                    isVideoOpen = false;
                    collapse(binding.popupPlayerController, 2000, 30);
                    binding.popupPlayerController.setVisibility(View.VISIBLE);
                }

                if (isVideoOpen) {
                    isMusicopen = false;
                    setPlayer();
                    binding.playerViewVideo.setVisibility(View.VISIBLE);
                    binding.playerControllerContainer.setVisibility(View.VISIBLE);
                    binding.playerViewVideo.setUseController(true);
                    binding.playerViewVideo.showController();
                    binding.previous.setVisibility(View.VISIBLE);
                    binding.playPause.setVisibility(View.VISIBLE);
                    binding.next.setVisibility(View.VISIBLE);
                    binding.volume.setVisibility(View.VISIBLE);
                    binding.color.setVisibility(View.VISIBLE);
                }
            }
        });
        binding.ivMiniUper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.ivMiniBottom.setVisibility(View.VISIBLE);
                binding.ivMiniUper.setVisibility(View.GONE);
                if (binding.miniPlayerContainer.getVisibility() == View.VISIBLE) {
                    isMusicopen = true;
                    isVideoOpen = false;
                    expand(binding.miniPlayerContainer, 2000, 30);
                    binding.miniPlayerContainer.setVisibility(View.GONE);
                }
                if (binding.playerViewVideo.getVisibility() == View.VISIBLE && isVideoOpen) {
                    isVideoOpen = true;
                    isMusicopen = false;
                    binding.playerViewVideo.setVisibility(View.GONE);
                    binding.playerViewVideo.setUseController(false);
                    binding.playerViewVideo.hideController();
                    binding.playerControllerContainer.setVisibility(View.GONE);
                    binding.playerViewVideo.setControllerAutoShow(false);
                    binding.previous.setVisibility(View.GONE);
                    binding.playPause.setVisibility(View.GONE);
                    binding.next.setVisibility(View.GONE);
                    binding.volume.setVisibility(View.GONE);
                    binding.color.setVisibility(View.GONE);
                    setPlayerClose();
                }
            }
        });
        binding.ivMiniBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.ivMiniUper.setVisibility(View.VISIBLE);
                binding.ivMiniBottom.setVisibility(View.GONE);
                if (isMusicopen) {
                    isVideoOpen = false;
                    collapse(binding.miniPlayerContainer, 2000, 30);
                    binding.miniPlayerContainer.setVisibility(View.VISIBLE);
                }
                if (isVideoOpen) {
                    isMusicopen = false;
                    setPlayer();
                    binding.playerViewVideo.setVisibility(View.VISIBLE);
                    binding.playerControllerContainer.setVisibility(View.VISIBLE);
                    binding.playerViewVideo.setUseController(true);
                    binding.playerViewVideo.showController();
                    binding.previous.setVisibility(View.VISIBLE);
                    binding.playPause.setVisibility(View.VISIBLE);
                    binding.next.setVisibility(View.VISIBLE);
                    binding.volume.setVisibility(View.VISIBLE);
                    binding.color.setVisibility(View.VISIBLE);
                }
            }
        });

        binding.backDrawer.setOnClickListener(view -> {
            lock_unlock_Drawer();
            if (binding.mainScreen.getVisibility() == View.VISIBLE) {
                binding.popupPlayerContainer.setVisibility(View.VISIBLE);
                binding.mainScreen.setVisibility(View.GONE);
            }
        });
        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("mytag", "here is main home clicked==================");
                binding.home.setSelected(true);
            }
        });
        binding.liveTvPopup.setOnClickListener(view -> {
            AppUtil.setup_Fragment(getSupportFragmentManager(), LiveTvFragment.newInstance(),
                    "", "", "", "", "LiveTv", "LiveTv", true);
            showFragment();
        });
        binding.radio.setOnClickListener(view -> {
            setVisibleStatus(false, false, false, false, false);
            AppUtil.setup_Fragment(getSupportFragmentManager(), RadioFragment.newInstance(),
                    Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_ID, ""),
                    getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "",
                    "DrawerInner", "DrawerInner", true);
            showFragment();
            setTitle(context.getResources().getString(R.string.live_radio));
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(getSupportFragmentManager(), VideoFragment.newInstance(),
                        Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_ID, ""),
                        "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                showFragment();
            }
        });
        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(getSupportFragmentManager(), PlaylistFavListingFragment.newInstance(),
                        Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_ID, ""),
                        "FavouritesSongsList", "FavouritesSongsList", "", "DrawerInner", "DrawerInner", true);
                showFragment();
            }
        });
        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(getSupportFragmentManager(), PlaylistFavListingFragment.newInstance(),
                        Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_ID, ""),
                        getResources().getString(R.string.playlist), getResources().getString(R.string.playlist), "", "DrawerInner", "DrawerInner", true);
                showFragment();
            }
        });

        binding.drawerPopup.setOnClickListener(view -> {
            AppUtil.setup_Fragment(getSupportFragmentManager(), MenuFragment.newInstance(), "", "", "", "", "Inner", "Inner", true);
            showFragment();
        });
        binding.close.setOnClickListener(view -> {
            Const.isPopupClose = true;
            binding.popupVideoContainer.setVisibility(View.GONE);
            binding.playerView.onPause();
            if (popupPlayer != null) {
                popupPlayer.release();
                popupPlayer = null;
            }
            if (player != null) {
                player.release();
                player = null;
            }
        });
        binding.volume.setOnClickListener(view -> {
            try {
                binding.volume.setSelected(!binding.volume.isSelected());
                player.setVolume(binding.volume.isSelected() ? 0f : 1f);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        binding.playPause.setOnClickListener(view -> {
            if (binding.playPause.isSelected()) {
                binding.playerViewVideo.onResume();
                if (player != null) {
                    player.setPlayWhenReady(false);
                }
                binding.playPause.setSelected(false);
            } else {
                binding.playerViewVideo.onPause();
                if (player != null)
                    player.setPlayWhenReady(true);
                binding.playPause.setSelected(true);
            }
        });
        binding.minimise.setOnClickListener(view -> {
            Const.isPopupClose = true;
            binding.playerView.onPause();
            long currentPosition = 0;
            if (popupPlayer != null)
                currentPosition = popupPlayer.getCurrentPosition();
            if (popupPlayer != null) {
                popupPlayer.release();
                popupPlayer = null;
            }
//            initializePlayer("normal", currentPosition);
            player.seekTo(currentPosition);
            player.setPlayWhenReady(true);
            binding.popupVideoContainer.setVisibility(View.GONE);
            binding.motionContainer.transitionToEnd();
            Const.isSwipe = false;
        });
        binding.next.setOnClickListener(view -> {
            if (menuItemsData != null && !menuItemsData.isEmpty() && menuItemsData.size() > 1) {
                if (selectedPosition < menuItemsData.size() - 1) {
                    selectedPosition++;
                    nextPreviousVideoData(menuItemsData);
                }

            }
        });
        binding.previous.setOnClickListener(view -> {
            if (menuItemsData != null && !menuItemsData.isEmpty() && menuItemsData.size() > 1) {
                if (selectedPosition != 0) {
                    if (selectedPosition <= menuItemsData.size()) {
                        selectedPosition--;
                        nextPreviousVideoData(menuItemsData);
                    }
                }
            }
        });

        binding.videoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeScreen.isPipModeEnabled = true;
                String url_type = Prefs.getPrefInstance().getValue(context, Const.VIDEO_URL_TYPE, "");
                if (url_type.equals("youtube")) {
                    binding.mainScreen.setVisibility(View.GONE);
                    binding.popupPlayerContainer.setVisibility(View.VISIBLE);

                    Const.isSwipe = true;
                    if (binding.motionContainer.getCurrentState() == R.id.start) {
                        isGoingDown = true;
                        binding.drawerContainer.setAlpha(0.0f);
                        binding.drawerContainer.setVisibility(View.VISIBLE);
                        binding.drawerContainer.animate().alpha(1.0f).setDuration(200).start();
                        Log.d("mytag", "videoHome if clicked clikced called================");
                    }
                } else {
                    if (binding.mainScreen.getVisibility() == View.VISIBLE) {
                        binding.popupPlayerContainer.setVisibility(View.VISIBLE);
                        binding.mainScreen.setVisibility(View.GONE);
                        isGoingDown = true;
                        binding.drawerContainer.setAlpha(0.0f);
                        binding.drawerContainer.setVisibility(View.VISIBLE);
                        binding.drawerContainer.animate().alpha(1.0f).setDuration(1000).start();
                        binding.motionContainer.transitionToEnd();
                        binding.playerViewVideo.setUseController(false);
                        miniPlayerVisibilityGone(true);
                        Log.d("mytag", "videoHome else clicked clikced called================");
                    }
                }
            }
        });
        binding.videoBack.setOnClickListener(view1 -> {
            String url_type = Prefs.getPrefInstance().getValue(context, Const.VIDEO_URL_TYPE, "");
            if (url_type.equals("youtube")) {
                if (binding.mainScreen.getVisibility() == View.VISIBLE) {
                    Log.d("mytag", "videoBack back clicked if part");
                    binding.popupPlayerContainer.setVisibility(View.GONE);
                    binding.mainScreen.setVisibility(View.VISIBLE);

                    Const.isSwipe = true;
                    if (binding.motionContainer.getCurrentState() == R.id.start) {
                        isGoingDown = true;
                        binding.drawerContainer.setAlpha(0.0f);
                        binding.drawerContainer.setVisibility(View.VISIBLE);
                        binding.drawerContainer.animate().alpha(1.0f).setDuration(200).start();
                        Log.d("mytag", "videoBack clicked clikced else called");
                        isVideoOpen = false;
                        binding.ivMiniBottom.setVisibility(View.GONE);
                        binding.ivPopupBottom.setVisibility(View.GONE);
                        binding.ivPopupUper.setVisibility(View.GONE);
                        binding.ivMiniUper.setVisibility(View.GONE);
                    }
                }
            } else {
                Const.isSwipe = true;
                if (binding.motionContainer.getCurrentState() == R.id.start) {
                    isGoingDown = true;
                    binding.drawerContainer.setAlpha(0.0f);
                    binding.drawerContainer.setVisibility(View.VISIBLE);
                    binding.drawerContainer.animate().alpha(1.0f).setDuration(1000).start();
                    binding.motionContainer.transitionToEnd();
                    binding.playerViewVideo.setUseController(false);
                    miniPlayerVisibilityGone(true);
                    isFromVideoFragment = false;
                    isPipModeEnabled = true;
                }
            }
        });
        binding.videoLiveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("mytag", "here is live tv popup clicked");
                setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(getSupportFragmentManager(), LiveTvFragment.newInstance(),
                        "", "", "", "", "DrawerInner", "DrawerInner", true);
                binding.videoBack.performClick();
            }
        });
        binding.videoRadio.setOnClickListener(view -> {
            setVisibleStatus(false, false, false, false, false);
            AppUtil.setup_Fragment(getSupportFragmentManager(), RadioFragment.newInstance(),
                    Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_ID, ""),
                    getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "",
                    "DrawerInner", "DrawerInner", true);
            binding.videoBack.performClick();
            setTitle(context.getResources().getString(R.string.live_radio));
        });
        binding.videoIvVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(getSupportFragmentManager(), VideoFragment.newInstance(),
                        Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_ID, ""),
                        "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                binding.videoBack.performClick();
            }
        });
        binding.videoIvFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(getSupportFragmentManager(), PlaylistFavListingFragment.newInstance(),
                        Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_ID, ""),
                        "FavouritesSongsList", "FavouritesSongsList", "", "DrawerInner", "DrawerInner", true);
                binding.videoBack.performClick();
//                binding.videoHome.performClick();
            }
        });
        binding.videoIvPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(getSupportFragmentManager(), PlaylistFavListingFragment.newInstance(),
                        Prefs.getPrefInstance().getValue(HomeScreen.this, Const.MENU_RADIO_ID, ""),
                        "PlaylistList", "PlaylistList", "", "DrawerInner", "DrawerInner", true);
                binding.videoBack.performClick();
            }
        });
        binding.videoDrawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.videoDrawerPopup.setSelected(true);
                binding.drawerPopup.setSelected(true);
                AppUtil.setup_Fragment(getSupportFragmentManager(), MenuFragment.newInstance(), "", "", "", "", "Inner", "Inner", true);
                binding.videoBack.performClick();
            }
        });
    }

    @Override
    public void livetvBack() {
        binding.playerViewVideo.setVisibility(View.VISIBLE);
        PlayerManageMent();
        Prefs.getPrefInstance().setValue(context, Const.VIDEO_URL_TYPE, "normal");
        binding.ivPopupBottom.setVisibility(View.VISIBLE);
        boolean isVideoPlaying = LiveTvFragment.getInstance().getVideoIsPlaying();
        binding.popupPlayerContainer.setVisibility(View.VISIBLE);
        binding.mainScreen.setVisibility(View.GONE);
        if (isVideoPlaying) {
            Log.d("mytag", "livetvBack onBackPressed back clicke323232322322" + playBackUrl);
            long currentPosition = LiveTvFragment.getInstance().getCurrentPosition();
            setLiveTvVideo(playBackUrl, "playVideo", currentPosition);
            islivetvBackClicked = false;
        } else {
            Log.d("mytag", "livetvBack onBackPressed back clicked9999999999999");
            setLiveTvVideo(playBackUrl, "close", 0);
        }
    }

    public static long getVideoPosition() {
        return player.getCurrentPosition();
    }

    @Override
    public void hideVideoPlayer() {
        if (binding.mainScreen.getVisibility() == View.VISIBLE) {
            Log.d("mytag", "hideVideoPlayerhideVideoPlayerhideVideoPlayerhideVideoPlayer");
            binding.popupPlayerContainer.setVisibility(View.GONE);
            binding.mainScreen.setVisibility(View.VISIBLE);
            if (Const.isSwipe) {
                if (binding.motionContainer.getCurrentState() == R.id.end) {
                    Log.d("mytag", "here is player view video open------------------");
                    isGoingDown = false;
                    binding.drawerContainer.setAlpha(1.0f);
                    binding.drawerContainer.setVisibility(View.VISIBLE);
                    binding.drawerContainer.animate().alpha(0.0f).setDuration(1000).withEndAction(() -> binding.drawerContainer.setVisibility(View.GONE)).start();
                    binding.motionContainer.transitionToStart();
                    binding.playerViewVideo.setUseController(true);
                }
            } else {
                if (binding.motionContainer.getCurrentState() == R.id.end) {
                    Log.d("mytag", "here is player view video open222222222222------------------");
                    isGoingDown = false;
                    binding.drawerContainer.setAlpha(1.0f);
                    binding.drawerContainer.setVisibility(View.VISIBLE);
                    binding.drawerContainer.animate().alpha(0.0f).setDuration(1000).withEndAction(() -> binding.drawerContainer.setVisibility(View.GONE)).start();
                    binding.motionContainer.transitionToStart();
                    binding.playerViewVideo.setUseController(true);
                }
            }
        }
    }

    @Override
    protected void onRestart() {
        Log.d("mytag", "onRestart called================");
        binding.playerView.onResume();
        binding.playerView.onResume();
        if (player != null) {
            closeLoveTvPlayer();
            Log.d("mytag", "onRestart called================" + player);
            player.setPlayWhenReady(true);
        }
        if (popupPlayer != null) {
            popupPlayer.setPlayWhenReady(true);
        }
        super.onRestart();
    }

    @Override
    protected void onStart() {
        Log.d("mytag", "onStart called================");
        binding.playerView.onResume();
        binding.playerView.onResume();
        if (player != null) {
            Log.d("mytag", "onStart called================" + player);
            player.setPlayWhenReady(true);
        }
        if (popupPlayer != null) {
            popupPlayer.setPlayWhenReady(true);
        }
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d("mytag", "onResume called================");
        super.onResume();
        addObserver();
        if (MediaController.getInstance().getPlayingSongDetail() != null) {
            loadAssetDetails(MediaController.getInstance().getPlayingSongDetail());
            onResumeNotif();
        }
        mTracker.setScreenName("Screen - " + "Home Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void onResumeNotif() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameContainer);
        String song_id = MediaController.getInstance().getPlayingSongDetail().getSongId();
        Log.d("mytag", "newSongLoaded called in onResume " + song_id);
        HomeCategoryAdapter.onLoadSong(song_id);
        if (fragment instanceof AssetDetailFragment) {
            Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, song_id);
            ((AssetDetailFragment) fragment).onLoadSong(song_id);
        } else if (fragment instanceof PlaylistFavListingFragment) {
            Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, song_id);
            ((PlaylistFavListingFragment) fragment).onLoadSong(song_id);
        } else if (fragment instanceof CategoryViewAllFragment) {
            Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, song_id);
            ((CategoryViewAllFragment) fragment).onLoadSong(song_id);
        } else if (fragment instanceof GridMusicFragment) {
            Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, song_id);
            ((GridMusicFragment) fragment).onLoadSong(song_id);
        } else if (fragment instanceof SearchFragment) {
            Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, song_id);
            ((SearchFragment) fragment).onLoadSong(song_id);
        }
    }

    @Override
    protected void onStop() {
        Log.d("mytag", "onStop called================");
        if (player != null) {
            player.setPlayWhenReady(false);
        }
        if (popupPlayer != null) {
            popupPlayer.setPlayWhenReady(true);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("mytag", "onDestroy called================");
        removeObserver();
        if (MediaController.getInstance().isAudioPaused()) {
            MediaController.getInstance().cleanupPlayer(HomeScreen.this, true, true);
        }
        if (player != null) {
            player.setPlayWhenReady(false);
        }
        if (popupPlayer != null) {
            popupPlayer.setPlayWhenReady(false);
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        isLiveTVShareClicked = true;
//        isPipModeEnabled = true;
        Log.d("mytag", "onPause called1111==========" + player + "  " + isPipModeEnabled + " "
                + isLiveTVShareClicked + " " + islivetvBackClicked);
        binding.playerView.onResume();
        try {
            if (!islivetvBackClicked) {
                isPipModeEnabled = true;
                isLiveTVShareClicked = true;
                Log.d("mytag", "onPause called22222222======" + player + "  " + isPipModeEnabled + " " +
                        isLiveTVShareClicked + " " + islivetvBackClicked);
                FloatingWidgetService.hidePipPlayer();
            }

            if (player != null) {
                Log.d("mytag", "onPause called3333333========" + player + "  "
                        + isPipModeEnabled + " " + isLiveTVShareClicked + " " + islivetvBackClicked);
                if (isFromVideoFragment) {
                    if (!isPipModeEnabled) {
                        Log.d("mytag", "onPause called99999999========" + player + "  "
                                + isPipModeEnabled + " " + isLiveTVShareClicked + " " + islivetvBackClicked);
                        Intent serviceIntent = new Intent(context, FloatingWidgetService.class);
                        serviceIntent.putExtra("videoUri", playBackUrl);
                        serviceIntent.putExtra("isfrom", "video");
                        startService(serviceIntent);
                    }
                } else {
                    Log.d("mytag", "onPause called4444444444444================" + isPipModeEnabled + " " + isLiveTVShareClicked);
                    if (!isLiveTVShareClicked || !isPipModeEnabled) {
                        Intent serviceIntent = new Intent(context, FloatingWidgetService.class);
                        serviceIntent.putExtra("videoUri", playBackUrl);
                        serviceIntent.putExtra("isfrom", "livetv");
                        startService(serviceIntent);
                    }
                }
                player.setPlayWhenReady(true);
            }
            if (popupPlayer != null) {
                popupPlayer.setPlayWhenReady(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
        removeObserver();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            androidx.appcompat.app.AlertDialog.Builder alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this);
            alertDialog.setTitle(getResources().getString(R.string.app_name));
            alertDialog.setMessage("Are you sure want to exit?");
            alertDialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                            isPipModeEnabled = true;
                            isLiveTVShareClicked = true;
                            isFromVideoFragment = false;
                            FloatingWidgetService.hidePipPlayer();
                        }
                    });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        if (binding.motionContainer.getCurrentState() == R.id.start && binding.drawerContainer.getVisibility() == View.GONE) {
            Log.d("mytag", "main activityonBackPressed back clicked11111111111");
            binding.videoBack.performClick();
            return;
        }

        if (doubleBackToExitPressedOnce) {
            FloatingWidgetService.hidePipPlayer();
            finish();
            return;
        }

        if (binding.drawerContainer.isDrawerOpen(GravityCompat.START)) {
            Log.d("mytag", "main activity onBackPressed back clicked2222222222222222222");
            binding.drawerContainer.closeDrawer(GravityCompat.START);
            binding.drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            return;
        }

        if (binding.mainScreen.getVisibility() == View.VISIBLE) {
            FloatingWidgetService.hidePipPlayer();
            Log.d("mytag", "main activity onBackPressed back clicked333333333333333");
            int count = getSupportFragmentManager().getBackStackEntryCount();
            String name = getSupportFragmentManager().getBackStackEntryAt(count - 1).getName();
            if (count > 0) {
                if (count == 1) {
                    binding.popupPlayerContainer.setVisibility(View.VISIBLE);
                    binding.mainScreen.setVisibility(View.GONE);
                }
                if (name.equals("LiveTv")) {
                    binding.ivPopupBottom.setVisibility(View.VISIBLE);
                    boolean isVideoPlaying = LiveTvFragment.getInstance().getVideoIsPlaying();
                    if (isVideoPlaying) {
                        long currentPosition = LiveTvFragment.getInstance().getCurrentPosition();
                        setLiveTvVideo(playBackUrl, "playVideo", currentPosition);
                    } else {
                        setLiveTvVideo(playBackUrl, "close", 0);
                    }
                }
                getSupportFragmentManager().popBackStack();
                setTitle(name);
            } else {
                homevisible = true;
                displayInterstitial();
                binding.popupPlayerContainer.setVisibility(View.VISIBLE);
                binding.mainScreen.setVisibility(View.GONE);
                if (binding.popupPlayerController.getVisibility() == View.VISIBLE) {
                    binding.ivPopupBottom.setVisibility(View.GONE);
                }
                if (binding.miniPlayerContainer.getVisibility() == View.VISIBLE) {
                    binding.ivMiniBottom.setVisibility(View.GONE);
                }
            }
        } else {
            this.doubleBackToExitPressedOnce = true;
            AppUtil.show_Exit_Snackbar(HomeScreen.this, binding.frameContainer, false);
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        }
    }

    @Override
    public void setLiveTvVideo(String url, String type, long currentPosition) {
        if (type.equals("getReady")) {
            isLiveTVShareClicked = true;
            playBackUrl = url;
            Const.isSwipe = false;
            initializePlayer("livetv", currentPosition);
            binding.motionContainer.transitionToStart();
        } else if (type.equals("close")) {
            playBackUrl = "";
            Const.isSwipe = true;
            if (player != null) {
                player.release();
                player = null;
            }
            binding.motionContainer.transitionToStart();
        } else {
            if (player != null) {
                player.seekTo(currentPosition);
                player.setPlayWhenReady(true);
            } else {
                initializePlayer("livetv", currentPosition);
            }
            Const.isSwipe = false;
            binding.motionContainer.transitionToEnd();
        }
    }

    private void getPopupVideo() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_home_items = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_home_items(get_home_items).enqueue(new Callback<HomeItemsResponse>() {
                @Override
                public void onResponse(@NonNull Call<HomeItemsResponse> call, @NonNull Response<HomeItemsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getPopupvideoData() != null && !response.body().getPopupvideoData().isEmpty()) {
                                playBackUrl = response.body().getLivetvvideoData().get(0).getVideosLink();
                                initializePlayer("popupvideo", 0);
                                binding.popupVideoContainer.setVisibility(View.VISIBLE);
                            } else {
                                binding.popupVideoContainer.setVisibility(View.GONE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }

                }


                @Override
                public void onFailure(@NonNull Call<HomeItemsResponse> call, @NonNull Throwable t) {
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }

            });
        } else {
            binding.loader.setVisibility(View.GONE);
        }
    }

    private void callVideoDetails(String menu_id, String video_id, String category_id, String type) {
        Log.d("mytag", "hree in callVideoDetails----------------" + type);
        Const.isSwipe = true;
        CURRENT_PAGE = 1;
        LAST_PAGE = 1;
        LIMIT = 20;
        binding.listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
        binding.listing.setHasFixedSize(true);
        videoListingAdapter = new VideoListingAdapter(context, getSupportFragmentManager(), new ArrayList<>(), getResources().getString(R.string.type_suggested));
        binding.listing.setAdapter(videoListingAdapter);
        binding.listing.setVisibility(View.VISIBLE);
        if (type != null && type.toLowerCase().equals(getResources().getString(R.string.type_fav).toLowerCase()))
            getFavVideoDetail(video_id);
        else
            getVideoDetail(menu_id, video_id, category_id);
        binding.fav.setOnClickListener(view1 -> add_remove_fav_videos(menu_id, video_id));
    }

    private void getVideoDetail(String menu_id, String video_id, String category_id) {
        Log.d("mytag", "hree in getVideoDetail====================" + "       " + type + "   " + category_id);
        if (category_id != null && !category_id.isEmpty())
            type = "CategoryVideos";
        else type = "Videos";
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("menu_id", menu_id);
                jsonObject.put("videos_id", video_id);
                jsonObject.put("category_id", category_id);
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_video_detail = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_video_detail(get_video_detail, type).enqueue(new Callback<VideoDetailsResponse>() {
                @Override
                public void onResponse(@NonNull Call<VideoDetailsResponse> call, @NonNull Response<VideoDetailsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getSuggestedData() != null && !response.body().getSuggestedData().isEmpty()) {
                                loadingInProgress = false;
                                videoListingAdapter.add(response.body().getSuggestedData());
                                if (CURRENT_PAGE == 1) {
                                    binding.listing.setVisibility(View.VISIBLE);
                                    binding.noDataFound.setVisibility(View.GONE);
                                } else {
                                    loadingInProgress = false;
                                }
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFoundSuggesions.setVisibility(View.GONE);
                            } else {
                                binding.listing.setVisibility(View.GONE);
                                binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                            }
                            if (response.body().getVideosDetail() != null && !response.body().getVideosDetail().isEmpty()) {
                                binding.assetTitle.setText(response.body().getVideosDetail().get(0).getVideosName());
                                binding.tvTitle.setText(response.body().getVideosDetail().get(0).getVideosName());
                                binding.assetDetail.setText(response.body().getVideosDetail().get(0).getVideosDescription());
                                binding.fav.setSelected(response.body().getVideosDetail().get(0).getFavouritesStatus());
                                playBackUrl = response.body().getVideosDetail().get(0).getVideosLink();
                                String video_image = response.body().getVideosDetail().get(0).getVideosImage();
                                Prefs.getPrefInstance().setValue(context, Const.VIDEO_IMAGE, video_image);
                                Prefs.getPrefInstance().setValue(context, Const.VIDEO_URL, playBackUrl);
                                String url_type = Prefs.getPrefInstance().getValue(context, Const.VIDEO_URL_TYPE, "");
                                if (url_type.equals("youtube")) {
                                    binding.webView.setVisibility(View.VISIBLE);
                                    binding.playerViewVideo.setVisibility(View.GONE);
                                    binding.videoOverlayContainer.setVisibility(View.GONE);
                                    binding.videoOverlay.setVisibility(View.GONE);
                                } else {
                                    String video_seekbar = Prefs.getPrefInstance().getValue(context, Const.VIDEO_SEEKBAR_POSITION, "");
                                    if (video_seekbar != null && !video_seekbar.isEmpty() && !video_seekbar.equals("null")) {
                                        initializePlayer("normal", Long.parseLong(video_seekbar));
                                    } else {
                                        initializePlayer("normal", 0);
                                    }
                                }
                                binding.loader.setVisibility(View.GONE);
                            } else {
                                loadingInProgress = false;
                                if (CURRENT_PAGE == 1) {
                                    binding.loader.setVisibility(View.GONE);
                                    binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                                    binding.listing.setVisibility(View.GONE);
                                }
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            loadingInProgress = false;
                            if (CURRENT_PAGE == 1) {
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                                binding.listing.setVisibility(View.GONE);
                                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                final AlertDialog dialog = new AlertDialog.Builder(context)
                                        .setCancelable(false)
                                        .setView(dialog_view)
                                        .show();

                                if (dialog.getWindow() != null)
                                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                    dialog.dismiss();
                                });
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<VideoDetailsResponse> call, @NonNull Throwable t) {
                    loadingInProgress = false;
                    if (CURRENT_PAGE == 1) {
                        binding.loader.setVisibility(View.GONE);
                        binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                        binding.listing.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }
            });
        } else {
            loadingInProgress = false;
            if (CURRENT_PAGE == 1) {
                binding.loader.setVisibility(View.GONE);
                binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                binding.listing.setVisibility(View.GONE);
            }
        }
    }

    private void getFavVideoDetail(String video_id) {
        type = "FavouriteVideosDetails";
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("videos_id", video_id);
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_fav_video_detail = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_fav_video_detail(get_fav_video_detail, type).enqueue(new Callback<VideoDetailsResponse>() {
                @Override
                public void onResponse(@NonNull Call<VideoDetailsResponse> call, @NonNull Response<VideoDetailsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getSuggestedData() != null && !response.body().getSuggestedData().isEmpty()) {
                                loadingInProgress = false;
                                videoListingAdapter.add(response.body().getSuggestedData());
                                if (CURRENT_PAGE == 1) {
                                    binding.listing.setVisibility(View.VISIBLE);
                                    binding.noDataFound.setVisibility(View.GONE);
                                } else {
                                    loadingInProgress = false;
                                }
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFoundSuggesions.setVisibility(View.GONE);
                            } else {
                                binding.listing.setVisibility(View.GONE);
                                binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                            }
                            if (response.body().getVideosDetail() != null && !response.body().getVideosDetail().isEmpty()) {
                                binding.assetTitle.setText(response.body().getVideosDetail().get(0).getVideosName());
                                binding.tvTitle.setText(response.body().getVideosDetail().get(0).getVideosName());
                                binding.assetDetail.setText(response.body().getVideosDetail().get(0).getVideosDescription());
                                binding.fav.setSelected(response.body().getVideosDetail().get(0).getFavouritesStatus());
                                playBackUrl = response.body().getVideosDetail().get(0).getVideosLink();
                                Prefs.getPrefInstance().setValue(context, Const.VIDEO_URL, playBackUrl);
                                String video_image = response.body().getVideosDetail().get(0).getVideosImage();
                                Prefs.getPrefInstance().setValue(context, Const.VIDEO_IMAGE, video_image);
                                String url_type = Prefs.getPrefInstance().getValue(context, Const.VIDEO_URL_TYPE, "");
                                if (url_type.equals("youtube")) {
                                    binding.webView.setVisibility(View.VISIBLE);
                                    binding.playerViewVideo.setVisibility(View.GONE);
                                    binding.videoOverlayContainer.setVisibility(View.GONE);
                                    binding.videoOverlay.setVisibility(View.GONE);
                                } else {
                                    String video_seekbar = Prefs.getPrefInstance().getValue(context, Const.VIDEO_SEEKBAR_POSITION, "");
                                    if (video_seekbar != null && !video_seekbar.isEmpty() && !video_seekbar.equals("null")) {
                                        Log.d("mytag", "hree video_seekbarvideo_seekbar in fav video details====================" + video_seekbar);
                                        initializePlayer("normal", Long.parseLong(video_seekbar));
                                    } else {
                                        initializePlayer("normal", 0);
                                    }
                                }
                                binding.loader.setVisibility(View.GONE);
                            } else {
                                loadingInProgress = false;
                                if (CURRENT_PAGE == 1) {
                                    binding.loader.setVisibility(View.GONE);
                                    binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                                    binding.listing.setVisibility(View.GONE);
                                }
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            loadingInProgress = false;
                            if (CURRENT_PAGE == 1) {
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                                binding.listing.setVisibility(View.GONE);
                                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                final AlertDialog dialog = new AlertDialog.Builder(context)
                                        .setCancelable(false)
                                        .setView(dialog_view)
                                        .show();

                                if (dialog.getWindow() != null)
                                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                    dialog.dismiss();
                                });
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<VideoDetailsResponse> call, @NonNull Throwable t) {
                    loadingInProgress = false;
                    if (CURRENT_PAGE == 1) {
                        binding.loader.setVisibility(View.GONE);
                        binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                        binding.listing.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }
            });
        } else {
            loadingInProgress = false;
            if (CURRENT_PAGE == 1) {
                binding.loader.setVisibility(View.GONE);
                binding.noDataFoundSuggesions.setVisibility(View.VISIBLE);
                binding.listing.setVisibility(View.GONE);
            }
        }
    }

    private void add_remove_fav_videos(String menu_id, String video_id) {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("menu_id", menu_id);
                jsonObject.put("videos_id", video_id);
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody add_remove_fav_videos = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().add_remove_fav_videos(add_remove_fav_videos, "AddRemoveFavouriteVideos").enqueue(new Callback<FavVideosResponse>() {
                @Override
                public void onResponse(@NonNull Call<FavVideosResponse> call, @NonNull Response<FavVideosResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            AppUtil.show_Snackbar(context, binding.mainContainer, response.body().getMessage(), false);
                            binding.fav.setSelected(response.body().getFavouritesStatus());
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });

                        }
                    } else {
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<FavVideosResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    @Override
    public void webviewManageMent() {
        binding.webView.setVisibility(View.VISIBLE);
        binding.playerViewVideo.setVisibility(View.GONE);
        binding.videoOverlayContainer.setVisibility(View.GONE);
        binding.videoOverlay.setVisibility(View.GONE);
    }

    @Override
    public void PlayerManageMent() {
        binding.webView.setVisibility(View.GONE);
    }

    @Override
    public void showMotionVideo(MenuItemsData queue, ArrayList<MenuItemsData> data, int position, String type) {
        binding.playerViewVideo.setVisibility(View.VISIBLE);
        binding.videoOverlayContainer.setVisibility(View.GONE);
        binding.webView.setVisibility(View.GONE);
        Log.d("mytag", "hree in showMotionVideo-------------" + type);
        menuItemsData = data;
        selectedPosition = position;
        videoType = type;
        callVideoDetails(queue.getMenuId(), queue.getVideosId(), queue.getCategoryId(), type);
        binding.drawerContainer.setAlpha(0.0f);
        binding.drawerContainer.setVisibility(View.GONE);
        if (MediaController.getInstance().getPlayingSongDetail() != null && !MediaController.getInstance().isAudioPaused()) {
            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
        }
        miniPlayerVisibilityGone(true);
        Log.d("mytag", "showMotionVideoshowMotionVideoshowMotionVideo clicked clikced called================");
        if (binding.motionContainer.getCurrentState() == R.id.end)
            binding.motionContainer.transitionToStart();
        if (binding.ivMiniUper.getVisibility() == View.GONE) {
            binding.ivMiniUper.setVisibility(View.VISIBLE);
        }
        if (binding.ivPopupUper.getVisibility() == View.GONE) {
            binding.ivPopupUper.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showYoutubeWebview(MenuItemsData queue, ArrayList<MenuItemsData> data, int position, String type) {
        if (player != null) {
            player.release();
            player = null;
        }
        isVideoOpen = false;
        binding.ivMiniBottom.setVisibility(View.GONE);
        binding.ivPopupBottom.setVisibility(View.GONE);
        binding.playerViewVideo.setVisibility(View.GONE);
        binding.videoOverlayContainer.setVisibility(View.GONE);
        binding.webView.setVisibility(View.VISIBLE);
        String url = Prefs.getPrefInstance().getValue(context, Const.VIDEO_YOUTUBE_URL, "");
        callURL(url);
        menuItemsData = data;
        selectedPosition = position;
        videoType = type;
        callVideoDetails(queue.getMenuId(), queue.getVideosId(), queue.getCategoryId(), type);
        binding.drawerContainer.setAlpha(0.0f);
        binding.drawerContainer.setVisibility(View.GONE);
        if (MediaController.getInstance().getPlayingSongDetail() != null && !MediaController.getInstance().isAudioPaused()) {
            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
        }
    }

    private void callURL(String path) {
        binding.webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        binding.webView.getSettings().setLoadsImagesAutomatically(true);
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        binding.webView.loadUrl(path);
        binding.loader.setVisibility(View.GONE);
    }

    @Override
    public void setVisibleStatus(boolean audioPlayerVisible, boolean songShouldPause, boolean videoPlayerVisible,
                                 boolean videoShouldPause, boolean videoInFullMode) {
        binding.miniPlayerContainer.setVisibility(audioPlayerVisible ? View.VISIBLE : View.GONE);
        binding.popupPlayerController.setVisibility(audioPlayerVisible ? View.VISIBLE : View.GONE);
        if (songShouldPause) {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
            }
        }
        if (videoPlayerVisible && (player != null || popupPlayer != null)) {
            binding.motionContainer.transitionToEnd();
        } else {
            binding.motionContainer.transitionToStart();
        }
        if (videoInFullMode) {
            binding.motionContainer.transitionToStart();
        }
    }

    @Override
    public void hideBothAerrow() {
        isMusicopen = false;
        binding.ivPopupUper.setVisibility(View.GONE);
        binding.ivPopupBottom.setVisibility(View.GONE);
        binding.ivMiniUper.setVisibility(View.GONE);
        binding.ivMiniBottom.setVisibility(View.GONE);
    }

    @Override
    public void showMainAcreen() {
        binding.mainScreen.setVisibility(View.GONE);
        binding.popupPlayerContainer.setVisibility(View.VISIBLE);
        bottomanduppermanage(true);
        miniPlayerVisibilityGone(false);
    }

    @Override
    public void showMainAcreeninFavPlaylist() {
        binding.mainScreen.setVisibility(View.GONE);
        binding.popupPlayerContainer.setVisibility(View.VISIBLE);
        bottomanduppermanage(false);
        miniPlayerVisibilityGone(false);
    }

    @Override
    public void bottomanduppermanage(boolean isHide) {
        if (isHide) {
            if (player != null) {
                binding.ivMiniUper.setVisibility(View.VISIBLE);
                binding.ivPopupUper.setVisibility(View.VISIBLE);
            } else {
                binding.ivPopupUper.setVisibility(View.GONE);
                binding.ivPopupBottom.setVisibility(View.GONE);
                binding.ivMiniUper.setVisibility(View.GONE);
                binding.ivMiniBottom.setVisibility(View.GONE);
            }
        } else {
            binding.ivMiniBottom.setVisibility(View.VISIBLE);
            binding.ivPopupBottom.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void miniPlayerVisibilityGone(boolean isHide) {
        Log.d("mytag", "miniplayer visibility is visible--------------------------" + isHide);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameContainer);
        if (MediaController.getInstance().getPlayingSongDetail() != null) {
            if (isHide) {
                binding.popupPlayerController.setVisibility(View.GONE);
                binding.miniPlayerContainer.setVisibility(View.GONE);
                if (binding.ivMiniUper.getVisibility() == View.GONE) {
                    binding.ivMiniUper.setVisibility(View.VISIBLE);
                }
                if (binding.ivPopupUper.getVisibility() == View.GONE) {
                    binding.ivPopupUper.setVisibility(View.VISIBLE);
                }
            } else {
                if (player != null) {
                    binding.popupPlayerController.setVisibility(View.GONE);
                    binding.miniPlayerContainer.setVisibility(View.GONE);
                    binding.ivMiniUper.setVisibility(View.GONE);
                    binding.ivPopupUper.setVisibility(View.GONE);
                } else {
                    binding.popupPlayerController.setVisibility(View.VISIBLE);
                    binding.miniPlayerContainer.setVisibility(View.VISIBLE);
                    binding.ivMiniUper.setVisibility(View.VISIBLE);
                    binding.ivPopupUper.setVisibility(View.VISIBLE);
                }
            }
        } else if (isHide && binding.miniPlayerContainer.getVisibility() == View.VISIBLE) {
            binding.miniPlayerContainer.setVisibility(View.GONE);
        } else if (isHide && binding.popupPlayerController.getVisibility() == View.VISIBLE) {
            binding.popupPlayerController.setVisibility(View.GONE);
        }
    }

    @Override
    public void setPlayer() {
        FloatingWidgetService.hidePipPlayer();
        Log.d("mytag", "player is setPlayer method-----------------");
        Const.isSwipe = true;
        if (binding.motionContainer.getCurrentState() == R.id.start) {
            isVideoOpen = true;
            isGoingDown = true;
            binding.drawerContainer.setAlpha(0.0f);
            binding.drawerContainer.setVisibility(View.VISIBLE);
            binding.drawerContainer.animate().alpha(1.0f).setDuration(1000).start();
            binding.motionContainer.transitionToEnd();
            binding.playerViewVideo.setUseController(false);
            miniPlayerVisibilityGone(true);
        }
    }

    @Override
    public void setPlayerClose() {
        FloatingWidgetService.hidePipPlayer();
        Log.d("mytag", "player is close-------------");
        Const.isPopupClose = true;
        binding.playerView.onPause();
        binding.playerViewVideo.onPause();
        if (player != null) {
            player.setPlayWhenReady(false);
        }
        if (popupPlayer != null) {
            popupPlayer.setPlayWhenReady(false);
        }
    }

    @Override
    public void closeVideoBottom() {
        binding.ivPopupUper.setVisibility(View.GONE);
        binding.ivPopupBottom.setVisibility(View.GONE);
        binding.ivMiniUper.setVisibility(View.GONE);
        binding.ivMiniBottom.setVisibility(View.GONE);
        binding.popupPlayerController.setVisibility(View.GONE);
        binding.miniPlayerContainer.setVisibility(View.GONE);
        if (player != null) {
            player.setPlayWhenReady(true);
        }
        if (popupPlayer != null) {
            popupPlayer.setPlayWhenReady(true);
        }
    }


    public void nextPreviousVideoData(ArrayList<MenuItemsData> data) {
        data = menuItemsData;
        callVideoDetails(data.get(selectedPosition).getMenuId(), data.get(selectedPosition).getVideosId(), data.get(selectedPosition).getCategoryId(), videoType);
        binding.drawerContainer.setAlpha(0.0f);
        binding.drawerContainer.setVisibility(View.GONE);
        if (MediaController.getInstance().getPlayingSongDetail() != null && !MediaController.getInstance().isAudioPaused()) {
            MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
        }
        miniPlayerVisibilityGone(true);
        if (binding.motionContainer.getCurrentState() == R.id.end)
            binding.motionContainer.transitionToStart();
    }

    private void initializePlayer(String type, long currentPosition) {
        Log.d("mytag", "hree in initializePlayer====================");
        if (binding.miniPlayerContainer.getVisibility() == View.VISIBLE)
            binding.miniPlayerContainer.setVisibility(View.GONE);
        if (binding.popupPlayerController.getVisibility() == View.VISIBLE)
            binding.popupPlayerController.setVisibility(View.GONE);
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.CONTENT_TYPE_MOVIE)
                .build();

        if (type.equals("popupvideo")) {
            popupPlayer = new SimpleExoPlayer.Builder(context).build();
            if (popupPlayer.getAudioComponent() != null)
                popupPlayer.getAudioComponent().setAudioAttributes(audioAttributes, true);
            popupPlayer.addListener(new Player.EventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                    binding.close.performClick();
                    switch (error.type) {
                        case TYPE_SOURCE:
                        case TYPE_OUT_OF_MEMORY:
                        case TYPE_REMOTE:
                        case TYPE_RENDERER:
                        case TYPE_UNEXPECTED:
                            break;
                    }
                }
            });
            popupPlayer.setPlayWhenReady(true);
            binding.playerView.setPlayer(popupPlayer);
            binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            binding.playerView.setUseController(false);
            if (!playBackUrl.equals("")) {
                Uri uri = Uri.parse(playBackUrl);
                MediaSource mediaSource = buildMediaSource(uri, null);
                popupPlayer.prepare(mediaSource, true, true);
                popupPlayer.seekTo(currentPosition);
                initializePlayer("normal", 0);
            }

        } else {
            binding.videoOverlay.setVisibility(View.GONE);
            binding.volume.setSelected(false);

            player = new SimpleExoPlayer.Builder(context).build();
            if (player.getAudioComponent() != null)
                player.getAudioComponent().setAudioAttributes(audioAttributes, true);
            player.addListener(new Player.EventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (playbackState == Player.STATE_READY) {
                        binding.videoOverlay.setVisibility(View.GONE);
                        binding.playPause.setEnabled(true);

                        if (player.isCurrentWindowLive()) {
                            binding.next.setVisibility(View.GONE);
                            binding.previous.setVisibility(View.GONE);
                        } else {
                            binding.next.setVisibility(View.VISIBLE);
                            binding.previous.setVisibility(View.VISIBLE);
                        }
                    }
                    binding.playPause.setSelected(playWhenReady);

                    if (playbackState == PlaybackStateCompat.STATE_SKIPPING_TO_NEXT) {
                        if (menuItemsData != null && !menuItemsData.isEmpty() && menuItemsData.size() > 1) {
                            if (selectedPosition < menuItemsData.size() - 1) {
                                selectedPosition++;
                                nextPreviousVideoData(menuItemsData);
                            }
                        }
                    }
                    if (playbackState == PlaybackStateCompat.STATE_SKIPPING_TO_PREVIOUS) {
                        Log.d("mytag", "previous");
                        if (menuItemsData != null && !menuItemsData.isEmpty() && menuItemsData.size() > 1) {
                            if (selectedPosition != 0) {
                                if (selectedPosition <= menuItemsData.size()) {
                                    selectedPosition--;
                                    nextPreviousVideoData(menuItemsData);
                                }
                            }
                        }
                    }
                }

                @Override
                public void onPlayerError(@NotNull ExoPlaybackException error) {
                    binding.videoOverlay.setVisibility(View.VISIBLE);
                    String video_image = Prefs.getPrefInstance().getValue(context, Const.VIDEO_IMAGE, "");
                    if (binding.playPause.isSelected())
                        binding.playPause.setSelected(false);
                    binding.playPause.setEnabled(false);
                    switch (error.type) {
                        case TYPE_SOURCE:
                            if (error.getCause() instanceof BehindLiveWindowException) {
                                player.retry();
                            }
                            break;
                        case TYPE_RENDERER:
                        case TYPE_UNEXPECTED:
                            player.retry();
                            break;
                        case TYPE_OUT_OF_MEMORY:
                        case TYPE_REMOTE:
                            AppUtil.show_Snackbar(context, binding.playerView, error.getMessage(), true);
                            break;
                    }
                }
            });
            Log.d("mytag", " initialize playerhere is player view video open------------------");
            binding.playerViewVideo.setPlayer(player);
            binding.playerViewVideo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            binding.playerViewVideo.setUseController(Const.isSwipe);

            if (type.equals("livetv")) {
                Log.d("mytag", "here onPlayerError-----------------------------------" + type);
                player.setPlayWhenReady(true);
            } else {
                player.setPlayWhenReady(popupPlayer == null);
            }
            if (!playBackUrl.equals("")) {
                Log.d("mytag", "here playBackUrl-----------------------------------" + playBackUrl);
                Uri uri = Uri.parse(playBackUrl);
                MediaSource mediaSource = buildMediaSource(uri, null);
                player.prepare(mediaSource, true, true);
                player.seekTo(currentPosition);
            }
        }
    }

    private MediaSource buildMediaSource(Uri uri, @Nullable String overrideExtension) {
        @C.ContentType int type = Util.inferContentType(uri, overrideExtension);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name)))))
                        .createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name)))))
                        .createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name)))))
                        .createMediaSource(uri);
            case C.TYPE_OTHER:
                DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory()
                        .setConstantBitrateSeekingEnabled(true);
                return new ExtractorMediaSource.Factory(new DefaultHttpDataSourceFactory(Util.getUserAgent(context, context.getResources().getString(R.string.app_name))))
                        .setExtractorsFactory(extractorsFactory)
                        .createMediaSource(uri);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }

    @Override
    public void showAssetOptions(SongDetails data, String type) {
        View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.asset_options_dialog), null);
        //set favorites
        Log.d("mytag", "type home - " + type);
        ((Button) dialog_view.findViewById(R.id.add_to_fav)).setText(type.equals(getResources().getString(R.string.type_fav)) ? getResources().getString(R.string.remove_from_favorites) : getResources().getString(R.string.add_to_favorites));
        ((Button) dialog_view.findViewById(R.id.add_to_playlist)).setText(type.equals(getResources().getString(R.string.type_playlist_songs)) ? getResources().getString(R.string.remove_from_playlist) : getResources().getString(R.string.add_to_playlist));
        final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                .setCancelable(false)
                .setView(dialog_view)
                .show();

        if (dialog.getWindow() != null)
            dialog.getWindow().getDecorView().getBackground().setAlpha(0);
        dialog_view.findViewById(R.id.add_to_fav).setOnClickListener(view -> {
            if (type.equals(getResources().getString(R.string.type_fav)) && PlaylistFavListingFragment.getInstance() != null) {
                addRemoveAudioToFavourites(data, type);
            } else {
                Log.d("mytag", "fav status - " + data.getFavouritesStatus());
                if (data.getFavouritesStatus())
                    AppUtil.show_Snackbar(context, binding.mainContainer, getResources().getString(R.string.song_exist_in_fav), false);
                else addRemoveAudioToFavourites(data, type);
            }
            dialog.dismiss();
        });
        dialog_view.findViewById(R.id.add_to_playlist).setOnClickListener(view -> {
            dialog.dismiss();
            if (type.equals(getResources().getString(R.string.type_playlist_songs)))
                removeSongFromPlaylist(data);
            else
                showPlaylistOptions(data);
        });
        dialog_view.findViewById(R.id.cancel).setOnClickListener(view -> dialog.dismiss());
    }

    @Override
    public void addSongToPlaylist(SongDetails detailsData, String path) {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                jsonObject.put("playlist_id", path);
                jsonObject.put("song_id", detailsData.getSongId());
                jsonObject.put("menu_id", detailsData.getMenuId());
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody add_song_playlist = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().add_song_playlist(add_song_playlist, "AddPlaylistSongs").enqueue(new Callback<AddPlaylistSongsResponse>() {
                @Override
                public void onResponse(@NonNull Call<AddPlaylistSongsResponse> call, @NonNull Response<AddPlaylistSongsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            binding.loader.setVisibility(View.GONE);
                            AppUtil.show_Snackbar(HomeScreen.this, binding.frameContainer, response.body().getMessage(), false);
                            if (NowPlayingFragment.getInstance() != null)
                                NowPlayingFragment.getInstance().getMusicDetail(MediaController.getInstance().getPlayingSongDetail().getMenuId(), MediaController.getInstance().getPlayingSongDetail().getSongId());
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                        }
                    } else {
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        if (response.errorBody() != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(jsonObject.getString("message")));
                            } catch (JSONException | IOException e) {
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                                e.printStackTrace();
                            }
                        } else {
                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        }
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPlaylistSongsResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    @Override
    public void callTotalShared(ArrayList<SongDetails> detailsData) {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                jsonObject.put("song_id", detailsData.get(0).getSongId());
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_total_shared = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_total_shared(get_total_shared).enqueue(new Callback<TotalSharedResponse>() {
                @Override
                public void onResponse(@NonNull Call<TotalSharedResponse> call, @NonNull Response<TotalSharedResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameContainer);
                            if (fragment instanceof NowPlayingFragment) {
                                NowPlayingFragment.getInstance().setShareCount(response.body().getTotalSharedStatus(), response.body().getTotalShared());
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TotalSharedResponse> call, @NonNull Throwable t) {
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    @Override
    public void removeSongFromPlaylist(SongDetails detailsData) {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                jsonObject.put("playlist_id", detailsData.getPlaylistId());
                jsonObject.put("song_id", detailsData.getSongId());
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody add_song_playlist = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().add_song_playlist(add_song_playlist, "RemovePlaylistSongs").enqueue(new Callback<AddPlaylistSongsResponse>() {
                @Override
                public void onResponse(@NonNull Call<AddPlaylistSongsResponse> call, @NonNull Response<AddPlaylistSongsResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            binding.loader.setVisibility(View.GONE);
                            AppUtil.show_Snackbar(HomeScreen.this, binding.frameContainer, response.body().getMessage(), false);
                            if (PlaylistFavListingFragment.getInstance() != null)
                                PlaylistFavListingFragment.getInstance().setPlaylistSongs();
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                        }
                    } else {
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        if (response.errorBody() != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(jsonObject.getString("message")));
                            } catch (JSONException | IOException e) {
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                                e.printStackTrace();
                            }
                        } else {
                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        }
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPlaylistSongsResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    @Override
    public void addRemoveAudioToFavourites(SongDetails data, String type) {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("menu_id", data.getMenuId());
                jsonObject.put("song_id", data.getSongId());
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody add_fav = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().add_fav(add_fav, "AddRemoveSong").enqueue(new Callback<FavSongResponse>() {
                @Override
                public void onResponse(@NonNull Call<FavSongResponse> call, @NonNull Response<FavSongResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            AppUtil.show_Snackbar(context, binding.frameContainer, response.body().getMessage(), false);
                            Log.d("mytag", "type remove fav- " + type);
                            if ((type != null && type.equals(getResources().getString(R.string.type_fav))) && PlaylistFavListingFragment.getInstance() != null) {
                                PlaylistFavListingFragment.getInstance().setFavSongs();
                                Log.d("mytag", "in set fav");
                            }
                            if (NowPlayingFragment.getInstance() != null)
                                NowPlayingFragment.getInstance().setFav(response.body().getFavoritesStatus(), response.body().getFavoritesCount());
                            data.setFavouritesStatus(response.body().getFavoritesStatus());
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });

                        }
                    } else {
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<FavSongResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    @Override
    public void showPlaylistOptions(SongDetails detailsData) {
        View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.asset_playlist_options_dialog), null);
        final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                .setCancelable(false)
                .setView(dialog_view)
                .show();

        if (dialog.getWindow() != null)
            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

        dialog_view.findViewById(R.id.create_new_playlist).setOnClickListener(view -> {
            dialog.dismiss();
            createPlaylistOptions(detailsData);
        });
        dialog_view.findViewById(R.id.existing_playlist).setOnClickListener(view -> {
            dialog.dismiss();
            existingPlaylistOptions(detailsData);
        });
        dialog_view.findViewById(R.id.cancel).setOnClickListener(view -> dialog.dismiss());

    }

    private void createPlaylistOptions(SongDetails detailsData) {
        View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.create_new_playlist_dialog), null);
        final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                .setCancelable(false)
                .setView(dialog_view)
                .show();

        if (dialog.getWindow() != null)
            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

        TextInputEditText playlistInput = dialog_view.findViewById(R.id.playlist_input);
        TextView error = dialog_view.findViewById(R.id.error);
        dialog_view.findViewById(R.id.error).setVisibility(View.GONE);

        playlistInput.setOnFocusChangeListener((view, isFocused) -> {
            if (isFocused) {
                error.setText("");
            }
        });

        playlistInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence text, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                error.setText("");
            }

            @Override
            public void afterTextChanged(Editable text) {
            }
        });

        dialog_view.findViewById(R.id.create_new_playlist).setOnClickListener(view -> {
            if (playlistInput.getText() == null) {
                error.setVisibility(View.VISIBLE);
                error.setText(getResources().getString(R.string.playlist_name_empty));
            } else if (playlistInput.getText().toString().trim().length() == 0) {
                error.setVisibility(View.VISIBLE);
                error.setText(getResources().getString(R.string.playlist_name_empty));
            } else {
                dialog.dismiss();
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(binding.frameContainer.getWindowToken(), 0);
                createPlaylist(detailsData, playlistInput.getText().toString());
            }
        });
        dialog_view.findViewById(R.id.cancel).setOnClickListener(view -> dialog.dismiss());
    }

    private void createPlaylist(SongDetails detailsData, String playlistName) {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                jsonObject.put("playlist_name", playlistName);
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody create_playlist = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().create_playlist(create_playlist, "PlaylistAdd").enqueue(new Callback<AddPlaylistResponse>() {
                @Override
                public void onResponse(@NonNull Call<AddPlaylistResponse> call, @NonNull Response<AddPlaylistResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(binding.frameContainer.getWindowToken(), 0);
                            existingPlaylistOptions(detailsData);
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(binding.frameContainer.getWindowToken(), 0);
                            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                        }
                    } else {
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        if (response.errorBody() != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(jsonObject.getString("message")));
                            } catch (JSONException | IOException e) {
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                                e.printStackTrace();
                            }
                        } else {
                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        }
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPlaylistResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    private void existingPlaylistOptions(SongDetails detailsData) {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_playlist = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_playlist(get_playlist, "PlaylistList").enqueue(new Callback<ExistingPlaylistResponse>() {
                @Override
                public void onResponse(@NonNull Call<ExistingPlaylistResponse> call, @NonNull Response<ExistingPlaylistResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null) {
                                if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                    binding.loader.setVisibility(View.GONE);
                                    View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.existing_playlist_dialog), null);
                                    final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                            .setCancelable(false)
                                            .setView(dialog_view)
                                            .show();

                                    if (dialog.getWindow() != null)
                                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                    RecyclerView listing = dialog_view.findViewById(R.id.listing);
                                    listing.setHasFixedSize(true);
                                    listing.setLayoutManager(new LinearLayoutManager(HomeScreen.this));
                                    listing.setAdapter(new ExistingPlaylistAdapter(HomeScreen.this, getSupportFragmentManager(), response.body().getData(), false, detailsData, dialog));
                                    dialog_view.findViewById(R.id.cancel).setOnClickListener(view -> {
                                        binding.loader.setVisibility(View.GONE);
                                        dialog.dismiss();
                                    });
                                } else {
                                    binding.loader.setVisibility(View.GONE);
                                    View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                                    final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                            .setCancelable(false)
                                            .setView(dialog_view)
                                            .show();

                                    if (dialog.getWindow() != null)
                                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_existing_playlist));
                                    ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.create_new));
                                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                                    dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                        dialog.dismiss();
                                        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(binding.frameContainer.getWindowToken(), 0);
                                        createPlaylistOptions(detailsData);
                                    });
                                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                                }
                            } else {
                                binding.loader.setVisibility(View.GONE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                        }
                    } else {
                        binding.loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        if (response.errorBody() != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(jsonObject.getString("message")));
                            } catch (JSONException | IOException e) {
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                                e.printStackTrace();
                            }
                        } else {
                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        }
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ExistingPlaylistResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NotNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) Objects.requireNonNull(this.getSystemService(Context.INPUT_METHOD_SERVICE))).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void go_back() {
        onBackPressed();
    }

    private void loadAssetDetails(SongDetails playingSongDetail) {
        binding.playerView.onPause();
        binding.playerViewVideo.onPause();
        if (player != null) {
            Log.d("mytag", "here is load asset details called-----------------------------" + player);
            player.release();
            player = null;
        }
        if (popupPlayer != null) {
            popupPlayer.release();
            popupPlayer = null;
        }
        if (binding.motionContainer.getCurrentState() == R.id.end) {
            binding.motionContainer.transitionToStart();
        }
        if (playingSongDetail.getType() != null && playingSongDetail.getType().toLowerCase().equals(getResources().getString(R.string.radio).toLowerCase())) {
            binding.miniPlayerContainer.setVisibility(View.GONE);
            binding.popupPlayerController.setVisibility(View.GONE);
            binding.songSeekBarPopup.setVisibility(View.VISIBLE);
            binding.songSeekBarPopupMini.setVisibility(View.VISIBLE);
            binding.songSeekBarPopup.setEnabled(false);
            binding.songSeekBarPopupMini.setEnabled(false);
            int progressbarValue = (int) (MediaController.getInstance().getPlayingSongDetail().audioProgress * 100);
            String currentTimeString = String.format(Locale.ENGLISH, "%02d:%02d", (MediaController.getInstance().getPlayingSongDetail().audioProgressSec) / 60, MediaController.getInstance().getPlayingSongDetail().audioProgressSec % 60);
            String totalTimeString = String.format(Locale.ENGLISH, "%02d:%02d", (MediaController.getInstance().getPlayingSongDetail().audioProgressTotalSec) / 60, MediaController.getInstance().getPlayingSongDetail().audioProgressTotalSec % 60);
            binding.startTime.setText(currentTimeString);
            binding.startTimeMini.setText(currentTimeString);
            binding.endTime.setText(totalTimeString);
            binding.endTimeMini.setText(totalTimeString);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.songSeekBarPopup.setProgress(progressbarValue, true);
                binding.songSeekBarPopupMini.setProgress(progressbarValue, true);
            } else {
                binding.songSeekBarPopup.setProgress(progressbarValue);
                binding.songSeekBarPopupMini.setProgress(progressbarValue);
            }
            binding.songShuffle.setEnabled(false);
            binding.songShuffleMini.setEnabled(false);
            binding.songPreviousPopup.setEnabled(false);
            binding.songNextPopup.setEnabled(false);
            binding.songPreviousMini.setEnabled(false);
            binding.songNextMini.setEnabled(false);
            binding.songRepeat.setEnabled(false);
            binding.songRepeatMini.setEnabled(false);
            binding.songDescriptionMini.setText(getResources().getString(R.string.radio));
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameContainer);
            if (fragment instanceof RadioFragment) {
                binding.miniPlayerContainer.setVisibility(View.GONE);
                binding.popupPlayerController.setVisibility(View.GONE);
                RadioFragment.getInstance().setRadioPLayPause();
            }
        } else {
            binding.songSeekBarPopup.setVisibility(View.VISIBLE);
            binding.songSeekBarPopupMini.setVisibility(View.VISIBLE);
            binding.songSeekBarPopup.setEnabled(true);
            binding.songSeekBarPopupMini.setEnabled(true);
            binding.songShuffle.setVisibility(View.VISIBLE);
            binding.songShuffle.setEnabled(true);
            binding.songShuffleMini.setVisibility(View.VISIBLE);
            binding.songShuffleMini.setEnabled(true);
            binding.songPreviousPopup.setVisibility(View.VISIBLE);
            binding.songPreviousPopup.setEnabled(true);
            binding.songNextPopup.setVisibility(View.VISIBLE);
            binding.songNextPopup.setEnabled(true);
            binding.songPreviousMini.setVisibility(View.VISIBLE);
            binding.songPreviousMini.setEnabled(true);
            binding.songNextMini.setVisibility(View.VISIBLE);
            binding.songNextMini.setEnabled(true);
            binding.songRepeat.setVisibility(View.VISIBLE);
            binding.songRepeat.setEnabled(true);
            binding.songRepeatMini.setVisibility(View.VISIBLE);
            binding.songRepeatMini.setEnabled(true);
            int progressbarValue = (int) (MediaController.getInstance().getPlayingSongDetail().audioProgress * 100);
            String currentTimeString = String.format(Locale.ENGLISH, "%02d:%02d", (MediaController.getInstance().getPlayingSongDetail().audioProgressSec) / 60, MediaController.getInstance().getPlayingSongDetail().audioProgressSec % 60);
            String totalTimeString = String.format(Locale.ENGLISH, "%02d:%02d", (MediaController.getInstance().getPlayingSongDetail().audioProgressTotalSec) / 60, MediaController.getInstance().getPlayingSongDetail().audioProgressTotalSec % 60);
            binding.startTime.setText(currentTimeString);
            binding.startTimeMini.setText(currentTimeString);
            binding.endTime.setText(totalTimeString);
            binding.endTimeMini.setText(totalTimeString);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.songSeekBarPopup.setProgress(progressbarValue, true);
                binding.songSeekBarPopupMini.setProgress(progressbarValue, true);
            } else {
                binding.songSeekBarPopup.setProgress(progressbarValue);
                binding.songSeekBarPopupMini.setProgress(progressbarValue);
            }

            binding.songPlayPausePopup.setSelected(!MediaController.getInstance().isAudioPaused());
            binding.songPlayPauseMini.setSelected(!MediaController.getInstance().isAudioPaused());

            binding.songNameMini.setText(playingSongDetail.getSongName());
            binding.songNamePopup.setText(playingSongDetail.getSongName());
            if (playingSongDetail.getSongArtist() != null && !playingSongDetail.getSongArtist().isEmpty()) {
                binding.songDescriptionMini.setVisibility(View.VISIBLE);
                binding.songDescriptionPopup.setVisibility(View.VISIBLE);
                binding.songDescriptionMini.setText(playingSongDetail.getSongArtist());
                binding.songDescriptionPopup.setText(playingSongDetail.getSongArtist());
            } else {
                binding.songDescriptionMini.setVisibility(View.VISIBLE);
                binding.songDescriptionPopup.setVisibility(View.GONE);
            }

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameContainer);
            if (fragment instanceof NowPlayingFragment) {
                Log.d("mytag", "here is NowPlayingFragment-----------------");
                binding.miniPlayerContainer.setVisibility(View.GONE);
                binding.popupPlayerController.setVisibility(View.GONE);
            } else if (fragment instanceof RadioFragment) {
                binding.miniPlayerContainer.setVisibility(View.GONE);
                binding.popupPlayerController.setVisibility(View.GONE);
            } else if (fragment instanceof LiveTvFragment) {
                binding.miniPlayerContainer.setVisibility(View.GONE);
                binding.popupPlayerController.setVisibility(View.GONE);
            } else {
                binding.miniPlayerContainer.setVisibility(View.VISIBLE);
                binding.popupPlayerController.setVisibility(View.VISIBLE);
            }
        }
    }

    public void addObserver() {
//        int TAG_Observer = MediaController.generateObserverTag();
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioDidReset);
        NotificationManager.getInstance().addObserver(this, NotificationManager.repeatChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.shuffleChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioBuffered);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioPlayStateChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioDidStarted);
        NotificationManager.getInstance().addObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.newaudioloaded);
    }

    public void removeObserver() {
        NotificationManager.getInstance().removeObserver(this, NotificationManager.repeatChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.shuffleChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioDidReset);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioBuffered);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioPlayStateChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioDidStarted);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.audioProgressDidChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.newaudioloaded);
    }

    @Override
    public void showMainScreen() {
        binding.popupPlayerContainer.setVisibility(View.VISIBLE);
        binding.mainScreen.setVisibility(View.GONE);
    }

    @Override
    public void lock_unlock_Drawer() {
        Log.d("mytag", "lock_unlock_Drawer called");
        if (binding.drawerContainer.isDrawerOpen(GravityCompat.START)) {
            binding.drawerContainer.closeDrawer(GravityCompat.START);
            binding.drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            binding.drawerContainer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void notifyPlayerCleaned() {
        binding.mainScreen.setVisibility(View.VISIBLE);
        binding.popupPlayerContainer.setVisibility(View.GONE);
        binding.miniPlayerContainer.setVisibility(View.GONE);
        binding.popupPlayerController.setVisibility(View.GONE);

        binding.songPlayPauseMini.setSelected(false);
        binding.songPlayPausePopup.setSelected(false);
        binding.songPlayPauseMini.setVisibility(View.VISIBLE);
        binding.songPlayPausePopup.setVisibility(View.VISIBLE);
        binding.songLoaderMini.setVisibility(View.GONE);
        binding.songLoaderPopup.setVisibility(View.GONE);

        binding.songRepeat.setSelected(MusicPreference.getRepeat(HomeScreen.this) == 1);
        MediaController.repeatMode = binding.songRepeat.isSelected() ? 1 : 0;
        binding.songRepeatMini.setSelected(MusicPreference.getRepeat(HomeScreen.this) == 1);
        MediaController.repeatMode = binding.songRepeatMini.isSelected() ? 1 : 0;
        binding.songShuffle.setSelected(MusicPreference.getShuffle(HomeScreen.this));
        MediaController.shuffleMusic = binding.songShuffle.isSelected();
        binding.songShuffleMini.setSelected(MusicPreference.getShuffle(HomeScreen.this));
        MediaController.shuffleMusic = binding.songShuffleMini.isSelected();
        MediaController.shuffleList(MusicPreference.playlist);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    public void show_dismiss_ProgressLoader(int visibility) {
        binding.loader.setVisibility(visibility);
    }

    @Override
    public void showFragment() {
        binding.popupPlayerContainer.setVisibility(View.GONE);
        binding.mainScreen.setVisibility(View.VISIBLE);
    }

    private void get_instagram() {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(HomeScreen.this, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_instagram = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_instagram(get_instagram).enqueue(new Callback<InstagramResponse>() {
                @Override
                public void onResponse(@NonNull Call<InstagramResponse> call, @NonNull Response<InstagramResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getLink() != null && !response.body().getLink().isEmpty())
                                instagramPath = response.body().getLink();
                            else instagramPath = "https://www.instagram.com/";
                            Prefs.getPrefInstance().setValue(HomeScreen.this, Const.INSTAGRAM_PATH, instagramPath);
                        } else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        }else {
                            instagramPath = "https://www.instagram.com/";
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<InstagramResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    instagramPath = "https://www.instagram.com/";
                }
            });
        } else {
        }
    }

    private void callSongPlay() {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                jsonObject.put("song_id", MediaController.getInstance().getPlayingSongDetail().getSongId());
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_song_play = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_song_play(call_song_play).enqueue(new Callback<SongPlayResponse>() {
                @Override
                public void onResponse(@NonNull Call<SongPlayResponse> call, @NonNull Response<SongPlayResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                        } else {
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SongPlayResponse> call, @NonNull Throwable t) {
                }
            });
        } else {
        }
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameContainer);
        if (MediaController.getInstance().getPlayingSongDetail() != null && MediaController.getInstance().getPlayingSongDetail().getType() != null && MediaController.getInstance().getPlayingSongDetail().getType().toLowerCase().equals(getResources().getString(R.string.radio).toLowerCase())) {
            if (id == NotificationManager.audioDidStarted || id == NotificationManager.audioPlayStateChanged || id == NotificationManager.audioDidReset) {
                loadAssetDetails(MediaController.getInstance().getPlayingSongDetail());
            } else if (id == NotificationManager.audioBuffered) {
                int progressbarValue = (int) (MediaController.getInstance().getPlayingSongDetail().audioBufferProgress * 100);
                if (fragment instanceof RadioFragment)
                    RadioFragment.getInstance().setSecondaryProgress(progressbarValue);
            } else if (id == NotificationManager.audioProgressDidChanged) {
                if (fragment instanceof RadioFragment) {
                    RadioFragment.getInstance().setRadioSongLoader(false);
                }
            }
        } else {
            if (id == NotificationManager.audioDidStarted || id == NotificationManager.audioPlayStateChanged || id == NotificationManager.audioDidReset) {
                loadAssetDetails(MediaController.getInstance().getPlayingSongDetail());
                if (fragment instanceof NowPlayingFragment) {
                    NowPlayingFragment.getInstance().getMusicDetail(MediaController.getInstance().getPlayingSongDetail().getMenuId(), MediaController.getInstance().getPlayingSongDetail().getSongId());
                }
            } else if (id == NotificationManager.repeatChanged) {
                binding.songRepeat.setSelected((boolean) args[0]);
                binding.songRepeatMini.setSelected((boolean) args[0]);
            } else if (id == NotificationManager.shuffleChanged) {
                binding.songShuffle.setSelected((boolean) args[0]);
                binding.songShuffleMini.setSelected((boolean) args[0]);
            } else if (id == NotificationManager.audioBuffered) {
                int progressbarValue = (int) (MediaController.getInstance().getPlayingSongDetail().audioBufferProgress * 100);
                binding.songSeekBarPopup.setSecondaryProgress(progressbarValue);
                binding.songSeekBarPopupMini.setSecondaryProgress(progressbarValue);
            } else if (id == NotificationManager.audioProgressDidChanged) {
                int progressbarValue = (int) (MediaController.getInstance().getPlayingSongDetail().audioProgress * 100);
                String currentTimeString = String.format(Locale.ENGLISH, "%02d:%02d", (MediaController.getInstance().getPlayingSongDetail().audioProgressSec) / 60, MediaController.getInstance().getPlayingSongDetail().audioProgressSec % 60);
                String totalTimeString = String.format(Locale.ENGLISH, "%02d:%02d", (MediaController.getInstance().getPlayingSongDetail().audioProgressTotalSec) / 60, MediaController.getInstance().getPlayingSongDetail().audioProgressTotalSec % 60);
                binding.startTime.setText(currentTimeString);
                binding.startTimeMini.setText(currentTimeString);
                binding.endTime.setText(totalTimeString);
                binding.endTimeMini.setText(totalTimeString);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    binding.songSeekBarPopup.setProgress(progressbarValue, true);
                    binding.songSeekBarPopupMini.setProgress(progressbarValue, true);
                } else {
                    binding.songSeekBarPopup.setProgress(progressbarValue);
                    binding.songSeekBarPopupMini.setProgress(progressbarValue);
                }
                if (fragment instanceof NowPlayingFragment)
                    NowPlayingFragment.getInstance().setSongLoader(false);
                binding.songLoaderMini.setVisibility(View.GONE);
                binding.songLoaderPopup.setVisibility(View.GONE);
                binding.songPlayPauseMini.setVisibility(View.VISIBLE);
                binding.songPlayPausePopup.setVisibility(View.VISIBLE);

            }
        }
    }

    @Override
    public void newSongLoaded(Object... args) {
        playCount--;
        songDetail = (SongDetails) args[0];
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frameContainer);
        HomeCategoryAdapter.onLoadSong(String.valueOf(songDetail.getSongId()));
        if (fragment instanceof AssetDetailFragment) {
            ((AssetDetailFragment) fragment).onLoadSong(String.valueOf(songDetail.getSongId()));
        } else if (fragment instanceof PlaylistFavListingFragment) {
            ((PlaylistFavListingFragment) fragment).onLoadSong(String.valueOf(songDetail.getSongId()));
        } else if (fragment instanceof CategoryViewAllFragment) {
            ((CategoryViewAllFragment) fragment).onLoadSong(String.valueOf(songDetail.getSongId()));
        } else if (fragment instanceof GridMusicFragment) {
            ((GridMusicFragment) fragment).onLoadSong(String.valueOf(songDetail.getSongId()));
        } else if (fragment instanceof SearchFragment) {
            ((SearchFragment) fragment).onLoadSong(String.valueOf(songDetail.getSongId()));
        }
        if (MediaController.getInstance().getPlayingSongDetail().getType() != null &&
                MediaController.getInstance().getPlayingSongDetail().getType().toLowerCase().equals(getResources().getString(R.string.radio).toLowerCase())) {
            if (fragment instanceof RadioFragment) {
                RadioFragment.getInstance().setRadioSongLoader(true);
            }
        } else {
            binding.songLoaderMini.setVisibility(View.VISIBLE);
            binding.songLoaderPopup.setVisibility(View.VISIBLE);
            binding.songPlayPauseMini.setVisibility(View.GONE);
            binding.songPlayPausePopup.setVisibility(View.INVISIBLE);
            callSongPlay();
            if (fragment instanceof NowPlayingFragment) {
                NowPlayingFragment.getInstance().setSongLoader(true);
            }
        }

        if (playCount == -1) {
            android.util.Log.d("mytag", "interstial add key in onresume isss222222222222-----------" + playCount);
            playCount = 3;
            homevisible = true;
            displayInterstitial();
        }

        //video player
        binding.playerViewVideo.onPause();
        binding.playerView.onPause();
        if (player != null) {
            player.release();
            player = null;
        }
        if (popupPlayer != null) {
            popupPlayer.release();
            popupPlayer = null;
        }
        binding.motionContainer.transitionToStart();
    }

    @Override
    public void preparePlayback() {
    }

    @Override
    public void onVisibilityChange(int visibility) {

    }

    private void animateOpenPopUpPlayer(boolean doAnimate, SongDetails detailsData) {
        Log.d("mytag", "animateOpenPopUpPlayer called");
        if (NowPlayingFragment.getInstance() != null)
            AppUtil.setup_Fragment(getSupportFragmentManager(), NowPlayingFragment.newInstance(),
                    MediaController.getInstance().getPlayingSongDetail().getMenuId()
                    , "", "", MediaController.getInstance().getPlayingSongDetail().getSongId()
                    , "DrawerInner", "DrawerInner", true);
        showFragment();
        binding.mainScreen.setVisibility(View.VISIBLE);
        if (doAnimate)
            ViewAnimator
                    .animate(binding.mainScreen)
                    .duration(500)
                    .translationY(2000, 0)
                    .onStart(() -> {
                        Log.d("mytag", "here111111111 is touch click listener ");
                        if (MediaController.getInstance().getPlayingSongDetail() != null)
                            binding.mainScreen.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                        binding.popupPlayerContainer.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                    })
                    .onStop(() -> {
                        binding.mainScreen.setLayerType(View.LAYER_TYPE_NONE, null);
                        binding.popupPlayerContainer.setLayerType(View.LAYER_TYPE_NONE, null);
                        binding.popupPlayerContainer.setVisibility(View.GONE);
                    })
                    .start();
        else binding.popupPlayerContainer.setVisibility(View.GONE);
    }

    private void animateClosePopUpPlayer() {
        binding.mainScreen.setVisibility(View.VISIBLE);
        ViewAnimator.animate(binding.popupPlayerContainer)
                .duration(500)
                .translationY(0, 2000)
                .onStart(() -> {
                    binding.mainScreen.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                    binding.popupPlayerContainer.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                })
                .onStop(() -> {
                    binding.mainScreen.setLayerType(View.LAYER_TYPE_NONE, null);
                    binding.popupPlayerContainer.setLayerType(View.LAYER_TYPE_NONE, null);
                    binding.popupPlayerContainer.setVisibility(View.GONE);
                    ViewAnimator.animate(binding.popupPlayerContainer).duration(500).translationY(0, 2000).start();
                })
                .start();
    }

    private void getSliderData() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_social_media = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().getBannerSlider(call_social_media).enqueue(new Callback<BannerSlider>() {
                @Override
                public void onResponse(@NonNull Call<BannerSlider> call, @NonNull Response<BannerSlider> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                bannerSliderData = response.body().getData();
                                adapterMainviewPager = new AdapterMainviewPager(context, bannerSliderData);
                                binding.mPager.setAdapter(adapterMainviewPager);
                                binding.mPager.setPagingEnabled(true);
                                binding.indicator.setViewPager(binding.mPager);
                                startImageSliderTimer();
                            } else {
                                binding.noDataSlider.setVisibility(View.GONE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        } else {
                            binding.noDataSlider.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BannerSlider> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.noDataSlider.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.noDataSlider.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void getMainHomeData() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("device", "android");
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                Log.d("mytag", "jsonobject to string-----------------" + jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody getHome = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().getHomeData(getHome, 200/*, CURRENT_PAGE*/).enqueue(new Callback<Home>() {
                @Override
                public void onResponse(@NonNull Call<Home> call, @NonNull Response<Home> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                linearLayoutManager.setAutoMeasureEnabled(true);
                                binding.rvMain.setLayoutManager(linearLayoutManager);
                                binding.rvMain.setHasFixedSize(false);
                                binding.rvMain.setNestedScrollingEnabled(false);

                                HomeListingAdapter homeListingAdapter = new HomeListingAdapter(context, getSupportFragmentManager(), response.body().getData());
                                binding.rvMain.setAdapter(homeListingAdapter);
                                binding.rvMain.setVisibility(View.VISIBLE);
                                binding.noDataFound.setVisibility(View.GONE);

                            } else {
                                binding.noDataFound.setVisibility(View.VISIBLE);
                            }
                            binding.loader.setVisibility(View.GONE);
                        } else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        }else {
                            binding.noDataFound.setVisibility(View.VISIBLE);
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });

                        }
                    } else {
                        binding.noDataFound.setVisibility(View.VISIBLE);
                        binding.loader.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Home> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.noDataFound.setVisibility(View.VISIBLE);
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            binding.noDataFound.setVisibility(View.VISIBLE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    public static void expand(final View v, int duration, int targetHeight) {

        int prevHeight = v.getHeight();

        v.setVisibility(View.VISIBLE);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    public static void collapse(final View v, int duration, int targetHeight) {
        int prevHeight = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    private void askpermisiion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, 2084);
        }
    }

    private void getGoogleAdView() {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            binding.loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().getGooleAd().enqueue(new Callback<ADView>() {
                @Override
                public void onResponse(@NonNull Call<ADView> call, @NonNull Response<ADView> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                String android_key = response.body().getData().get(0).getAndroidBanner();
                                android.util.Log.d("mytag", "my ad key is-----" + android_key);
                                String android_interstitial = response.body().getData().get(0).getAndroidInterstitial();
                                android.util.Log.d("mytag", "android_interstitial ad key is-----" + android_interstitial);

                                Prefs.getPrefInstance().setValue(context, Const.Google_add_key, android_key);
                                Prefs.getPrefInstance().setValue(context, Const.INTERESTITIAL_KEY, android_interstitial);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ADView> call, @NonNull Throwable t) {
                    t.printStackTrace();
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void startImageSliderTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {

                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {

                                int currentItem = binding.mPager.getCurrentItem() + 1;
                                int totalItem = adapterMainviewPager.getCount();
                                if (currentItem == totalItem) {
                                    binding.mPager.setCurrentItem(0);
                                } else {
                                    binding.mPager.setCurrentItem(currentItem);
                                }
                            }
                        });
                    }
                }
            }, 0, 5000);
        }
    }

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            Durisimo.applicationHandler.post(runnable);
        } else {
            Durisimo.applicationHandler.postDelayed(runnable, delay);
        }
    }
}



