package com.durisimomusic.util;

import com.durisimomusic.data.models.asset.SongDetails;
import com.durisimomusic.data.models.video.MenuItemsData;

import java.util.ArrayList;

public interface AppManageInterface {
    void go_back();
    void lock_unlock_Drawer();
    void notifyPlayerCleaned();
    void show_dismiss_ProgressLoader(int visibility);
    void showFragment();
    void showAssetOptions(SongDetails data, String type);
    void addSongToPlaylist(SongDetails detailsData, String path);
    void callTotalShared(ArrayList<SongDetails> detailsData);
    void removeSongFromPlaylist(SongDetails detailsData);
    void addRemoveAudioToFavourites(SongDetails data, String type);
    void showPlaylistOptions(SongDetails detailsData);
    void miniPlayerVisibilityGone(boolean isHide);
    void setLiveTvVideo(String playBackUrl, String type, long currentPosition);
    void setVisibleStatus(boolean audioPlayerVisible, boolean songShouldPause, boolean videoPlayerVisible, boolean videoShouldPause, boolean videoInFullMode);
    //    void showMotionVideo(boolean isVisible,String menu_id, String video_id, String category_id);
    void showMotionVideo(MenuItemsData queue, ArrayList<MenuItemsData> data, int position, String type);
    void showYoutubeWebview(MenuItemsData queue, ArrayList<MenuItemsData> data, int position, String type);
    void setPlayerClose();
    void bottomanduppermanage(boolean isHide);
    void setPlayer();
    void closeVideoBottom();
    void showMainScreen();
    void livetvBack();
    void webviewManageMent();
    void PlayerManageMent();
    void hideBothAerrow();
    void showMainAcreen();
    void showMainAcreeninFavPlaylist();
    void hideVideoPlayer();
//    void closeVideoTVPlayer();
}