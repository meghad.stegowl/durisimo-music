package com.durisimomusic.data.models;

public class More {
    int image;
    String title;
    String image1;

    public More(String image, String name) {
        this.image1 = image;
        this.title = name;
    }

    public int getImage() {
        return image;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public More(int image, String title) {
        this.image = image;
        this.title = title;
    }
}
