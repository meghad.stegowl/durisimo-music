package com.durisimomusic.data.models.social_media;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialMediaData {

    @SerializedName("instagram_link")
    @Expose
    private String instagramLink;
    @SerializedName("twitter_link")
    @Expose
    private String twitterLink;
    @SerializedName("facebook_link")
    @Expose
    private String facebookLink;
    @SerializedName("other_link")
    @Expose
    private String otherLink;

    public String getInstagramLink() {
        return instagramLink;
    }

    public void setInstagramLink(String instagramLink) {
        this.instagramLink = instagramLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getOtherLink() {
        return otherLink;
    }

    public void setOtherLink(String otherLink) {
        this.otherLink = otherLink;
    }
}
