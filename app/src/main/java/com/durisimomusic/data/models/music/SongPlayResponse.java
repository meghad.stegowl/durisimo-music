package com.durisimomusic.data.models.music;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SongPlayResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("song_id")
    @Expose
    private String songId;
    @SerializedName("total_played")
    @Expose
    private Integer totalPlayed;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public Integer getTotalPlayed() {
        return totalPlayed;
    }

    public void setTotalPlayed(Integer totalPlayed) {
        this.totalPlayed = totalPlayed;
    }
}
