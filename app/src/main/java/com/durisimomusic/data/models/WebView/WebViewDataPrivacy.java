package com.durisimomusic.data.models.WebView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebViewDataPrivacy {
    @SerializedName("privacypolicy_id")
    @Expose
    private Integer privacypolicyId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getPrivacypolicyId() {
        return privacypolicyId;
    }

    public void setPrivacypolicyId(Integer privacypolicyId) {
        this.privacypolicyId = privacypolicyId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
