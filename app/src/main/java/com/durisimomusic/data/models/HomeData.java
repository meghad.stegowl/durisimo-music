package com.durisimomusic.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HomeData {

    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("assests")
    @Expose
    private ArrayList<HomeCategoryData> assests = null;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<HomeCategoryData> getAssests() {
        return assests;
    }

    public void setAssests(ArrayList<HomeCategoryData> assests) {
        this.assests = assests;
    }
}
