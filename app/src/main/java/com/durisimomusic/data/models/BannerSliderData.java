package com.durisimomusic.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BannerSliderData {

    @SerializedName("bannerslider_id")
    @Expose
    private Integer bannersliderId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getBannersliderId() {
        return bannersliderId;
    }

    public void setBannersliderId(Integer bannersliderId) {
        this.bannersliderId = bannersliderId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
