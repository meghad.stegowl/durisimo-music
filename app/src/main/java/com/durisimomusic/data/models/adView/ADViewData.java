package com.durisimomusic.data.models.adView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ADViewData {

    @SerializedName("googleads_id")
    @Expose
    private Integer googleadsId;
    @SerializedName("android_banner")
    @Expose
    private String androidBanner;
    @SerializedName("android_interstitial")
    @Expose
    private String androidInterstitial;
    @SerializedName("ios_banner")
    @Expose
    private String iosBanner;
    @SerializedName("ios_interstitial")
    @Expose
    private String iosInterstitial;

    public Integer getGoogleadsId() {
        return googleadsId;
    }

    public void setGoogleadsId(Integer googleadsId) {
        this.googleadsId = googleadsId;
    }

    public String getAndroidBanner() {
        return androidBanner;
    }

    public void setAndroidBanner(String androidBanner) {
        this.androidBanner = androidBanner;
    }

    public String getAndroidInterstitial() {
        return androidInterstitial;
    }

    public void setAndroidInterstitial(String androidInterstitial) {
        this.androidInterstitial = androidInterstitial;
    }

    public String getIosBanner() {
        return iosBanner;
    }

    public void setIosBanner(String iosBanner) {
        this.iosBanner = iosBanner;
    }

    public String getIosInterstitial() {
        return iosInterstitial;
    }

    public void setIosInterstitial(String iosInterstitial) {
        this.iosInterstitial = iosInterstitial;
    }
}
