package com.durisimomusic.data.remote;

import com.durisimomusic.data.models.BannerSlider;
import com.durisimomusic.data.models.Home;
import com.durisimomusic.data.models.HomeGridCategory;
import com.durisimomusic.data.models.HomeListingMusic;
import com.durisimomusic.data.models.WebView.WebViewResponse;
import com.durisimomusic.data.models.adView.ADView;
import com.durisimomusic.data.models.common.CommonResponse;
import com.durisimomusic.data.models.drawer_items.DrawerItemsResponse;
import com.durisimomusic.data.models.home.HomeItemsResponse;
import com.durisimomusic.data.models.instagram.InstagramResponse;
import com.durisimomusic.data.models.intro_slider.IntroSliderResponse;
import com.durisimomusic.data.models.live_tv.LikeUnlikeResponse;
import com.durisimomusic.data.models.live_tv.LiveTvResponse;
import com.durisimomusic.data.models.music.AddPlaylistResponse;
import com.durisimomusic.data.models.music.AddPlaylistSongsResponse;
import com.durisimomusic.data.models.music.ExistingPlaylistResponse;
import com.durisimomusic.data.models.music.FavSongResponse;
import com.durisimomusic.data.models.music.LikeDislikeSongResponse;
import com.durisimomusic.data.models.music.MusicResponse;
import com.durisimomusic.data.models.music.SongPlayResponse;
import com.durisimomusic.data.models.music.TotalSharedResponse;
import com.durisimomusic.data.models.notifications.NotificationsResponse;
import com.durisimomusic.data.models.ppv.PPV;
import com.durisimomusic.data.models.share_my_app.ShareMyAppResponse;
import com.durisimomusic.data.models.social_media.SocialMediaResponse;
import com.durisimomusic.data.models.support.SupportResponse;
import com.durisimomusic.data.models.user.LoginUserResponse;
import com.durisimomusic.data.models.user.ProfileResponse;
import com.durisimomusic.data.models.user.SkipUserResponse;
import com.durisimomusic.data.models.user.UploadImageResponse;
import com.durisimomusic.data.models.video.FavVideosResponse;
import com.durisimomusic.data.models.video.MenuItemsResponse;
import com.durisimomusic.data.models.video.MenuStatusResponse;
import com.durisimomusic.data.models.video.VideoDetailsResponse;
import com.durisimomusic.util.Const;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @POST(Const.LOGIN_USER)
    Call<LoginUserResponse> user_login(@Body RequestBody user_login);

    @POST(Const.SKIP_USER)
    Call<SkipUserResponse> skip_user(@Body RequestBody skip_user);

    @POST(Const.DRAWER_ITEMS)
    Call<DrawerItemsResponse> get_drawer_items(@Body RequestBody get_drawer_items);

    @POST(Const.REGISTER_USER)
    Call<CommonResponse> user_registering(@Body RequestBody user_registering);

    @POST(Const.PROFILE)
    Call<ProfileResponse> get_profile(@Body RequestBody user_registering, @Query("type") String type);

    @POST(Const.LOGOUT)
    Call<CommonResponse> call_logout(@Body RequestBody call_logout);

    @POST(Const.FORGOT_PASSWORD)
    Call<CommonResponse> forgot_password(@Body RequestBody forgot_password);

    @POST(Const.CHANGE_PASSWORD)
    Call<CommonResponse> change_password(@Body RequestBody change_password);

    @POST(Const.LIVE_TV_SLIDER)
    Call<LiveTvResponse> get_live_tv_slider(@Body RequestBody get_live_tv_slider);

    @POST(Const.HOME_API)
    Call<Home> getHomeData(@Body RequestBody get_carousal, @Query("limit") int limit/*, @Query("page") int page*/);

    @POST(Const.HOME_GRID_CATEGORY_API)
    Call<HomeGridCategory> getViewallGridCategory(@Body RequestBody requestBody,@Path("category_id") int category_id,
                                                  @Query("limit") int limit, @Query("page") int page);

    @POST(Const.HOME_ITEMS)
    Call<HomeItemsResponse> get_home_items(@Body RequestBody get_home_items);

    @POST(Const.CALL_SONG_PLAY)
    Call<SongPlayResponse> call_song_play(@Body RequestBody call_song_play);

    @POST(Const.SHARE_MY_APP)
    Call<ShareMyAppResponse> share_my_app(@Body RequestBody share_my_app);

    @POST(Const.VIDEO_LIKE_UNLIKE)
    Call<LikeUnlikeResponse> call_like_unlike(@Body RequestBody call_like_unlike);

    @POST(Const.GET_NOTIFICATIONS)
    Call<NotificationsResponse> call_notifications(@Body RequestBody call_notifications);

    @POST(Const.SOCIAL_MEDIA)
    Call<SocialMediaResponse> call_social_media(@Body RequestBody call_social_media);

    @POST(Const.MENU_ITEMS)
    Call<MenuItemsResponse> call_menu_items(@Body RequestBody call_menu_items, @Query("menu_id") String menu_id,
                                            @Query("menu_type") String menu_type);

    @POST(Const.MENU_ITEMS)
    Call<MenuItemsResponse> call_menu_items(@Body RequestBody call_menu_items, @Query("menu_id") String menu_id,
                                            @Query("menu_type") String menu_type,
                                            @Query("limit") int limit, @Query("page") int page);

    @POST(Const.MUSIC_SONGS)
    Call<HomeListingMusic> get_music(@Body RequestBody call_menu_items, @Query("menu_id") String menu_id,
                                           @Query("menu_type") String menu_type,
                                     @Query("limit") int limit, @Query("page") int page);

    @POST(Const.GET_SEARCH)
    Call<MusicResponse> get_search_result(@Body RequestBody get_search_result, @Query("limit") int limit, @Query("page") int page);

    @POST(Const.MENU_ITEMS)
    Call<MusicResponse> call_radio(@Body RequestBody call_menu_items, @Query("menu_id") String menu_id, @Query("menu_type") String menu_type);

    @POST(Const.CATEGORY_ITEMS)
    Call<MenuItemsResponse> get_category_videos(@Body RequestBody get_video_detail, @Query("menu_id") String menu_id,
                                                @Query("category_id") String category_id, @Query("category_for") String category_for,
                                                @Query("limit") int limit, @Query("page") int page);

    @POST(Const.CATEGORY_ITEMS)
    Call<MusicResponse> get_category_music(@Body RequestBody get_category_music, @Query("menu_id") String menu_id,
                                           @Query("category_id") String category_id, @Query("category_for") String category_for);

    @POST(Const.CATEGORY_ITEMS)
    Call<MusicResponse> get_category_music(@Body RequestBody get_category_music, @Query("menu_id") String menu_id,
                                           @Query("category_id") String category_id, @Query("category_for") String category_for
            , @Query("limit") int limit, @Query("page") int page);


    @POST(Const.CATEGORY_ITEM_DETAILS)
    Call<MusicResponse> get_now_playing(@Body RequestBody get_now_playing, @Query("type") String type);

    @POST(Const.TOTAL_SHARED)
    Call<TotalSharedResponse> get_total_shared(@Body RequestBody get_total_shared);

    @POST(Const.CATEGORY_ITEM_DETAILS)
    Call<VideoDetailsResponse> get_video_detail(@Body RequestBody get_video_detail, @Query("type") String type);

    @POST(Const.FAV_VIDEOS)
    Call<VideoDetailsResponse> get_fav_video_detail(@Body RequestBody get_fav_video_detail, @Query("type") String type);

    @POST(Const.FAV_VIDEOS)
    Call<MenuItemsResponse> get_fav_videos(@Body RequestBody get_fav_videos, @Query("type") String type,
                                           @Query("limit") int limit, @Query("page") int page);

    @POST(Const.FAV_VIDEOS)
    Call<FavVideosResponse> add_remove_fav_videos(@Body RequestBody add_remove_fav_videos, @Query("type") String type);

    @POST(Const.SONG_LIKE)
    Call<LikeDislikeSongResponse> call_like_dis_like(@Body RequestBody call_like_dis_like);

    @POST(Const.FAVORITE_SONG)
    Call<FavSongResponse> add_fav(@Body RequestBody add_fav, @Query("type") String type);

    @POST(Const.PLAYLIST)
    Call<AddPlaylistResponse> create_playlist(@Body RequestBody create_playlist, @Query("type") String type);

    @POST(Const.PLAYLIST)
    Call<ExistingPlaylistResponse> get_playlist(@Body RequestBody get_playlist, @Query("type") String type);

    @POST(Const.ADD_PLAYLIST_SONGS)
    Call<AddPlaylistSongsResponse> add_song_playlist(@Body RequestBody add_song_playlist, @Query("type") String type);

    @POST(Const.ADD_PLAYLIST_SONGS)
    Call<MusicResponse> get_playlist_songs(@Body RequestBody add_song_playlist, @Query("type") String type, @Query("limit") int limit, @Query("page") int page);

    @POST(Const.GET_FAVORITES)
    Call<MusicResponse> get_fav(@Body RequestBody get_fav, @Query("type") String type, @Query("limit") int limit, @Query("page") int page);
    
    @POST(Const.SUBSCIPTIONS)
    Call<CommonResponse> call_subscriptions(@Body RequestBody call_subscriptions);

    @POST(Const.BOOKINGS)
    Call<CommonResponse> call_bookings(@Body RequestBody call_bookings);

    @POST(Const.GET_INSTAGRAM)
    Call<InstagramResponse> get_instagram(@Body RequestBody get_instagram);

    @POST(Const.SUPPORT)
    Call<SupportResponse> call_support(@Body RequestBody call_support);

    @Multipart
    @POST(Const.UPLOAD_PICTURE)
    Call<UploadImageResponse> upload_image(@Part MultipartBody.Part image);

    @GET(Const.WEB_VIEW)
    Call<WebViewResponse> call_web_view();

    @GET(Const.GET_HEADER_MENU_STATUS)
    Call<MenuStatusResponse> get_menu_status();

    @POST(Const.BANNER_SLIDER)
    Call<BannerSlider> getBannerSlider(@Body RequestBody get_bannerSlider);

    @GET(Const.Intro_SLIDER)
    Call<IntroSliderResponse> getIntroSlider();

    @GET(Const.GOOGLE_AD)
    Call<ADView> getGooleAd();

    @GET(Const.PPV)
    Call<PPV> getPPVLink();
}

