package com.durisimomusic.playerManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

public class IncomingCallReceiver extends BroadcastReceiver {

    static int flag;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE))) {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                if (!MediaController.getInstance().isAudioPaused()) {
                    flag = 1;
                    MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                }
            }
        }
        if (TelephonyManager.EXTRA_STATE_IDLE.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE))) {
            if (MediaController.getInstance().getPlayingSongDetail() != null) {
                if (MediaController.getInstance().isAudioPaused()) {
                    if (flag == 1) {
                        flag = 0;
                        MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                }
            }
        }
    }
}