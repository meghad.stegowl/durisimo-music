package com.durisimomusic.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.durisimomusic.R;
import com.durisimomusic.Services.FloatingWidgetService;
import com.durisimomusic.activities.HomeScreen;
import com.durisimomusic.activities.Login;
import com.durisimomusic.application.Durisimo;
import com.durisimomusic.data.models.asset.SongDetails;
import com.durisimomusic.data.models.music.MusicResponse;
import com.durisimomusic.data.remote.APIUtils;
import com.durisimomusic.databinding.FragmentRadioBinding;
import com.durisimomusic.playerManager.MediaController;
import com.durisimomusic.util.AppManageInterface;
import com.durisimomusic.util.AppUtil;
import com.durisimomusic.util.Const;
import com.durisimomusic.util.Prefs;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class RadioFragment extends Fragment {
    private static RadioFragment instance = null;
    FragmentRadioBinding binding;

    private AppManageInterface appManageInterface;
    private String path, type, pageTitle;
    private Context context;
    private FragmentManager fragmentManager;
    private Tracker mTracker;
    private ArrayList<SongDetails> data = new ArrayList<>();


    public static synchronized RadioFragment getInstance() {
        return instance;
    }

    public static synchronized RadioFragment newInstance() {
        return instance = new RadioFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = Durisimo.getDefaultTracker();
        binding = FragmentRadioBinding.inflate(inflater, container, false);
        FloatingWidgetService.hidePipPlayer();
        HomeScreen.isPipModeEnabled = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Radio");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        appManageInterface.closeVideoBottom();

        binding.loader.setVisibility(View.GONE);
        binding.radio.setSelected(true);
        binding.songLoaderRadio.setVisibility(View.GONE);
        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
//            binding.title.setText(pageTitle);
            getRadio();
            automaticallyPlay();
        } else {
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(d -> {
                dialog.dismiss();
                fragmentManager.popBackStack();
            });
        }

        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, HomeScreen.class);
                startActivity(i);
            }
        });

        binding.liveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, LiveTvFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, RadioFragment.newInstance(), "", getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "FavouritesSongsList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "PlaylistList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.drawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, MenuFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        if (MediaController.getInstance().getPlayingSongDetail() != null &&
                MediaController.getInstance().getPlayingSongDetail().getType() != null
                && MediaController.getInstance().getPlayingSongDetail().getType().equals(context.getResources().getString(R.string.radio))) {
            binding.songPlayPauseRadio.setSelected(!MediaController.getInstance().isAudioPaused());
        } else {
            binding.songPlayPauseRadio.setSelected(false);
        }
        setRadioSongLoader(false);

        binding.songPlayPauseRadio.setOnClickListener(view13 -> {
            if (AppUtil.isInternetAvailable(context)) {
                if (MediaController.getInstance().getPlayingSongDetail() != null) {
                    if (MediaController.getInstance().getPlayingSongDetail().getType() != null && MediaController.getInstance().getPlayingSongDetail().getType().toLowerCase().equals(context.getResources().getString(R.string.radio).toLowerCase())) {
                        if (MediaController.getInstance().getPlayingSongDetail().getSongId() != null && MediaController.getInstance().getPlayingSongDetail().getSongId().equals(data.get(0).getSongId())) {
                            if (MediaController.getInstance().isAudioPaused()) {
                                MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                            } else {
                                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                            }
                        } else {
                            MediaController.getInstance().setPlaylist(data, data.get(0));
                        }
                    } else MediaController.getInstance().setPlaylist(data, data.get(0));
                } else {
                    try {
                        MediaController.getInstance().setPlaylist(data, data.get(0));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                AppUtil.show_Snackbar(context, binding.container, getResources().getString(R.string.no_internet_connection), false);
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onDestroyView() {
        appManageInterface.setVisibleStatus(false, false, true, false, false);
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void automaticallyPlay() {
        if (MediaController.getInstance().getPlayingSongDetail() != null) {
            if (MediaController.getInstance().getPlayingSongDetail().getType() != null &&
                    MediaController.getInstance().getPlayingSongDetail().getType().toLowerCase().equals(context.getResources().getString(R.string.radio).toLowerCase())) {
                if (MediaController.getInstance().getPlayingSongDetail().getSongId() != null &&
                        MediaController.getInstance().getPlayingSongDetail().getSongId().equals(data.get(0).getSongId())) {
                    if (MediaController.getInstance().isAudioPaused()) {
                        MediaController.getInstance().playAudio(MediaController.getInstance().getPlayingSongDetail());
                    } else {
                        MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                    }
                } else {
                    try {
                        MediaController.getInstance().setPlaylist(data, data.get(0));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    MediaController.getInstance().setPlaylist(data, data.get(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                MediaController.getInstance().setPlaylist(data, data.get(0));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getRadio() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_radio = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_radio(call_radio, "32", "Radio").enqueue(new Callback<MusicResponse>() {
                @Override
                public void onResponse(@NonNull Call<MusicResponse> call, @NonNull Response<MusicResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                binding.assetTitle.setText(response.body().getData().get(0).getSongName());
                                response.body().getData().get(0).setType(context.getResources().getString(R.string.radio));
                                response.body().getData().get(0).setSongId(response.body().getData().get(0).getSongId());
                                response.body().getData().get(0).setSong(response.body().getData().get(0).getSong());
                                response.body().getData().get(0).setSongName(response.body().getData().get(0).getSongName());
                                response.body().getData().get(0).setSongImage(response.body().getData().get(0).getSongImage());
                                data = response.body().getData();
                                automaticallyPlay();
                                Glide
                                        .with(Durisimo.applicationContext)
                                        .load(response.body().getData().get(0).getSongImage())
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .centerCrop()
                                        .placeholder(R.drawable.app_logo)
                                        .into(binding.assetImage);
                                binding.loader.setVisibility(View.GONE);
                            } else {
                                binding.loader.setVisibility(View.GONE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MusicResponse> call, @NonNull Throwable t) {
                    binding.loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    public void setRadioSongLoader(boolean isLoaderVisible) {
        binding.songLoaderRadio.setVisibility(isLoaderVisible ? View.VISIBLE : View.GONE);
        binding.songPlayPauseRadio.setVisibility(isLoaderVisible ? View.GONE : View.VISIBLE);
        int progressbarValue = 0;
        if (MediaController.getInstance().getPlayingSongDetail() != null && MediaController.getInstance().getPlayingSongDetail().getType() != null && MediaController.getInstance().getPlayingSongDetail().getType().equals(context.getResources().getString(R.string.radio)))
            progressbarValue = (int) (MediaController.getInstance().getPlayingSongDetail().audioProgress * 100);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.songSeekBarRadio.setProgress(progressbarValue, true);
        } else {
            binding.songSeekBarRadio.setProgress(progressbarValue);
        }
    }

    public void setRadioPLayPause() {
        if (MediaController.getInstance().getPlayingSongDetail() != null &&
                MediaController.getInstance().getPlayingSongDetail().getType() != null
                && MediaController.getInstance().getPlayingSongDetail().getType().equals(context.getResources().getString(R.string.radio))) {
            binding.songPlayPauseRadio.setSelected(!MediaController.getInstance().isAudioPaused());
        } else {
            binding.songPlayPauseRadio.setSelected(false);
        }
    }

    public void setSecondaryProgress(int progressbarValue) {
        if (MediaController.getInstance().getPlayingSongDetail() != null && MediaController.getInstance().getPlayingSongDetail().getType() != null && MediaController.getInstance().getPlayingSongDetail().getType().equals(context.getResources().getString(R.string.radio)))
            binding.songSeekBarRadio.setSecondaryProgress(progressbarValue);
    }
}
