package com.durisimomusic.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.durisimomusic.R;
import com.durisimomusic.Services.FloatingWidgetService;
import com.durisimomusic.activities.HomeScreen;
import com.durisimomusic.activities.Login;
import com.durisimomusic.adapters.AdapterMore;
import com.durisimomusic.application.Durisimo;
import com.durisimomusic.data.models.More;
import com.durisimomusic.data.models.common.CommonResponse;
import com.durisimomusic.data.models.instagram.InstagramResponse;
import com.durisimomusic.data.remote.APIUtils;
import com.durisimomusic.databinding.FragmentMenuBinding;
import com.durisimomusic.playerManager.MediaController;
import com.durisimomusic.util.AppManageInterface;
import com.durisimomusic.util.AppUtil;
import com.durisimomusic.util.Const;
import com.durisimomusic.util.Prefs;
import com.durisimomusic.util.RecyclerItemClickListener;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuFragment extends Fragment {

    private static MenuFragment instance = null;
    FragmentMenuBinding binding;

    private AppManageInterface appManageInterface;
    private Context context;
    private FragmentManager fragmentManager;
    private Tracker mTracker;
    private String instagramPath;
    private ArrayList<More> moreArrayList;
    private AdapterMore adapterMore;
    private int pos = -1;
    FloatingWidgetService floatingWidgetService;

    public static synchronized MenuFragment getInstance() {
        return instance;
    }

    public static synchronized MenuFragment newInstance() {
        return instance = new MenuFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = Durisimo.getDefaultTracker();
        binding = FragmentMenuBinding.inflate(inflater, container, false);
        FloatingWidgetService.hidePipPlayer();
        HomeScreen.isPipModeEnabled = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Drawer");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        init();
        listner();
    }

    private void init() {
//        appManageInterface.bottomanduppermanage(true);
//        appManageInterface.miniPlayerVisibilityGone(false);
        get_instagram();
        binding.drawerPopup.setSelected(true);
        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.showMainAcreeninFavPlaylist();
//                appManageInterface.go_back();
            }
        });

        binding.liveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, LiveTvFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(fragmentManager, RadioFragment.newInstance(), "", getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "FavouritesSongsList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "PlaylistList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.drawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.drawerPopup.setSelected(true);
            }
        });

        binding.listing.setLayoutManager(new LinearLayoutManager(context));

        String[] menuName;
        if (Prefs.getPrefInstance().getValue(context, Const.LOGIN_ACCESS, "").equals(context.getResources().getString(R.string.logged_in))) {
            menuName = new String[]{/*"PPV",*/"My favorite videos", "My Profile", "Notifications", "Social Media", "Log Out"};
        } else {
            menuName = new String[]{/*"PPV",*/"My favorite videos", "My Profile", "Notifications", "Social Media", "Login"};
        }

        int[] menuImage = {/*R.drawable.ic_ppv,*/R.drawable.ic_fav_video, R.drawable.ic_profile, R.drawable.notifications_drawer,
                R.drawable.social_media, R.drawable.ic_profile};
        moreArrayList = new ArrayList<>();
        for (int i = 0; i < menuImage.length; i++) {
            moreArrayList.add(new More(menuImage[i], menuName[i]));
        }
        adapterMore = new AdapterMore(moreArrayList, context);
        binding.listing.setAdapter(adapterMore);
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private void get_instagram() {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_instagram = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_instagram(get_instagram).enqueue(new Callback<InstagramResponse>() {
                @Override
                public void onResponse(@NonNull Call<InstagramResponse> call, @NonNull Response<InstagramResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getLink() != null && !response.body().getLink().isEmpty())
                                instagramPath = response.body().getLink();
                            else instagramPath = "https://www.instagram.com/";
                            Prefs.getPrefInstance().setValue(context, Const.INSTAGRAM_PATH, instagramPath);
                        } else {
                            instagramPath = "https://www.instagram.com/";
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<InstagramResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    instagramPath = "https://www.instagram.com/";
                }
            });
        } else {

        }
    }

    private void listner() {

        binding.listing.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                adapterMore.setSelectedItem(position);
                pos = position;
                More item = moreArrayList.get(position);
                String name = item.getTitle();
                switch (name) {
//                    case "PPV":
//                        AppUtil.setup_Fragment(fragmentManager, PPVFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
//                        appManageInterface.showFragment();
//                        break;

                    case "My favorite videos":
                        AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "My favorite videos", "", "",
                                "DrawerInner", "DrawerInner", true);
                        appManageInterface.showFragment();
                        break;


                    case "My Profile":
                        appManageInterface.setVisibleStatus(false, false, false, false, false);
                        if (Prefs.getPrefInstance().getValue(context, Const.LOGIN_ACCESS, "").equals(context.getResources().getString(R.string.logged_in))) {
                            AppUtil.setup_Fragment(fragmentManager, ProfileFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                            appManageInterface.showFragment();
                        } else {
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Please Login to Continue.");
                            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText("Ok");
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("Cancel");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog.dismiss());
                            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(dv -> {
                                Prefs.getPrefInstance().remove(context, Const.USER_NAME);
                                Prefs.getPrefInstance().remove(context, Const.PHONE_NUMBER);
                                Prefs.getPrefInstance().remove(context, Const.PROFILE_IMAGE);
                                Prefs.getPrefInstance().remove(context, Const.LOGIN_ACCESS);
                                Prefs.getPrefInstance().remove(context, Const.USER_ID);
                                Prefs.getPrefInstance().remove(context, Const.TOKEN);
                                Prefs.getPrefInstance().remove(context, Const.EMAIL);
                                Intent i = new Intent(context, Login.class);
                                startActivity(i);
                                getActivity().finish();
                            });
                        }
                        break;

                    case "Notifications":
                        AppUtil.setup_Fragment(fragmentManager, NotificationsFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                        appManageInterface.showFragment();
                        break;

                    case "Social Media":
                        AppUtil.setup_Fragment(fragmentManager, SocialMediaFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                        appManageInterface.showFragment();
                        break;

                    case "Log Out":
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.sign_out_text));
                        ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(context.getResources().getString(R.string.yes));
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.cancel));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog.dismiss());
                        dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(dv -> {
                            callLogout();
                            if (MediaController.getInstance().getPlayingSongDetail() != null && !MediaController.getInstance().isAudioPaused()) {
                                MediaController.getInstance().pauseAudio(MediaController.getInstance().getPlayingSongDetail());
                            }
                            ((HomeScreen) context).showFragment();
                        });
                        break;

                    case "Login":
                        appManageInterface.setPlayerClose();
                        Intent i = new Intent(context, Login.class);
                        startActivity(i);
                        getActivity().finish();
                        break;
                }
            }
        }));
    }

    private void callLogout() {
        if (AppUtil.isInternetAvailable(context)) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_logout = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().call_logout(call_logout).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            Prefs.getPrefInstance().remove(context, Const.USER_NAME);
                            Prefs.getPrefInstance().remove(context, Const.PHONE_NUMBER);
                            Prefs.getPrefInstance().remove(context, Const.PROFILE_IMAGE);
                            Prefs.getPrefInstance().remove(context, Const.LOGIN_ACCESS);
                            Prefs.getPrefInstance().remove(context, Const.USER_ID);
                            Prefs.getPrefInstance().remove(context, Const.TOKEN);
                            Prefs.getPrefInstance().remove(context, Const.EMAIL);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            ((HomeScreen) context).finish();
                        } else {
                            Prefs.getPrefInstance().remove(context, Const.USER_NAME);
                            Prefs.getPrefInstance().remove(context, Const.PHONE_NUMBER);
                            Prefs.getPrefInstance().remove(context, Const.PROFILE_IMAGE);
                            Prefs.getPrefInstance().remove(context, Const.LOGIN_ACCESS);
                            Prefs.getPrefInstance().remove(context, Const.USER_ID);
                            Prefs.getPrefInstance().remove(context, Const.TOKEN);
                            Prefs.getPrefInstance().remove(context, Const.EMAIL);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            ((HomeScreen) context).finish();
                        }
                    } else {
                        Prefs.getPrefInstance().remove(context, Const.USER_NAME);
                        Prefs.getPrefInstance().remove(context, Const.PHONE_NUMBER);
                        Prefs.getPrefInstance().remove(context, Const.PROFILE_IMAGE);
                        Prefs.getPrefInstance().remove(context, Const.LOGIN_ACCESS);
                        Prefs.getPrefInstance().remove(context, Const.USER_ID);
                        Prefs.getPrefInstance().remove(context, Const.TOKEN);
                        Prefs.getPrefInstance().remove(context, Const.EMAIL);
                        Intent i = new Intent(context, Login.class);
                        startActivity(i);
                        ((HomeScreen) context).finish();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    Prefs.getPrefInstance().remove(context, Const.USER_NAME);
                    Prefs.getPrefInstance().remove(context, Const.PHONE_NUMBER);
                    Prefs.getPrefInstance().remove(context, Const.PROFILE_IMAGE);
                    Prefs.getPrefInstance().remove(context, Const.LOGIN_ACCESS);
                    Prefs.getPrefInstance().remove(context, Const.USER_ID);
                    Prefs.getPrefInstance().remove(context, Const.TOKEN);
                    Prefs.getPrefInstance().remove(context, Const.EMAIL);
                    Intent i = new Intent(context, Login.class);
                    startActivity(i);
                    ((HomeScreen) context).finish();
                }
            });
        } else {
            Prefs.getPrefInstance().remove(context, Const.USER_NAME);
            Prefs.getPrefInstance().remove(context, Const.PHONE_NUMBER);
            Prefs.getPrefInstance().remove(context, Const.PROFILE_IMAGE);
            Prefs.getPrefInstance().remove(context, Const.LOGIN_ACCESS);
            Prefs.getPrefInstance().remove(context, Const.USER_ID);
            Prefs.getPrefInstance().remove(context, Const.TOKEN);
            Prefs.getPrefInstance().remove(context, Const.EMAIL);
            Intent i = new Intent(context, Login.class);
            startActivity(i);
            ((HomeScreen) context).finish();
        }
    }
}
