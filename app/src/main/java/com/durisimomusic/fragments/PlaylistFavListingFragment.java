package com.durisimomusic.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.durisimomusic.R;
import com.durisimomusic.Services.FloatingWidgetService;
import com.durisimomusic.activities.HomeScreen;
import com.durisimomusic.activities.Login;
import com.durisimomusic.adapters.AdapterMainviewPager;
import com.durisimomusic.adapters.AssetListingAdapter;
import com.durisimomusic.adapters.ExistingPlaylistAdapter;
import com.durisimomusic.application.Durisimo;
import com.durisimomusic.data.models.BannerSlider;
import com.durisimomusic.data.models.BannerSliderData;
import com.durisimomusic.data.models.asset.SongDetails;
import com.durisimomusic.data.models.music.ExistingPlaylistResponse;
import com.durisimomusic.data.models.music.MusicResponse;
import com.durisimomusic.data.remote.APIUtils;
import com.durisimomusic.databinding.FragmentCommonListingBinding;
import com.durisimomusic.util.AppManageInterface;
import com.durisimomusic.util.AppUtil;
import com.durisimomusic.util.Const;
import com.durisimomusic.util.Prefs;
import com.durisimomusic.util.RecyclerViewPositionHelper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaylistFavListingFragment extends Fragment {

    private static PlaylistFavListingFragment instance = null;
    FragmentCommonListingBinding binding;

    private int LIMIT = 20, CURRENT_PAGE = 1, LAST_PAGE = 100;
    private AppManageInterface appManageInterface;
    private String path, type, pageTitle, id;
    private Context context;
    private FragmentManager fragmentManager;
    private Tracker mTracker;
    private boolean loadingInProgress, isInner;
    private ExistingPlaylistAdapter existingPlaylistAdapter;
    private AssetListingAdapter assetListingAdapter;
    private ArrayList<BannerSliderData> bannerSliderData = new ArrayList<>();
    AdapterMainviewPager adapterMainviewPager;
    private ArrayList<SongDetails> data = new ArrayList<>();
    private final Object progressTimerSync = new Object();
    private final Object sync = new Object();
    private Timer progressTimer = null;

   public static synchronized PlaylistFavListingFragment getInstance() {
        return instance;
    }

    public static synchronized PlaylistFavListingFragment newInstance() {
        return instance = new PlaylistFavListingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = Durisimo.getDefaultTracker();
        binding = FragmentCommonListingBinding.inflate(inflater, container, false);
        FloatingWidgetService.hidePipPlayer();
        HomeScreen.isPipModeEnabled = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Video Listing");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        binding.listing.setVisibility(View.GONE);
        binding.noDataFound.setVisibility(View.GONE);
        binding.rlNoPlaylistData.setVisibility(View.GONE);
        binding.rlNoFavoriteData.setVisibility(View.GONE);
        binding.loader.setVisibility(View.GONE);
        getSliderData();
        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
            id = getArguments().getString("id");
        }

        if (type != null && path != null) {
            if (type.equals(context.getResources().getString(R.string.type_playlist_songs)))
                binding.ivPlaylist.setSelected(true);
            else if (type.equals("FavouritesSongsList"))
                binding.ivFav.setSelected(true);
            else
                binding.ivPlaylist.setSelected(true);

            CURRENT_PAGE = 1;
            LAST_PAGE = 1;
            LIMIT = 20;
            binding.listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
            binding.listing.setHasFixedSize(true);
//            if (type.toLowerCase().equals("VideosCategory".toLowerCase()))
            existingPlaylistAdapter = new ExistingPlaylistAdapter(context, fragmentManager, new ArrayList<>(), true, new SongDetails(), null);
            binding.listing.setAdapter(existingPlaylistAdapter);
            binding.listing.setVisibility(View.VISIBLE);

            RecyclerViewPositionHelper positionHelper = RecyclerViewPositionHelper.createHelper(binding.listing);

            binding.listing.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0) {
                        LinearLayoutManager layoutManager = (LinearLayoutManager) binding.listing.getLayoutManager();
                        if (layoutManager != null) {
                            int total = layoutManager.getItemCount();
                            int currentLastItem = positionHelper.findLastCompletelyVisibleItemPosition() + 1;

                            if (currentLastItem == (total - 10)) {
                                if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                                    loadingInProgress = true;
                                    CURRENT_PAGE++;
                                    if (type.equals(context.getResources().getString(R.string.type_playlist_songs)))
                                        getPlaylistSongs();
                                    else if (type.equals("FavouritesSongsList"))
                                        getFavoriteSongs();
                                    else
                                        getPlaylist();
                                }
                            }
                        }
                    }
                }
            });

            binding.scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                        if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                            loadingInProgress = true;
                            CURRENT_PAGE++;
                            if (type.equals(context.getResources().getString(R.string.type_playlist_songs)))
                                getPlaylistSongs();
                            else if (type.equals("FavouritesSongsList"))
                                getFavoriteSongs();
                            else
                                getPlaylist();
                        }
                    }
                }
            });

            binding.ivNoPlaylist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, HomeScreen.class);
                    startActivity(i);
                }
            });

            binding.ivNoFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, HomeScreen.class);
                    startActivity(i);
                }
            });

            if (type.equals(context.getResources().getString(R.string.type_playlist_songs)))
                getPlaylistSongs();
            else if (type.equals("FavouritesSongsList"))
                getFavoriteSongs();
            else
                getPlaylist();
        } else {
            if (type.equals(context.getResources().getString(R.string.type_playlist_songs)))
                binding.noDataFound.setVisibility(View.VISIBLE);
            else if (type.equals("FavouritesSongsList"))
                binding.rlNoFavoriteData.setVisibility(View.VISIBLE);
            else
                binding.rlNoPlaylistData.setVisibility(View.VISIBLE);

            binding.listing.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(d -> {
                dialog.dismiss();
                fragmentManager.popBackStack();
            });
        }
//        binding.back.setOnClickListener(v -> appManageInterface.go_back());

        binding.innerSearch.setOnClickListener(view1 -> {
            if (binding.searchInput.getText() == null) {
                AppUtil.show_Snackbar(context, binding.searchInput, getString(R.string.type_to_search), true);
            } else if (binding.searchInput.getText().toString().trim().length() < 1) {
                AppUtil.show_Snackbar(context, binding.searchInput, getString(R.string.type_to_search), true);
            } else {
                AppUtil.setup_Fragment(fragmentManager, SearchFragment.newInstance(), "", "", binding.searchInput.getText().toString(), "", "Inner", "Inner", true);
            }
        });

        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.showMainAcreeninFavPlaylist();
            }
        });

        binding.liveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, LiveTvFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(fragmentManager, RadioFragment.newInstance(), "", "Radio", "Live Radio", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "FavouritesSongsList", "FavouritesSongsList", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "PlaylistList", "PlaylistList", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });

        binding.drawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.drawerPopup.setSelected(true);
                AppUtil.setup_Fragment(fragmentManager, MenuFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
    }

    public void onLoadSong(String songid) {
        Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, songid);
        if (data != null) {
            for (SongDetails item : data) {
                if (String.valueOf(item.getSongId()).equals(songid)) {
                    Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, songid);
                }
                Log.d("mytag", "onload song now playin : " + Prefs.getPrefInstance().getValue(context, Const.NOW_PLAYING, ""));
            }
            if (assetListingAdapter != null) {
                assetListingAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void getPlaylist() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_playlist = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_playlist(get_playlist, "PlaylistList").enqueue(new Callback<ExistingPlaylistResponse>() {
                @Override
                public void onResponse(@NonNull Call<ExistingPlaylistResponse> call, @NonNull Response<ExistingPlaylistResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                binding.listing.setHasFixedSize(true);
                                binding.listing.setLayoutManager(new LinearLayoutManager(context));
                                binding.listing.setAdapter(new ExistingPlaylistAdapter(context, fragmentManager, response.body().getData(), true, new SongDetails(), null));

                                binding.listing.setVisibility(View.VISIBLE);
                                binding.noDataFound.setVisibility(View.GONE);
                                binding.rlNoPlaylistData.setVisibility(View.GONE);
                                binding.loader.setVisibility(View.GONE);

                            } else {
                                binding.listing.setVisibility(View.GONE);
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFound.setVisibility(View.GONE);
                                binding.rlNoPlaylistData.setVisibility(View.VISIBLE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.listing.setVisibility(View.GONE);
                            binding.loader.setVisibility(View.GONE);
                            binding.noDataFound.setVisibility(View.GONE);
                            binding.rlNoPlaylistData.setVisibility(View.VISIBLE);
                        }
                    } else {
                        binding.listing.setVisibility(View.GONE);
                        binding.loader.setVisibility(View.GONE);
                        binding.noDataFound.setVisibility(View.GONE);
                        binding.rlNoPlaylistData.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ExistingPlaylistResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.listing.setVisibility(View.GONE);
                    binding.loader.setVisibility(View.GONE);
                    binding.noDataFound.setVisibility(View.GONE);
                    binding.rlNoPlaylistData.setVisibility(View.VISIBLE);
                }
            });
        } else {
            binding.listing.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
            binding.noDataFound.setVisibility(View.GONE);
            binding.rlNoPlaylistData.setVisibility(View.VISIBLE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    private void getPlaylistSongs() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                jsonObject.put("playlist_id", path);
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_playlist = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_playlist_songs(get_playlist, "PlaylistSongsList", LIMIT, CURRENT_PAGE).enqueue(new Callback<MusicResponse>() {
                @Override
                public void onResponse(@NonNull Call<MusicResponse> call, @NonNull Response<MusicResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                binding.listing.setHasFixedSize(true);
                                binding.listing.setLayoutManager(new LinearLayoutManager(context));
                                assetListingAdapter = new AssetListingAdapter(context, fragmentManager, response.body().getData(), context.getResources().getString(R.string.type_playlist_songs));
                                binding.listing.setAdapter(assetListingAdapter);
                                binding.listing.setVisibility(View.VISIBLE);
                                binding.noDataFound.setVisibility(View.GONE);
                                binding.loader.setVisibility(View.GONE);
                            } else {
                                binding.listing.setVisibility(View.GONE);
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFound.setVisibility(View.VISIBLE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.listing.setVisibility(View.GONE);
                            binding.loader.setVisibility(View.GONE);
                            binding.noDataFound.setVisibility(View.VISIBLE);
                        }
                    } else {
                        binding.listing.setVisibility(View.GONE);
                        binding.loader.setVisibility(View.GONE);
                        binding.noDataFound.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MusicResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.listing.setVisibility(View.GONE);
                    binding.loader.setVisibility(View.GONE);
                    binding.noDataFound.setVisibility(View.VISIBLE);
                }
            });
        } else {
            binding.listing.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
            binding.noDataFound.setVisibility(View.VISIBLE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    private void getFavoriteSongs() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                jsonObject.put("playlist_id", path);
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_fav = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_fav(get_fav, "FavouritesSongsList", LIMIT, CURRENT_PAGE).enqueue(new Callback<MusicResponse>() {
                @Override
                public void onResponse(@NonNull Call<MusicResponse> call, @NonNull Response<MusicResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                binding.listing.setHasFixedSize(true);
                                binding.listing.setLayoutManager(new LinearLayoutManager(context));
//                                binding.listing.setAdapter(new AssetListingAdapter(context, fragmentManager, response.body().getData(), context.getResources().getString(R.string.type_fav)));
                                assetListingAdapter = new AssetListingAdapter(context, fragmentManager, response.body().getData(), context.getResources().getString(R.string.type_fav));
                                binding.listing.setAdapter(assetListingAdapter);
                                binding.listing.setVisibility(View.VISIBLE);
                                binding.noDataFound.setVisibility(View.GONE);
                                binding.rlNoFavoriteData.setVisibility(View.GONE);
                                binding.loader.setVisibility(View.GONE);
                            } else {
                                binding.listing.setVisibility(View.GONE);
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFound.setVisibility(View.GONE);
                                binding.rlNoFavoriteData.setVisibility(View.VISIBLE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.listing.setVisibility(View.GONE);
                            binding.loader.setVisibility(View.GONE);
                            binding.noDataFound.setVisibility(View.GONE);
                            binding.rlNoFavoriteData.setVisibility(View.VISIBLE);
                        }
                    } else {
                        binding.listing.setVisibility(View.GONE);
                        binding.loader.setVisibility(View.GONE);
                        binding.noDataFound.setVisibility(View.GONE);
                        binding.rlNoFavoriteData.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MusicResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.listing.setVisibility(View.GONE);
                    binding.loader.setVisibility(View.GONE);
                    binding.noDataFound.setVisibility(View.GONE);
                    binding.rlNoFavoriteData.setVisibility(View.VISIBLE);
                }
            });
        } else {
            binding.listing.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
            binding.noDataFound.setVisibility(View.GONE);
            binding.rlNoFavoriteData.setVisibility(View.VISIBLE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    public void setPlaylistSongs() {
        getPlaylistSongs();
    }

    public void setFavSongs() {
        getFavoriteSongs();
    }

    public void setPlaylist() {
        getPlaylist();
    }

    public void setNoDataFound() {
        binding.listing.setVisibility(View.GONE);
        binding.loader.setVisibility(View.GONE);
        binding.noDataFound.setVisibility(View.GONE);
        binding.rlNoPlaylistData.setVisibility(View.VISIBLE);
    }

    private void getSliderData() {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody call_social_media = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().getBannerSlider(call_social_media).enqueue(new Callback<BannerSlider>() {
                @Override
                public void onResponse(@NonNull Call<BannerSlider> call, @NonNull Response<BannerSlider> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                bannerSliderData = response.body().getData();
                                adapterMainviewPager = new AdapterMainviewPager(context, bannerSliderData);
                                binding.mPager.setAdapter(adapterMainviewPager);
                                binding.mPager.setPagingEnabled(true);
                                binding.indicator.setViewPager(binding.mPager);
                                startImageSliderTimer();
                            } else {
                                binding.noDataSlider.setVisibility(View.GONE);
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            binding.noDataSlider.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BannerSlider> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    binding.noDataSlider.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            binding.noDataSlider.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void startImageSliderTimer() {
        synchronized (progressTimerSync) {
            if (progressTimer != null) {
                try {
                    progressTimer.cancel();
                    progressTimer = null;
                } catch (Exception e) {

                }
            }
            progressTimer = new Timer();
            progressTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sync) {
                        runOnUIThread(new Runnable() {
                            @Override
                            public void run() {

                                int currentItem = binding.mPager.getCurrentItem() + 1;
                                int totalItem = adapterMainviewPager.getCount();
                                if (currentItem == totalItem) {
                                    binding.mPager.setCurrentItem(0);
                                } else {
                                    binding.mPager.setCurrentItem(currentItem);
                                }
                            }
                        });
                    }
                }
            }, 0, 5000);
        }
    }

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            Durisimo.applicationHandler.post(runnable);
        } else {
            Durisimo.applicationHandler.postDelayed(runnable, delay);
        }
    }
}
