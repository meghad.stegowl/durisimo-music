package com.durisimomusic.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.durisimomusic.R;
import com.durisimomusic.Services.FloatingWidgetService;
import com.durisimomusic.activities.HomeScreen;
import com.durisimomusic.activities.Login;
import com.durisimomusic.adapters.AssetListingAdapter;
import com.durisimomusic.application.Durisimo;
import com.durisimomusic.data.models.asset.SongDetails;
import com.durisimomusic.data.models.music.MusicResponse;
import com.durisimomusic.data.remote.APIUtils;
import com.durisimomusic.databinding.FragmentSearchBinding;
import com.durisimomusic.util.AppManageInterface;
import com.durisimomusic.util.AppUtil;
import com.durisimomusic.util.Const;
import com.durisimomusic.util.Prefs;
import com.durisimomusic.util.RecyclerViewPositionHelper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {

    private static SearchFragment instance = null;
    FragmentSearchBinding binding;

    private int LIMIT = 20, CURRENT_PAGE = 1, LAST_PAGE = 100;
    private AppManageInterface appManageInterface;
    private String path, type, pageTitle;
    private Context context;
    private FragmentManager fragmentManager;
    private Tracker mTracker;
    private boolean loadingInProgress;
    private AssetListingAdapter assetListingAdapter;
    private ArrayList<SongDetails> data = new ArrayList<>();


    public static synchronized SearchFragment getInstance() {
        return instance;
    }

    public static synchronized SearchFragment newInstance() {
        return instance = new SearchFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTracker = Durisimo.getDefaultTracker();
        binding = FragmentSearchBinding.inflate(inflater, container, false);
        FloatingWidgetService.hidePipPlayer();
        HomeScreen.isPipModeEnabled = true;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Search");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        binding.listing.setVisibility(View.GONE);
        binding.noDataFound.setVisibility(View.GONE);
        binding.loader.setVisibility(View.GONE);
        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
        }


        init();

        CURRENT_PAGE = 1;
        LAST_PAGE = 1;
        LIMIT = 20;
        binding.listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        binding.listing.setHasFixedSize(true);
        assetListingAdapter = new AssetListingAdapter(context, fragmentManager, new ArrayList<>(), "");
        binding.listing.setAdapter(assetListingAdapter);
        binding.listing.setVisibility(View.VISIBLE);

        RecyclerViewPositionHelper positionHelper = RecyclerViewPositionHelper.createHelper(binding.listing);

        binding.listing.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) binding.listing.getLayoutManager();
                    if (layoutManager != null) {
                        int total = layoutManager.getItemCount();
                        int currentLastItem = positionHelper.findLastCompletelyVisibleItemPosition() + 1;

                        if (currentLastItem == (total - 10)) {
                            if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                                loadingInProgress = true;
                                CURRENT_PAGE++;
                                getSearchListing(pageTitle);
                            }
                        }
                    }

                }
            }
        });

        binding.scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                    if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                        loadingInProgress = true;
                        CURRENT_PAGE++;
                        getSearchListing(pageTitle);

                    }
                }
            }
        });
        getSearchListing(pageTitle);
        binding.back.setOnClickListener(v -> appManageInterface.go_back());
        binding.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, HomeScreen.class);
                startActivity(i);
            }
        });
        binding.liveTvPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, LiveTvFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appManageInterface.setVisibleStatus(false, false, false, false, false);
                AppUtil.setup_Fragment(fragmentManager, RadioFragment.newInstance(), "", getResources().getString(R.string.radio), getResources().getString(R.string.live_radio), "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, VideoFragment.newInstance(), "", "VideosCategory", "VideosCategory", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "FavouritesSongsList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, PlaylistFavListingFragment.newInstance(), "", "PlaylistList", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
        binding.drawerPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.setup_Fragment(fragmentManager, MenuFragment.newInstance(), "", "", "", "", "DrawerInner", "DrawerInner", true);
                appManageInterface.showFragment();
            }
        });
    }

    private void init() {

        binding.innerSearch.setOnClickListener(view -> {
            if (binding.searchInput.getText() == null) {
                AppUtil.show_Snackbar(context, binding.searchInput, getString(R.string.type_to_search), true);
            } else if (binding.searchInput.getText().toString().trim().length() < 1) {
                AppUtil.show_Snackbar(context, binding.searchInput, getString(R.string.type_to_search), true);
            } else {
                getSearchListing(binding.searchInput.getText().toString());
            }
        });

        binding.clearSearch.setOnClickListener(view -> {
            binding.searchInput.setText("");
            data = new ArrayList<SongDetails>();
            assetListingAdapter = new AssetListingAdapter(context, fragmentManager, new ArrayList<>(), "");
            binding.listing.setAdapter(assetListingAdapter);
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void getSearchListing(String keyword) {
        if (AppUtil.isInternetAvailable(context)) {
            binding.loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", Prefs.getPrefInstance().getValue(context, Const.TOKEN, ""));
                jsonObject.put("search", keyword);
            } catch (JSONException e) {
                binding.loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_search_result = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_search_result(get_search_result, LIMIT, CURRENT_PAGE).enqueue(new Callback<MusicResponse>() {
                @Override
                public void onResponse(@NonNull Call<MusicResponse> call, @NonNull Response<MusicResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                loadingInProgress = false;
                                binding.loader.setVisibility(View.GONE);
                                assetListingAdapter.add(response.body().getData());
                                LAST_PAGE = response.body().getLastPage();
                                if (CURRENT_PAGE == 1) {
                                    binding.listing.setVisibility(View.VISIBLE);
                                    binding.noDataFound.setVisibility(View.GONE);
                                    binding.loader.setVisibility(View.GONE);
                                } else {
                                    loadingInProgress = false;
                                    if (CURRENT_PAGE == 1) {
                                        binding.loader.setVisibility(View.GONE);
                                        binding.noDataFound.setVisibility(View.VISIBLE);
                                        binding.listing.setVisibility(View.GONE);
                                    }
                                }
                            } else {
                                loadingInProgress = false;
                                if (CURRENT_PAGE == 1) {
                                    binding.loader.setVisibility(View.GONE);
                                    binding.noDataFound.setVisibility(View.VISIBLE);
                                    binding.listing.setVisibility(View.GONE);
                                }
                            }
                        }else if (response.body().getStatus() != null && response.body().getStatus().equals(401)) {
                            binding.loader.setVisibility(View.GONE);
                            Intent i = new Intent(context, Login.class);
                            startActivity(i);
                            getActivity().finish();
                        } else {
                            loadingInProgress = false;
                            if (CURRENT_PAGE == 1) {
                                binding.loader.setVisibility(View.GONE);
                                binding.noDataFound.setVisibility(View.VISIBLE);
                                binding.listing.setVisibility(View.GONE);
                                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                final AlertDialog dialog = new AlertDialog.Builder(context)
                                        .setCancelable(false)
                                        .setView(dialog_view)
                                        .show();

                                if (dialog.getWindow() != null)
                                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                    dialog.dismiss();
                                });
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MusicResponse> call, @NonNull Throwable t) {
                    loadingInProgress = false;
                    if (CURRENT_PAGE == 1) {
                        binding.loader.setVisibility(View.GONE);
                        binding.noDataFound.setVisibility(View.VISIBLE);
                        binding.listing.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }
            });
        } else {
            loadingInProgress = false;
            if (CURRENT_PAGE == 1) {
                binding.loader.setVisibility(View.GONE);
                binding.noDataFound.setVisibility(View.VISIBLE);
                binding.listing.setVisibility(View.GONE);
            }
        }
    }

    public void onLoadSong(String songid) {
        Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, songid);
        if (data != null) {
            for (SongDetails item : data) {
                if (String.valueOf(item.getSongId()).equals(songid)) {
                    Prefs.getPrefInstance().setValue(context, Const.NOW_PLAYING, songid);
                }
                Log.d("mytag", "onload song now playin : " + Prefs.getPrefInstance().getValue(context, Const.NOW_PLAYING, ""));
            }
            if (assetListingAdapter != null) {
                assetListingAdapter.notifyDataSetChanged();
            }
        }
    }

}
